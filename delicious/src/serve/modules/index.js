import request from '@/utils/request'

// 轮播图数据
export function getBanners() {
    return request.post('/basic/app/banner')
}

// 轮播图下面分类数据
export function getWidget() {
    return request.post('/basic/app/homeFunctionWidget')
}

//显示特惠
export function getOperatList() {
    return request.post('/basic/app/operatList')
}

// tab列表
export function getTabs(showPage = 0, platform = 0) {
    return request.post('/basic/pageConfig/tabs', {
        showPage,
        platform,
    })
}

// getQueryByTab列表
export function getQueryByTab(
    body,
    showPage = 0,
    platform = 0
) {
    return request.post('/c_msh/mLife/goods/list/queryByTab', {
        ...body,
        showPage,
        platform
    })
}

// 搜索词
export function getSwitch() {
    return request.post('/basic/miniProgramMenu/switch')
}
