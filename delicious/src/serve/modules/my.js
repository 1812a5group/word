import request from '@/utils/request'
// 获取页面id
export function getuserid() {
    return request.post("/c_msh/mLife/users/userGet")
}
// 获取优惠券
export function getMyCoupons(type = 0, startId = 0, size = 20, pageNo = 1) {
    return request.post("/c_msh/mLife/coupon/user/list", {
        "type": type,
        "startId": startId,
        "size": size,
        "pageNo": pageNo,
    })
}

// 获取优惠券詳情數據
export function getCouponsDetail(couponId) {
    return request.post("/c_msh/mLife/coupon/detail", {
        couponId : Number(couponId)
    })
}


// 获取优惠券詳情推荐數據
export function getDetailrec(pageNo,pageSize=20,cityName = "北京",meiWeiIncomeFlag=1) {
    return request.post("/c_msh/mLife/goods/goodsQuery", {
        "pageNo": pageNo,
        "pageSize": pageSize,
        "cityName": cityName,
        "meiWeiIncomeFlag": meiWeiIncomeFlag,
    })
}
