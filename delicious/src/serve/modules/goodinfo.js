import request from '@/utils/request'

export function getGoodInfo(id) { 
    return request.post('/c_msh/mLife/goods/goodsDetail', {
        "goodId": Number(id),
        "shareId": "",
        "shareUserId": null,
    })
}