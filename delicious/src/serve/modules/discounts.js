import requset from '@/utils/request'
// 获取数据
export const getAllList = () => {
    return requset.post('/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&&m=zofui_sales&sign=282de35e7506c6d626cb33b5d800def9 HTTP/1.1',
        {
            op: "info",
            lat: 39.95933,
            lng: 116.29845,
            zfid: 0,
            isnew: 1,
            isposter: 0,
            shopid: 0,
            cityId: 19,
            mwtoken: "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtfpT6UJJP1iltJYHbVX-CQ9JpQuiTGZSGJCejzpMgHY-B0KpP8zxXeNc0TSZoBgh5Qn18ik8Y-B1BaRuHmRLCG4fJrysIfj478kTzx8HWHnHzgzeOozU3XFPpfNf51Y5sKzJg7vrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
            from: "wxapp"
        }, {
        headers: {
            "content-type": "application/x-www-form-urlencoded"
        }
    }
    )
}
// 获取tab数据
export const getTabList = () => {
    return requset.post('/basic/pageConfig/tabs', {
        "showPage": 1,
        "platform": 0,
    })
}
// 获取列表数据
export const getShopList = (params) => {
    return requset.post('/c_msh/mLife/goods/list/queryByTab', {
        "showPage": 1,
        "platform": 0,
        "longitude": 116.29845,
        "latitude": 39.95933,
        ...params
    })
}