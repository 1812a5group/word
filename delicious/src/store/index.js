import Vue from 'vue'
import VueX from 'vuex'
import Logger from 'vuex/dist/logger'
// 模块
import my from './modules/my'
import index from './modules/index'
import discounts from './modules/discounts'
import goodinfo from './modules/goodinfo'

Vue.use(VueX)

export default new VueX.Store({
    modules: {
        my,
        index,
        discounts,
        goodinfo
    },
    plugins: [Logger()]
})