import {getAllList,getTabList,getShopList} from '@/serve'
const state = {
    dataObj:{},
    tabList:[],
    shopParams:{
        "cityId": 19,
        "tabId": 328,
        "showSort": 1,
        "showSortMode": 1,
        "pageNo": 1,
        "pageSize": 10,
    },
    totalCount:0,
    shopList:[]
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }

}

const actions = {
    // 获取数据
    async getAllList({commit},payload){
        let result = await getAllList()
        console.log(result.data,"数据-------");
        if(result.errno===0){
            commit('update', {
                dataObj: result.data
            })
        }
    },
    // 获取tab数据
    async getTabList({commit},payload){
        let result = await getTabList()
        console.log(result,'tabs');
        if(result.errNo===0){
            commit('update',{
                tabList:result.data
            })
        }
    },
    // 获取商品数据
    async getShopList({commit,state},payload){
        let result = await getShopList(payload)
        console.log('getShopList',result,);
        if(result.errNo===0){
            let shopList = result.data.data
            
            if(payload.pageNo>1){
                shopList=[...state.shopList,...shopList]
            }
            commit('update',{
                shopList,
                totalCount: result.data.totalCount
            })
        }
    }
}

const getters = {
    
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters
}