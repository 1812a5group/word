import {
    getBanners,
    getWidget,
    getOperatList,
    getTabs,
    getQueryByTab,
    getSwitch
} from '@/serve'
const state = {
    bannerData : [],
    widgetData : [],
    operatList : [],
    tabsData : [],
    shopData:[],
    shopCount:0,
    shopQuery: {
        "tabId": 41,
        "showSort": 1,
        "showSortMode": 1,
        "pageNo": 1,
        "pageSize": 10,
    },
    witchData : {}
};

const getters = {};

const mutations = {
    updata(state,payload) {
        for(let key in payload) {
            state[key] = payload[key]
        }
    }
}

const actions = {
    // 轮播数据
    async getBanner({commit},payload) {
        let res = await getBanners(); 
        console.log("轮播图",res.data);
        if(res.errNo === 0) {
            commit('updata',{
                bannerData : res.data
            })
        }
    },
    // 轮播图下面分类数据
    async getWidget({commit},payload) {
        let res = await getWidget();
        if(res.errNo === 0) {
            commit('updata',{
                widgetData : res.data.items
            })
        }
    },
    // 限时特惠数据
    async getOperatList({commit,payload}) {
        let res = await getOperatList();
        if(res.errNo === 0) {
            commit('updata',{
                operatList : res.data
            })
        }
    },
    // tab数据
    async getTabs({commit},payload) {
        let res = await getTabs();
        if(res.errNo === 0) {
            commit('updata',{
                tabsData : res.data
            })
        }
    },
    // tab列表
    async getQueryByTab({commit,state},payload) {
        console.log(state.shopQuery.pageNo,'pageNo');
        let res = await getQueryByTab(state.shopQuery);
        if(res.errNo === 0) {
            let shopData = res.data.data;
            if (state.shopQuery.pageNo > 1){
                shopData = [...state.shopData, ...shopData];
            }
            console.log(shopData,'showdata');
            commit('updata',{
                shopData:shopData,
                shopCount: res.data.totalCount
            })
        }
    },

    // 搜索词
    async getSwitch({commit,state},payload) {
        let res = await getSwitch();
        if(res.errNo === 0) {
            commit('updata',{
                witchData : res.data
            })
        }
        console.log(state.witchData,'witchData');
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}