import {
    getuserid,
    getMyCoupons
} from '@/serve'


const state = {
    // 用户信息
    userdata: {},
    // 优惠券
    couponsList: []
}

const getters = {

}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 获取用户信息
    async getuserid({
        commit
    }) {
       
        let res = await getuserid();
        if (res.errNo === 0) {
            commit("update", {
                userdata: res.data
            })
        }
    },
    async getMyCoupons({
        commit
    }, payload) {
        let res;
        if (!payload) {
            res = await getMyCoupons();
        } else {
             res = await getMyCoupons(payload.type);
        }
        if (res.errNo === 0) {
            commit("update", {
                couponsList: res.data
            })
        }
    }

};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};