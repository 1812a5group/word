import { getGoodInfo } from '@/serve'


const state = {
    goodInfo: {}
}

const getters = {

}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 获取用户信息
    async getGoodInfo({ commit },payload) {
        let result = await getGoodInfo(payload);
        console.log("详情数据result...", result.data);
        if (result.errNo === 0) {
            commit("update", {
                goodInfo: result.data
            }) 
        }
       
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};