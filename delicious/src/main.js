import Vue from 'vue'
import App from './App'
import store from './store'
import uView from 'uview-ui';
import * as filters from '@/filters';


// 引入过滤器
for (let filter in filters) {
  Vue.filter(filter, filters[filter]);
}
Vue.use(uView);
Vue.config.productionTip = false
App.mpType = 'app'


const app = new Vue({
  store,
  ...App
})

app.$mount()
