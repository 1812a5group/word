import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  title: "小楼又清风",
  fastRefresh: { },
  antd: { },
  dva: {
    immer: true,
    hmr: true,
  },
  locale: {},
  //配置资源打包路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812A/FiveGroups/fantasticit/' : '/',    //
  //配置路由路径
  base: process.env.NODE_ENV === 'production' ? '/1812A/FiveGroups/fantasticit' : '/',
  //路由懒加载
  dynamicImport: {
    loading: '@/components/Loading',
  },
  //配置文件hash后缀，使用增量发布策略
  hash:true,
});
