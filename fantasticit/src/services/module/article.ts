import { request } from 'umi';

// 获取文章列表
export function getArticleList(page: number, pageSize=12, status='publish'){
    return request('/api/article', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}

//获取文章分类列表
export function getArticleClassify(Classify:string,page: number, pageSize=12, status='publish'){
    return request(`/api/article/category/${Classify}`, {
        params: {
            page,
            pageSize,
            status,
        }
    })
}
