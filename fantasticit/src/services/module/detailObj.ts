import { request } from 'umi';

// 获取文章详情
export function getArticleDetail(id: string) {
    return request(`/api/article/${id}/views`, {
        method: 'POST'
    })
}