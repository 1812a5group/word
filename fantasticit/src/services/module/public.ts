import { IPostItem } from '@/types';
import { request } from 'umi';

// 获取推荐阅读的数据
export function getRecommend() {
    return request('/api/article/recommend')
}

//详情页右侧推荐阅读
export function getArticleDetailRecommend(id: string) {
    return request(`/api/article/recommend?articleId=${id}`)
}
//获取文章标题
export function getcAtegoryTitle(status = 'publish') {
    return request('/api/category', {
        params: {
            status,
        }
    })
}

//文章分类
export function getTagData() {
    return request('/api/tag')
}

//文章分类
export function getTagClassify(Classify: string, page: number, pageSize = 12, status = 'publish') {
    return request(`/api/article/tag/${Classify}`, {
        params: {
            page,
            pageSize,
            status,
        }
    })
}


// 生成海报
export function genePoster(data: IPostItem) {
    return request(`/api/poster`, {
        method: 'POST',
        data
    })
}


//喜欢
export function ChangeLike(id:string,url:string,type: string) {
    return request(`/api/${url}/${id}/likes`, {
        method: 'POST',
        data: {
            type
        }
    })
}
