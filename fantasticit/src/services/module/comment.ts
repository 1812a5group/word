
import { request } from 'umi'
//获取留言板评论
export function getCommentList(page: number, id: string, pageSize = 6) {
    return request(`/api/comment/host/${id}`, {
        params: {
            page,
            pageSize
        }
    })
}

//获取关于文章内容
export function getView(id: string) {
    return request(`/api/page/${id !== undefined ? id : ''}`)
}

//发布留言板 | 回复评论
export function postMessage(
    content: string,
    email: string,
    hostId: string,
    name: string,
    url: string,
    parentCommentId: string,
    replyUserEmail: string,
    replyUserName: string,
) {
    return request('/api/comment', {
        method: "POST",
        data: {
            content,
            email,
            hostId,
            name,
            url,
            parentCommentId,
            replyUserEmail,
            replyUserName
        }
    })
}

//获取留言板信息
export function getMsgboard() {
    return request('/api/page/msgboard')
}
//获取关于信息
export function getAbout() {
    return request('/api/page/about')
}