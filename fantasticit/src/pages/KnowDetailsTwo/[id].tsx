import React, { useRef, useState, useEffect } from 'react'
import { useDispatch, useIntl } from 'umi'
import {
    IRootState, MaskItem
} from '@/types'
import { RouteComponentProps } from "react-router-dom"
import { useSelector } from 'react-redux'
import style from './index.less'
import "../../less/markdown.less"
import { ChangeLike } from '@/services'
import moment from "moment"
import Cls from 'classnames'
import ImageView from '@/components/ImageView'
import ShowMask from '@/components/ShareMaskcopy'
import DelitePage from '@/components/DelitePage'
import MessageBoard from '@/components/MessageBoard'
import CommentItem from '@/components/CommentItems'
import Pagina from '@/components/Paginas'
import Directory from '@/components/Directory'
import HighLight from '@/components/HighLight'


// const ImageViews = React.memo(ImageView)
const DelitePages = React.memo(DelitePage)

interface State {
    title: string,
    parentNodeId: string
    item: MaskItem
}
interface Params {
    id: string
}
interface Context {

}
interface Props extends RouteComponentProps<Params, Context, State> {
    title: string,
    parentNodeId: string
    item: MaskItem
}

const fe: React.FC<Props> = (props) => {
    const intl = useIntl();
    const conScroll_left: any = useRef();
    const markdown: any = useRef();
    const dispatch = useDispatch();
    const [list, settList] = useState([]);
    const [likeFlag, settlikeFlag] = useState(false);
    // let [detailsPage, setDetailsPage] = useState("");
    const [page, setPage] = useState<number>(1);
    const [ind, setInd] = useState(0);
    const { commentList, commentTotal, getMsgboard } = useSelector((state: IRootState) => state.comments);
    const { knowledgedetailsTowdata, knowledgedetails } = useSelector((state: IRootState) => state.knowledge);
    const id = props.match.params.id;
    const locationState = props.location.state;
    // 获取文章详情
    useEffect(() => {
        dispatch({
            type: 'knowledge/getknowledgedetailsTowdata',
            payload: id
        })
        dispatch({
            type: "knowledge/getknowledgedetailsdata",
            payload: locationState.parentNodeId
        })
    }, [id, locationState.parentNodeId]);

    useEffect(() => {
        knowledgedetailsTowdata.toc && settList(JSON.parse(knowledgedetailsTowdata.toc))
    }, [knowledgedetailsTowdata]);

    //留言板评论
    useEffect(() => {
        dispatch({
            type: 'comments/getCommentList',
            payload: {
                page,
                id: knowledgedetailsTowdata.id
            }
        })
    }, [page, knowledgedetailsTowdata])

    //页码
    let handleCurrent = (page: number) => {
        setPage(page)
    }

    //左边滚动高亮
    window.onscroll = () => {
        conScroll_left.current && [...conScroll_left.current!.querySelectorAll("h2,h3,h4")].forEach((item, index) => {
            if (document.documentElement.scrollTop + 20 >= item.offsetTop) {
                setInd(index)
            }
        })
    }

    //右边目录点击--置顶
    function del_scroll(index: number) {
        setInd(index)
        document.documentElement.scrollTop = [...conScroll_left.current!.querySelectorAll("h2,h3,h4")] && ([...conScroll_left.current!.querySelectorAll("h2,h3,h4")][index] as HTMLElement).offsetTop
    }
    useEffect(() => {
        if (ind === 0) {
            conScroll_left.current!.scrollTop = 0
        } else {
            conScroll_left.current!.scrollTop = ([...conScroll_left.current!.children[1].children!][ind] as HTMLElement)
            // .offsetHeight * ind + 10
        }
    }, [ind])
    //喜欢
    useEffect(() => {
        let likeData = localStorage.getItem('LIKES')
        if (likeData) {
            let likes = JSON.parse(likeData)
            let flag = likes.findIndex((item: string) => item === id)
            if (flag != -1) {
                settlikeFlag(true)
            } else {
                settlikeFlag(false)
            }
        }
    }, []);
    //点击喜欢
    async function likeBtn() {
        let likeData = localStorage.getItem('LIKES')
        let arr: string[] = []
        likeData ? arr = JSON.parse(likeData) : arr
        if (arr.indexOf(id) === -1) {
            arr.push(id)
            localStorage.setItem('LIKES', JSON.stringify(arr))
            settlikeFlag(true)
            await ChangeLike(id, 'knowledge', 'like')
        } else {
            arr.splice(arr.indexOf(id), 1)
            localStorage.setItem('LIKES', JSON.stringify(arr))
            settlikeFlag(false)
            await ChangeLike(id, 'knowledge', 'dislike')
        }
        dispatch({
            type: 'knowledge/getknowledgedetailsTowdata',
            payload: id
        })
    }

    return (
        <div className={Cls(style.detail, 'container')}>
            <div className={Cls(style.jumptitle, 'container')}>
                <span onClick={() => props.history.push(`/knowledge`)} className={style.jupm}> 知识小测 </span> /
                <span className={style.jupm} onClick={() => props.history.push(`/KnowDetails/${props.location.state.parentNodeId}`)}> {locationState.title} </span> /
                {
                    knowledgedetails.map((item, index) => {
                        return item.id === id && <span key={index} className={item.id === id ? style.on : null}> {item.title} </span>
                    })
                }
            </div>

            <div className={Cls(style.icons, 'icons')}>
                <div onClick={() => likeBtn()} className={Cls(likeFlag ? style.likeActive : '')}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z" fill="currentColor"></path></svg>
                    {
                        knowledgedetailsTowdata.likes != 0 && <span>{knowledgedetailsTowdata.likes!}</span>
                    }
                </div>
                <div onClick={() => window.scrollTo(0, document.body.scrollHeight)}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z" fill="currentColor"></path></svg>
                </div>
                <div onClick={() => {
                    ShowMask(props.location.state.item);
                    // setItemShare()
                }}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z" fill="currentColor"></path></svg>
                </div>
            </div>
            <div className='container'>
                <div className={Cls(style.detail_left, 'index_left')}>
                    <div className={style.detail_content} ref={conScroll_left}>
                        <HighLight>
                            <div className={style.header}>
                                <ImageView>
                                    <img src={knowledgedetailsTowdata.cover ? knowledgedetailsTowdata.cover : ""} alt="" />
                                </ImageView>
                                <h1>{knowledgedetailsTowdata.title}</h1>
                                <p>
                                    <span>{intl.formatMessage({ id: 'menu.publishAt' })}<time>{moment(knowledgedetailsTowdata.createAt).format("YYYY-MM-DD HH:mm:ss")}</time></span>
                                    <span> • </span>
                                    <span>{intl.formatMessage({ id: 'menu.reading' })} {knowledgedetailsTowdata.views}</span>
                                </p>
                            </div>
                            <div className="markdown" ref={markdown}>
                                <div dangerouslySetInnerHTML={{ __html: knowledgedetailsTowdata.html! }}></div>
                            </div>
                        </HighLight>
                        <div className={style.content_bottom}>
                            <div className={style.con_first}>
                                {intl.formatMessage({ id: 'menu.publishAt' })}{moment(knowledgedetailsTowdata.publishAt).format("YYYY-MM-DD HH:mm:ss")} | {intl.formatMessage({ id: 'menu.copyright' })}
                            </div>

                            <div className={style.delitePage}>
                                {/* 分页效果的跳转路由 */}
                                <DelitePages knowledgedetails={knowledgedetails} id={id} item={props.item} />
                            </div>
                            <div>
                                <div className={style.con_btn}>
                                    <span role="img" aria-label="tag" className="anticon anticon-tag">
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="tag" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                            <path d="M938 458.8l-29.6-312.6c-1.5-16.2-14.4-29-30.6-30.6L565.2 86h-.4c-3.2 0-5.7 1-7.6 2.9L88.9 557.2a9.96 9.96 0 000 14.1l363.8 363.8c1.9 1.9 4.4 2.9 7.1 2.9s5.2-1 7.1-2.9l468.3-468.3c2-2.1 3-5 2.8-8zM459.7 834.7L189.3 564.3 589 164.6 836 188l23.4 247-399.7 399.7zM680 256c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm0 120c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path>
                                        </svg>
                                    </span>
                                    <span style={{ paddingLeft: "10px" }}>JS</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    {/* 评论组件 */}
                    <div className={style.main}>
                        <p className={style.comment}>{intl.formatMessage({ id: 'menu.comment' })}</p>
                        {/* <Message /> */}
                        {/* 评论框 */}
                        <MessageBoard id={getMsgboard.id} path={props.match.url.slice(1)} />
                        {/* 留言板 */}
                        {commentList && <CommentItem path={props.match.url.slice(1)} list={commentList} id={knowledgedetailsTowdata.id!} />}
                        {/* 分页 */}
                        {
                            commentList.length > 0 && <Pagina
                                total={commentTotal}
                                handleCurrent={handleCurrent}
                                page={page}
                            />
                        }
                    </div>

                </div>
                <div className={Cls(style.detail_right, 'index_right')}>
                    <div className='sticky'>
                        <div className={style.right_content}>
                            <div className={style.knowledge_rigth_top}>
                                {locationState.title}
                            </div>
                            <div className={style.knowledge_rigth_main}>
                                {
                                    knowledgedetails.map((item, index) => {
                                        return <li key={index}
                                            onClick={() => {
                                                props.history.push({
                                                    pathname: `/KnowDetailsTwo/${item.id}`,
                                                    state: {
                                                        title: item.title,
                                                        parentNodeId: item.parentId,
                                                        item: props.item
                                                    }
                                                })
                                            }}
                                            className={item.id === id ? style.on : null}
                                        > {item.title} </li>
                                    })
                                }
                            </div>
                        </div>

                        {/* 目录 */}
                        <Directory directoryList={list} ind={ind} del_scroll={del_scroll}></Directory>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default fe;
