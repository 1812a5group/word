import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
import ScrollAdd from '@/components/ScrollAdd'

interface Params {
  id: string
}

interface Props extends RouteComponentProps<Params> {

}

const CategoryAll: React.FC<Props> = (props) => {
  const dispatch = useDispatch()
  const [page, setpage] = useState(1)
  const { ArticleClassify, ArticleClassifyNum } = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    dispatch({
      type: 'article/getArticleClassify',
      Classify: props.match.params.id,
      payload: page
    })
  }, [page])

  const fetchMoreData = () => {
    setpage(page => page + 1)
  }
  return (
    <ScrollAdd ArticleList={ArticleClassify} ArticleNum={ArticleClassifyNum} page={page} fetchMoreData={fetchMoreData} />
  )
}
export default CategoryAll
