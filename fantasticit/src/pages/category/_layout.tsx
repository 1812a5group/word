import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink, Redirect } from 'umi';
import { RouteComponentProps } from 'react-router-dom'
import style from './index.less';
import Cln from 'classnames'
import ScrollAdd from '@/components/ScrollAdd'
import RecommendedReading from '@/components/RecommendedReading'
import ArticleTag from '@/components/ArticleTag'
interface Props extends RouteComponentProps {

}

const category: React.FC<Props> = (props) => {
    let [Ind, setInd] = useState(0)
    const { recommend, ategoryTitle, TagData, ArticleClassifyNum } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        let name = props.location.pathname.substr(10)
        let num = ategoryTitle.findIndex(item => item.value === name)
        setInd(num)
    }, [ategoryTitle])
    return (
        <div className={Cln(style.category, style.index)}>
            <div className={Cln(style.container, 'container')}>
                <div className={Cln(style.index_left, 'index_left')}>
                    <div className={style.category_title}>
                        <p><span>{ategoryTitle[Ind] && ategoryTitle[Ind].label}</span>分类文章</p>
                        <p className={style.Count}>共搜索到<span className={style.num}>{ArticleClassifyNum}</span>篇</p>
                    </div>
                    <div className={style.list}>
                        <div className={style.list_title}>
                            <NavLink to='/'>所有</NavLink>
                            {props.location.pathname === '/category' ? <Redirect exact from='/category' to='/category/fe' /> : ''}
                            {
                                ategoryTitle.map((item, index) => {
                                    return <a href={`/category/${item.value}`} key={item.id} className={Ind === index ? style.active : ''} onClick={(e) => {
                                        e.preventDefault();
                                        setInd(index)
                                        props.history.push(`/category/${item.value}`)
                                    }}>
                                        {item.label}
                                    </a>
                                })
                            }

                        </div>
                        <div>
                            {props.children}
                        </div>
                    </div>
                </div>

                <div className={Cln(style.index_right, 'index_right')}>
                    <div className='sticky'>
                        {/* 文章推荐 */}
                        <RecommendedReading infor={recommend} />
                        {/* 文章标签 */}
                        <ArticleTag TagData={TagData} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default category
