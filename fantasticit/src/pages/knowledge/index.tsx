import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import RecommendedReading from '@/components/RecommendedReading'
import ClassifiCation from '@/components/ClassifiCation'
import { IRootState } from '@/types'
import Cls from 'classnames'
import style from './index.less'
import { useDispatch, useSelector } from 'react-redux'
import ShowMask from '@/components/ShareMaskcopy'
import moment from '@/utils/moment'
import Loading from '@/components/Loading'

interface Props extends RouteComponentProps {

}
const knowledge: React.FC<Props> = (props) => {
    // 分享组件控制显示隐藏
    let [maskFlag, setMaskFlag] = useState(false);
    // 分享组件数据
    let [itemShare, setItemShare] = useState({ });
    const dispatch = useDispatch();
    //列表数据
    const { knowledgeData } = useSelector((state: IRootState) => state.knowledge)
    // 推荐阅读 | 文章标签
    const { recommend, ategoryTitle } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: "knowledge/getknowledges"
        })
    }, [])
    let setflag = (flag: boolean) => {
        setMaskFlag(flag);
    }
    //跳转详一级情页
    let jumpdetails = (id: string) => {
        props.history.push("/KnowDetails/" + id)
    }
    return (
        <div className={Cls(style.knowledge, 'container')}>
            <div className={Cls(style.knowledge_left, 'index_left')}>
                {
                    knowledgeData.length > 0 ? knowledgeData?.map((item, index) => {
                        return <div
                            key={index}
                            className={Cls(style.item, index !== 0 ? style.itemtop : '')}
                            onClick={() => jumpdetails(item.id)}
                        >
                            <div className={style.item_title}>
                                <b>
                                    {item.title}
                                </b>
                                <span style={{ color: "#999" }}> {moment(item.createAt).toNow()} </span>
                            </div>
                            <div className={style.item_content}>
                                <div className={style.header}><img src={item.cover} alt="" /></div>
                                <div className={style.fooler}>
                                    <p className={style.summary}> {item.summary} </p>
                                    <p className={style.views}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg>{item.views}
                                        {/* api组件分享方法 */}
                                        <span onClick={(e) => {
                                            e.stopPropagation(),
                                                ShowMask(item)
                                        }}> <span className={style.segmentation}>·</span>  <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>  分享</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    }) : <Loading />
                }
            </div>
            <div className={Cls(style.knowledge_rigth, 'index_right')}>
                <div className='sticky'>
                    <div className={style.knowledge_rigth_top}>
                        {/* 推荐阅读 */}
                        {
                            recommend && <RecommendedReading infor={recommend} />
                        }
                    </div>
                    <div className={style.knowledge_rigth_bottom}>
                        {/* 文章标签 */}
                        {ategoryTitle && <ClassifiCation infor={ategoryTitle} />}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default knowledge
