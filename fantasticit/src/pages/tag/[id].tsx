import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import {RouteComponentProps} from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
import ScrollAdd from '@/components/ScrollAdd'

interface Params {
  id: string
}

interface Props extends RouteComponentProps<Params> {

}

const TagAll: React.FC<Props> = (props) => {
  const dispatch = useDispatch()
  const [page, setpage] = useState(1)
  const { TagClassify, TagClassifyNum } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getTagClassify',
      Classify: props.match.params.id,
      payload: page
    })
  }, [page])

  const fetchMoreData = () => {
    setpage(page => page + 1)
  }
  return (
    <ScrollAdd ArticleList={TagClassify} ArticleNum={TagClassifyNum} page={page} fetchMoreData={fetchMoreData} />
  )
}
export default TagAll
