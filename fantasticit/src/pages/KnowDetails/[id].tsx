import React, { useEffect, useState } from 'react'
import style from './index.less'
import Cls from 'classnames'
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { IRootState, MaskItem } from '@/types';
import moment from 'moment';
import ShowMask from '@/components/ShareMaskcopy';
import { useIntl } from '@/.umi/plugin-locale/localeExports';
interface Params {
    id: string
}
interface Props extends RouteComponentProps<Params> {

}

const KnowledgeDetails: React.FC<Props> = (props) => {
    const dispatch = useDispatch()
    const intl = useIntl();
    let [itemMask, setItemMask] = useState({ })
    const { knowledgeData, knowledgedetails } = useSelector((state: IRootState) => state.knowledge)
    let arr = knowledgeData.filter(item => item.id === props.match.params.id)
    useEffect(() => {
        dispatch({
            type: "knowledge/getknowledges"
        })
        dispatch({
            type: "knowledge/getknowledgedetailsdata",
            payload: props.match.params.id
        })
    }, [props.match.params.id])
    return (
        <div className={Cls(style.KnowledgeDetails, 'container')} style={{
            backgroundImage: `url(${knowledgeData.map((item, index) => {
                return item.id === props.match.params.id && item.cover
            })})` + " none"
        }}>
            <div className={style.details_content}>
                <div className={style.details_title}>
                    <i onClick={() => props.history.push(`/knowledge`)} className={style.jupm}> 知识小测 </i>  <span>/</span>    {knowledgeData.map((item, index) => {
                        return item.id === props.match.params.id && <i key={index}> {item.title} </i>
                    })}
                </div>

                <div className={style.layout}>
                    <div className={Cls(style.content_left, 'index_left')}>
                        {knowledgeData.length > 0 && knowledgeData.map((item, index) => {
                            return item.id === props.match.params.id && <div key={index} className={Cls(style.content_left_nr)}>
                                <h3 className={style.title}>{item.title} </h3>
                                <div className={style.content_left_nr_chunk}>
                                    <img src={item.cover} alt="" />
                                    <h3>{item.title} </h3>
                                    <p>{item.summary}</p>
                                    <p style={{ color: "#ccc" }}>{item.views}次阅读  {new Date(item.createAt).toLocaleString()}</p>
                                    <p> <button className={style.btn} onClick={() => {
                                        setItemMask(item)
                                        knowledgedetails.length > 0 && props.history.push({
                                            pathname: `/KnowDetailsTwo/${knowledgedetails[0].id}`,
                                            state: {
                                                title: item.title,
                                                parentNodeId: item.id,
                                                item: item
                                            }
                                        })
                                    }}>{intl.formatMessage({ id: 'menu.startReading' })} </button> </p>
                                </div>
                            </div>
                        })}

                        <div className={style.content_bottom_list}>
                            {knowledgedetails && knowledgedetails.map((ite, index) => {
                                return <li key={index} onClick={() => {
                                    knowledgedetails && props.history.push({
                                        pathname: `/KnowDetailsTwo/${ite.id}`,
                                        state: {
                                            title: arr[0].title,
                                            parentNodeId: arr[0].id,
                                            item: itemMask
                                        }
                                    })
                                }}> <span> {ite.title} </span> <b>{new Date(ite.createAt!).toLocaleString()} &gt; </b> </li>
                            })}
                        </div>
                    </div>
                    <div className={Cls(style.content_right, 'index_right')}>
                        <div className={Cls(style.content_right_nr, 'sticky')}>
                            <h3>{intl.formatMessage({ id: 'menu.okb' })}</h3>
                            <div className={style.other}>
                                {knowledgeData.map((item, index) => {
                                    return item.id !== props.match.params.id && <div key={index} className={style.other_item} onClick={() => {
                                        props.history.replace("/KnowDetails/" + item.id)
                                    }}>
                                        <div className={style.other_item_title}>
                                            {item.title} <span style={{ color: "#999" }}> {moment(item.createAt).toNow()} </span>
                                        </div>
                                        <div className={style.other_item_recommended}>
                                            <div className={style.other_item_img}> <img src={item.cover} alt="" /> </div>
                                            <div className={style.other_item_share}> <p className={style.item_summary}>{item.summary}</p>   <p className={style.views}>
                                                <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg>{item.views}
                                                <span onClick={(e) => {
                                                    e.stopPropagation(),
                                                        ShowMask(item)
                                                }} className={style.littleHands}> <span className={style.segmentation}>·</span>  <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>  分享</span>
                                            </p></div>
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default KnowledgeDetails
