import React, { ReactElement, useRef, useEffect, useState } from 'react'
import { useDispatch, useHistory, useIntl } from 'umi'
import { useSelector } from 'react-redux'
import { RouteComponentProps } from "react-router-dom"
import { IArticleDetail, IRootState } from '@/types'
import moment from "moment"
import { Modal } from 'antd'
import style from './index.less'
import './index.less'
import Cls from 'classnames'
import { ChangeLike } from '@/services'
// 组件
import RecommendedReading from '@/components/RecommendedReading'
import ImageView from '@/components/ImageView'
import MessageBoard from '@/components/MessageBoard'
import HighLight from '@/components/HighLight'
import Directory from '@/components/Directory'
import Pagina from '@/components/Paginas'
import CommentItem from '@/components/CommentItems'
import ShowMask from '@/components/ShareMaskcopy'

const ArticleDetails: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
    const [list, settList] = useState([])
    const [likeFlag, settlikeFlag] = useState(false)
    const [ind, setInd] = useState(0)
    const { articleDetail, articleDetailRecommend } = useSelector((state: IRootState) => state.archives);
    const { commentList, commentTotal, getMsgboard } = useSelector((state: IRootState) => state.comments);
    const intl = useIntl();
    const [page, setPage] = useState<number>(1)
    const dispatch = useDispatch();
    const id = props.match.params.id
    const markdown: any = useRef()
    const conScroll_left: any = useRef()
    const [visible, setVisible] = React.useState(true);
    //喜欢
    useEffect(() => {
        let likeData = localStorage.getItem('LIKES')
        if (likeData) {
            let likes = JSON.parse(likeData)
            let flag = likes.findIndex((item: string) => item === id)
            if (flag != -1) {
                settlikeFlag(true)
            } else {
                settlikeFlag(false)
            }
        }
    }, []);
    // 获取文章详情
    useEffect(() => {
        dispatch({
            type: 'archives/getArticleDetail',
            payload: id
        })
        dispatch({
            type: 'archives/getArticleDetailRecommend',
            payload: id
        })
        dispatch({
            type: 'archives/getArticleDetailLabel',
            payload: id
        })
    }, [id]);

    //处理toc数据
    useEffect(() => {
        articleDetail.toc && settList(JSON.parse(articleDetail.toc))
    }, [articleDetail]);

    //左边滚动高亮
    window.onscroll = () => {
        conScroll_left.current && [...conScroll_left.current!.querySelectorAll("h2,h3,h4")].forEach((item, index) => {
            if (document.documentElement.scrollTop + 20 >= item.offsetTop) {
                setInd(index)
            }
        })
    }

    //右边目录点击--高亮置顶
    function del_scroll(index: number) {
        setInd(index)
        document.documentElement.scrollTop = [...conScroll_left.current!.querySelectorAll("h2,h3,h4")] && ([...conScroll_left.current!.querySelectorAll("h2,h3,h4")][index] as HTMLElement).offsetTop
    }

    //留言板评论
    useEffect(() => {
        dispatch({
            type: 'comments/getCommentList',
            payload: {
                page,
                id: articleDetail.id
            }
        })
    }, [page, articleDetail])
    //页码
    let handleCurrent = (page: number) => {
        setPage(page)
    }




    //点击喜欢
    async function likeBtn() {
        let likeData = localStorage.getItem('LIKES')
        let arr: string[] = []
        likeData ? arr = JSON.parse(likeData) : arr
        if (arr.indexOf(id) === -1) {
            arr.push(id)
            localStorage.setItem('LIKES', JSON.stringify(arr))
            settlikeFlag(true)
            await ChangeLike(id, 'article', 'like')
        } else {
            arr.splice(arr.indexOf(id), 1)
            localStorage.setItem('LIKES', JSON.stringify(arr))
            settlikeFlag(false)
            await ChangeLike(id, 'article', 'dislike')
        }
        dispatch({
            type: 'archives/getArticleDetail',
            payload: id
        })
    }
    let pay = null
    console.log(articleDetail);
    
    // 弹出去付款消息

    if (articleDetail.totalAmount) {
        pay = <Modal
            width="400px"
            okText="立即支付"
            okType="danger"
            title="确认以下收费信息"
            visible={visible}
            onOk={async () => {
                
                let result = await fetch('http://127.0.0.1:7001/pay', {
                    method: 'POST',
                    headers:{
                        'content-type':'application/json'
                    },
                    body: JSON.stringify({ totalAmount: articleDetail.totalAmount, id: articleDetail.id })
                }).then(res => res.json());
                
                if (result.url) {
                    
                    window.location.href = result.url;
                }

            }}
            onCancel={async () => {
                setVisible(false)
            }}
        >
            <p style={{ textAlign: "center" }}>{`支付金额:￥${articleDetail.totalAmount}`}</p>
        </Modal>
    }

    return (
        <div className={Cls(style.detail, 'container')} ref={conScroll_left} onClick={() => document.body.offsetTop}>
            <div className={Cls(style.icons, 'icons')}>
                <div onClick={() => likeBtn()} className={Cls(likeFlag ? style.likeActive : '')}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z" fill="currentColor"></path></svg>
                    {
                        articleDetail.likes != 0 && <span>{articleDetail.likes!}</span>
                    }
                </div>
                <div onClick={() => { window.scrollTo(0, document.body.scrollHeight) }}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z" fill="currentColor"></path></svg>
                </div>
                <div onClick={() => ShowMask(articleDetail as IArticleDetail)}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z" fill="currentColor"></path></svg>
                </div>
            </div>
            <div className={Cls(style.detail_left, 'index_left')}>
                <HighLight>
                    <div className={style.detail_content} >
                        <div className={style.header}>
                            <ImageView>
                                <img src={articleDetail.cover ? articleDetail.cover : ""} alt="" />
                            </ImageView>
                            <h1>{articleDetail.title}</h1>
                            <p>
                                <span>{intl.formatMessage({ id: 'menu.publishAt' })}<time>{moment(articleDetail.createAt).format("YYYY-MM-DD HH:mm:ss")}</time></span>
                                <span> • </span>
                                <span>{intl.formatMessage({ id: 'menu.reading' })} {articleDetail.views}</span>
                            </p>
                        </div>

                        <div className="markdown" ref={markdown} dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div>

                        <div className={style.content_bottom}>
                            <div className={style.con_first}>
                                {intl.formatMessage({ id: 'menu.publishAt' })}{moment(articleDetail.publishAt).format("YYYY-MM-DD HH:mm:ss")} | {intl.formatMessage({ id: 'menu.copyright' })}
                            </div>
                            <div>
                                <div className={style.con_btn}>
                                    <span role="img" aria-label="tag" className="anticon anticon-tag">
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="tag" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                            <path d="M938 458.8l-29.6-312.6c-1.5-16.2-14.4-29-30.6-30.6L565.2 86h-.4c-3.2 0-5.7 1-7.6 2.9L88.9 557.2a9.96 9.96 0 000 14.1l363.8 363.8c1.9 1.9 4.4 2.9 7.1 2.9s5.2-1 7.1-2.9l468.3-468.3c2-2.1 3-5 2.8-8zM459.7 834.7L189.3 564.3 589 164.6 836 188l23.4 247-399.7 399.7zM680 256c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm0 120c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path>
                                        </svg>
                                    </span>
                                    <span style={{ paddingLeft: "10px" }}>JS</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </HighLight>

                <div className={style.main}>
                    <p className={style.comment}>{intl.formatMessage({ id: 'menu.comment' })}</p>
                    {/* 评论框 */}
                    <MessageBoard id={getMsgboard.id} path={props.match.url.slice(1)} />
                    {/* 留言板 */}
                    {commentList && <CommentItem path={props.match.url.slice(1)} list={commentList} id={articleDetail.id!} />}
                    {/* 分页 */}
                    {
                        commentList.length > 0 && <Pagina
                            total={commentTotal}
                            handleCurrent={handleCurrent}
                            page={page}
                        />
                    }
                </div>

            </div>
            <div className={Cls(style.detail_right, 'index_right')}>
                <div className='sticky'>
                    <div className={style.right_content}>
                        <div className={style.knowledge_rigth_top}>
                            {
                                articleDetailRecommend && <RecommendedReading infor={articleDetailRecommend} />
                            }
                        </div>
                    </div>
                    {/* 目录 */}
                    <Directory directoryList={list} ind={ind} del_scroll={del_scroll}></Directory>

                </div>
            </div>
            {pay}
        </div >
    )
}

export default ArticleDetails;
