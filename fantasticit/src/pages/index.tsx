import { IRootState, ArticleItem } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink, useIntl } from 'umi';
import Cln from 'classnames'
import styles from './index.less';
import ScrollAdd from '@/components/ScrollAdd'
import RecommendedReading from '@/components/RecommendedReading'
import ArticleTag from '@/components/ArticleTag'
import Carousels from '@/components/Carousels'


const IndexPage: React.FC = (props) => {
  const [page, setpage] = useState(1)
  const dispatch = useDispatch()
  const intl = useIntl();
  const { recommend, ategoryTitle, ArticleList, ArticleNum, TagData } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page])
  const fetchMoreData = () => {
    setpage(page => page + 1)
  }
  return (
    <div className={Cln(styles.index)}>
      <div className={Cln(styles.container, 'container')}>
        <div className={Cln(styles.index_left, 'index_left')}>
          {/* 轮播图 */}
          <Carousels recommend={recommend} />
          {/* 文章列表 */}
          <div className={styles.list}>
            {/* 文章分类 */}
            <div className={styles.list_title}>
              <NavLink activeClassName={styles.active} to='/'>{intl.formatMessage({id:'menu.all'})}</NavLink>
              {
                ategoryTitle.map(item => {
                  return <NavLink key={item.id} to={`/category/${item.value}`} >{item.label}</NavLink>
                })
              }
            </div>
            {/* 下拉加载 */}
            <ScrollAdd ArticleList={ArticleList} ArticleNum={ArticleNum} page={page} fetchMoreData={fetchMoreData} />
          </div>
        </div>
        <div className={Cln(styles.index_right, 'index_right')}>
          <div className='sticky'>
            {/* 文章推荐 */}
            <RecommendedReading infor={recommend} />
            {/* 文章标签 */}
            <ArticleTag TagData={TagData} />
          </div>
        </div>
      </div>
    </div>
  );
}
export default IndexPage
