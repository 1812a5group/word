// 回复框
import { useState } from 'react'
import style from './index.less'
import { ChildItem, ModelsItem ,IRootState} from '@/types'
import classnames from 'classnames'
import { Popover } from 'antd'
import Popout from '@/components/Popout'
import { useDispatch, useIntl, useSelector } from 'umi'
interface Props {
    item: ChildItem | ModelsItem
    id: string
    path: string
    handleBackUp(id: string): void
}
const Inptextarea: React.FC<Props> = (props) => {
    const intl = useIntl();
    const dispatch = useDispatch()
    let { item, id, path, handleBackUp } = props
    let [content, setContent] = useState('')
    const [isModalVisible, setIsModalVisible] = useState(false);
    
    //点击设置信息按钮
    const onFinish = (values: any) => {
        window.localStorage.user = JSON.stringify(values)
        setIsModalVisible(false)
    };
    const onFinishFailed = (errorInfo: any) => {
    };
    // 弹框隐藏
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //点击发布按钮
    function handleBtn(item: ChildItem | ModelsItem) {
        let obj = {
            hostId: id,
            ...JSON.parse(window.localStorage.user),
            parentCommentId: item.parentCommentId === null ? item.id : item.parentCommentId,
            replyUserEmail: item.email,
            replyUserName: item.name,
            url: `/${path}`,
            content
        }
        dispatch({
            type: "comments/postMessage",
            payload: {
                ...obj
            }
        })
        setContent('')
        handleBackUp(item.id)
    }
    //渲染表情列表
    const contents = (
        <ul className={style.face} onClick={(e) => clickbrow(e)}><li>😀</li><li>😃</li><li>😄</li><li>😁</li><li>😆</li><li>😆</li><li>😅</li><li>😂</li><li>😉</li><li>😊</li><li>😇</li><li>😍</li><li>😘</li><li>😗</li><li>😚</li><li>😙</li><li>😋</li><li>😛</li><li>😜</li><li>😝</li><li>😐</li><li>😑</li><li>😶</li><li>😏</li><li>😒</li><li>😌</li><li>😔</li><li>😪</li><li>😴</li><li>😷</li><li>😵</li><li>😎</li><li>😕</li><li>😟</li><li>😮</li><li>😯</li><li>😲</li><li>😳</li><li>😦</li><li>😧</li><li>😨</li><li>😰</li><li>😥</li><li>😢</li><li>😭</li><li>😱</li><li>😖</li><li>😣</li><li>😞</li><li>😓</li><li>😩</li><li>😫</li><li>😡</li><li>😡</li><li>😠</li><li>😈</li><li>😺</li><li>😸</li><li>😹</li><li>😻</li><li>😼</li><li>😽</li><li>🙀</li><li>😿</li><li>😾</li><li>❤️</li><li>✋</li><li>✋</li><li>✌️</li><li>☝️</li><li>✊</li><li>✊</li><li>🐵</li><li>🐱</li><li>🐮</li><li>🐭</li><li>☕</li><li>♨️</li><li>⚓</li><li>✈️</li><li>⌛</li><li>⌚</li><li>☀️</li><li>⭐</li><li>☁️</li><li>☔</li><li>⚡</li><li>❄️</li><li>✨</li><li>🃏</li><li>🀄</li><li>☎️</li><li>☎️</li><li>✉️</li><li>✏️</li><li>✒️</li><li>✂️</li><li>♿</li><li>⚠️</li><li>♈</li><li>♉</li><li>♊</li><li>♋</li><li>♌</li><li>♍</li><li>♎</li><li>♏</li><li>♐</li><li>♑</li><li>♒</li><li>♓</li><li>✖️</li><li>➕</li><li>➖</li><li>➗</li><li>‼️</li><li>⁉️</li><li>❓</li><li>❔</li><li>❕</li><li>❗</li><li>❗</li><li>〰️</li><li>♻️</li><li>✅</li><li>☑️</li><li>✔️</li><li>❌</li><li>❎</li><li>➰</li><li>➿</li><li>〽️</li><li>✳️</li><li>✴️</li><li>❇️</li><li>©️</li><li>®️</li><li>™️</li><li>ℹ️</li><li>Ⓜ️</li><li>⚫</li><li>⚪</li><li>⬛</li><li>⬜</li><li>◼️</li><li>◻️</li><li>◾</li><li>◽</li><li>▪️</li><li>▫️</li></ul>
    );
    //设置回复框内容
    function clickbrow(e: React.MouseEvent<HTMLUListElement, MouseEvent>) {
        setContent(content + e.target.innerHTML)
    }
    return <div className={classnames(style.inp)}>
        <div className={style.terbox}>
            <textarea name="" id="" placeholder={intl.formatMessage({id:'menu.reply'})+ '  ' + item.name}
                onClick={() => setIsModalVisible(true)}
                value={content}
                onChange={(e) => {
                    setContent(e.target.value)
                }}
            ></textarea>
        </div>
        <div className={style.footer}>
            <div onClick={() => setIsModalVisible(true)}>

                <Popover className={style.left} placement="bottomLeft" content={contents} trigger={window.localStorage.user ? 'click' : ''}>
                    <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                        <path
                            fill="currentColor"
                            d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z">

                        </path>
                    </svg>
                    <span>{intl.formatMessage({id:'menu.reply'})}</span>
                </Popover >
            </div>
            <div className={style.right} >
                <button style={{ marginRight: "22px" }}
                    onClick={() => handleBackUp(item.id)}
                >
                    <span>{intl.formatMessage({id:'menu.close'})}</span>
                </button>
                <button onClick={() => handleBtn(item)}
                    className={content === '' ? '' : style.active}
                    disabled={content === ''}
                >
                    <span >{intl.formatMessage({id:'menu.send'})}</span>
                </button>
            </div>
        </div>
        {/* 登录弹框 */}
        {
            !window.localStorage.user && <Popout
                isModalVisible={isModalVisible}
                onFinish={onFinish}
                handleCancel={handleCancel}
                onFinishFailed={onFinishFailed}
            />
        }
    </div>


}
export default Inptextarea
