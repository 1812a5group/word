import React, { useState } from 'react'
import style from './index.less'
import { ArticleItem } from '@/types'
import InfiniteScroll from 'react-infinite-scroll-component';
import ArticleLists from '@/components/ArticleList'
// import ShareMask from '@/components/ShareMask'
import ShowMask from '@/components/ShareMaskcopy'
interface Props {
    ArticleList: ArticleItem[];
    ArticleNum: number;
    page: number;
    fetchMoreData: any
}

const ScrollAdd: React.FC<Props> = (props) => {
    // let [maskFlag, setMaskFlag] = useState(false)
    // let [itemShare, setItemShare] = useState({})
    // let setflag = (flag: boolean) => {
    //     setMaskFlag(flag);
    //     document.body.style.overflow = 'auto';
    //     document.body.style.width = 'calc(100%)'
    // }
    let showMask = (item: ArticleItem) => {
        // setMaskFlag(true);
        // setItemShare(item)
        ShowMask(item)
        document.body.style.overflow = 'hidden';
        document.body.style.width = 'calc(100% - 4px)'
    }
    return (
        <div className={style.list_content}>
            <InfiniteScroll
                dataLength={props.ArticleList.length}
                next={props.fetchMoreData}
                hasMore={props.ArticleNum > props.page * 12}
                loader={<h4>正在获取文章...</h4>}
            >
                <ArticleLists ArticleList={props.ArticleList} showMask={showMask} />
            </InfiniteScroll>
            {/* <ShareMask flag={maskFlag} item={itemShare} setflag={setflag} /> */}
            {props.ArticleList.length === 0 ? <div className={style.empty}>暂无数据</div> : ''}
        </div>
    )
}

export default ScrollAdd
