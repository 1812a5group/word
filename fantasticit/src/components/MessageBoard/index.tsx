// 发布评论
import React, { ChangeEvent, useEffect, useState } from 'react'
import Popout from '@/components/Popout'
import { Popover } from 'antd';
import { useDispatch, useIntl } from 'umi';
import style from './index.less'
interface Props {
    path?: string
    id: string
}
const MessageBoard: React.FC<Props> = (props) => {
    const intl = useIntl();
    let { path, id } = props
    const dispatch = useDispatch()
    // 控制弹框显示
    const [isModalVisible, setIsModalVisible] = useState(false);
    // 输入框的值
    const [content, setContent] = useState('');
    //设置信息
    const onFinish = (values: any) => {
        window.localStorage.user = JSON.stringify(values)
        setIsModalVisible(false)
    };
    const onFinishFailed = (errorInfo: any) => {
    };
    //隐藏弹框
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    // 发布评论按钮
    const handleComment = () => {
        dispatch({
            type: "comments/postMessage",
            payload: {
                ...JSON.parse(window.localStorage.user),
                hostId: id,
                content,
                url: '/' + path,
            }
        })
        setContent('')
    }
    // 渲染表情
    const contents = (
        <ul className={style.face} onClick={(e) => clickbrow(e)}><li>😀</li><li>😃</li><li>😄</li><li>😁</li><li>😆</li><li>😆</li><li>😅</li><li>😂</li><li>😉</li><li>😊</li><li>😇</li><li>😍</li><li>😘</li><li>😗</li><li>😚</li><li>😙</li><li>😋</li><li>😛</li><li>😜</li><li>😝</li><li>😐</li><li>😑</li><li>😶</li><li>😏</li><li>😒</li><li>😌</li><li>😔</li><li>😪</li><li>😴</li><li>😷</li><li>😵</li><li>😎</li><li>😕</li><li>😟</li><li>😮</li><li>😯</li><li>😲</li><li>😳</li><li>😦</li><li>😧</li><li>😨</li><li>😰</li><li>😥</li><li>😢</li><li>😭</li><li>😱</li><li>😖</li><li>😣</li><li>😞</li><li>😓</li><li>😩</li><li>😫</li><li>😡</li><li>😡</li><li>😠</li><li>😈</li><li>😺</li><li>😸</li><li>😹</li><li>😻</li><li>😼</li><li>😽</li><li>🙀</li><li>😿</li><li>😾</li><li>❤️</li><li>✋</li><li>✋</li><li>✌️</li><li>☝️</li><li>✊</li><li>✊</li><li>🐵</li><li>🐱</li><li>🐮</li><li>🐭</li><li>☕</li><li>♨️</li><li>⚓</li><li>✈️</li><li>⌛</li><li>⌚</li><li>☀️</li><li>⭐</li><li>☁️</li><li>☔</li><li>⚡</li><li>❄️</li><li>✨</li><li>🃏</li><li>🀄</li><li>☎️</li><li>☎️</li><li>✉️</li><li>✏️</li><li>✒️</li><li>✂️</li><li>♿</li><li>⚠️</li><li>♈</li><li>♉</li><li>♊</li><li>♋</li><li>♌</li><li>♍</li><li>♎</li><li>♏</li><li>♐</li><li>♑</li><li>♒</li><li>♓</li><li>✖️</li><li>➕</li><li>➖</li><li>➗</li><li>‼️</li><li>⁉️</li><li>❓</li><li>❔</li><li>❕</li><li>❗</li><li>❗</li><li>〰️</li><li>♻️</li><li>✅</li><li>☑️</li><li>✔️</li><li>❌</li><li>❎</li><li>➰</li><li>➿</li><li>〽️</li><li>✳️</li><li>✴️</li><li>❇️</li><li>©️</li><li>®️</li><li>™️</li><li>ℹ️</li><li>Ⓜ️</li><li>⚫</li><li>⚪</li><li>⬛</li><li>⬜</li><li>◼️</li><li>◻️</li><li>◾</li><li>◽</li><li>▪️</li><li>▫️</li></ul>
    );
    function clickbrow(e: React.MouseEvent<HTMLUListElement, MouseEvent>) {
        setContent(content + e.target.innerHTML)
    }
    return <div className={style.messagebord}>
        <div className={style.area}>
            <div className={style.tb}>
                <textarea name=""
                    onClick={() => setIsModalVisible(true)}
                    placeholder={intl.formatMessage({id:'menu.plc'})}
                    style={{ height: '142px', minHeight: "142px", maxHeight: '274px', overflowY: 'hidden', resize: 'none', width: "100%" }}
                    value={content}
                    onChange={(e) => {
                        setContent(e.target.value)
                    }}
                >
                </textarea>
            </div>
            <div className={style.footer}>
                <div onClick={() => setIsModalVisible(true)}>

                    <Popover className={style.left} placement="bottomLeft" content={contents} trigger={window.localStorage.user ? 'click' : ''}>
                        <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                            <path
                                fill="currentColor"
                                d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z">
                            </path>
                        </svg>
                        <span>{intl.formatMessage({id:'menu.emoji'})}</span>
                    </Popover >
                </div>
                <div className={style.right}>
                    <button className={content === '' ? '' : style.active}
                        onClick={handleComment}
                        disabled={content === ''}
                    >
                        <span>{intl.formatMessage({id:'menu.send'})}</span>
                    </button>
                </div>
            </div>

        </div>
        {/* 弹框 */}
        {
            !window.localStorage.user && <Popout
                isModalVisible={isModalVisible}
                onFinish={onFinish}
                handleCancel={handleCancel}
                onFinishFailed={onFinishFailed}
            />
        }
    </div>
}


export default MessageBoard;
