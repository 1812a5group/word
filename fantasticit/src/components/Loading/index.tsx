import style from './index.less'
export default function LOADING() {
    return <div className={style.loading}>
        <div className={style.waiting}>
            <ul>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
}
