//分页
import { Pagination } from 'antd';
import style from './index.less'
import './index.less'
interface Props {
    total:number,
    handleCurrent?:(page:number)=>void
    page:number
}
const Pagina:React.FC<Props> = (props) => {
    let {total,handleCurrent,page} = props
    return <div className={style.pagina}>
        <Pagination 
        onChange={handleCurrent}
        defaultCurrent={1} 
        total={total}
        pageSize={6}
        size="small"
        current={page}/>
    </div>
}
export default Pagina