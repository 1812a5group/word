import { useEffect, useRef, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useDispatch, useSelector } from 'umi';
import Header from '@/components/Header'
import Footer from '@/components/Footer'
import './index.less'
import 'antd/dist/antd.css'
import { IRootState } from '@/types';
const Layouts: React.FC<RouteComponentProps> = (props) => {
    let [searchFlag, setSearchFlag] = useState(false)
    let [gotopflag, setGotopflag] = useState(true)
    const dispatch = useDispatch()
    const { FuzzyData } = useSelector((state: IRootState) => state.knowledge)
    const mains: any = useRef(null)
    const gotop: any = useRef()
    const inpSearch: any = useRef(null)
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
        dispatch({
            type: 'article/getcAtegoryTitle',
        })
        dispatch({
            type: 'article/getTagData',
        })
    }, [])
    window.addEventListener("scroll", function (e: Event) {
        //返回顶部
        window.scrollY > 200 ? setGotopflag(false) : setGotopflag(true);
        //列表
        let uls: HTMLElement | null = document.querySelector('.ul___1UixA');
        if (uls) {
            let children = Array.from((uls as HTMLElement).children)
            children.forEach(item => {
                if (window.scrollY >= (item as HTMLElement).offsetTop - window.innerHeight) {
                    item.classList.add('active___K_5Ud')
                }
            })
        }
        //详情页分享
        let icons: HTMLElement | null = document.querySelector('.icons');
        if (icons) {
            window.scrollY > 200 ? icons.classList.add('active') : icons.classList.remove('active')
        }
    })
    //返回顶部
    function gotops(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        window.scrollTo(0, 0)
        gotop.current.style.display = "none"
    }
    // 搜索
    function changeValue() {
        dispatch({
            type: 'knowledge/getFuzzyData',
            payload: inpSearch.current.value
        })

    }
    // 搜索遮罩
    function setSearchFlags(flag: boolean) {
        setSearchFlag(flag)
        dispatch({
            type: 'knowledge/save',
            payload: {
                FuzzyData: []
            }
        })
    }

    return <div className='layouts'>
        {/* Header头部 */}
        <Header setSearchFlags={setSearchFlags} />
        {/* main 内容 */}
        <main className='layouts_main' ref={mains}>
            {props.children}
        </main>
        {/* Footer底部 */}
        <Footer />
        {/* 返回顶部 */}
        <div className="gotop" onClick={(e) => {
            gotops(e)
        }} ref={gotop} style={{ display: gotopflag ? "none" : "block" }}>
            <div className="ant-fade-enter ant-fade-enter-active ant-fade ant-back-top-content">
                <div className="ant-back-top-icon">
                    <span role="img" aria-label="vertical-align-top" className="anticon anticon-vertical-align-top">
                        <svg viewBox="64 64 896 896" focusable="false" data-icon="vertical-align-top" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                            <path d="M859.9 168H164.1c-4.5 0-8.1 3.6-8.1 8v60c0 4.4 3.6 8 8.1 8h695.8c4.5 0 8.1-3.6 8.1-8v-60c0-4.4-3.6-8-8.1-8zM518.3 355a8 8 0 00-12.6 0l-112 141.7a7.98 7.98 0 006.3 12.9h73.9V848c0 4.4 3.6 8 8 8h60c4.4 0 8-3.6 8-8V509.7H624c6.7 0 10.4-7.7 6.3-12.9L518.3 355z"></path>
                        </svg>
                    </span>
                </div>
            </div>
        </div>
        {/* 搜索遮罩层 */}
        {
            searchFlag && <div className="searchFlag">
                <div className="searchFlag_content container">
                    <div className="search_header">
                        <div> <h2 className="articleSearch">文章搜索</h2></div>
                        <div className="search_esc" onClick={() => setSearchFlags(false)}> <span ><span>X</span></span> <span>esc</span> </div>
                    </div>
                    <div className="search_input">
                        <input type="text"
                            ref={inpSearch}
                            placeholder="输入关键字，搜索文章" /> <button onClick={() => changeValue()}><svg viewBox="64 64 896 896" focusable="false" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path></svg></button>
                    </div>
                    <div className='search_input_fuzzy'>
                        {FuzzyData && FuzzyData.map((item, index) => {
                            return <li key={index} onClick={() => {
                                setSearchFlag(false)
                                props.history.push("/detail/" + item.id)
                            }}> {item.title} </li>
                        })}
                    </div>
                </div>
            </div>
        }

    </div>
}

export default Layouts;
