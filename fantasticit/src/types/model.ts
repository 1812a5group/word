import { ArticleModelState } from "@/models/article";
import { CommentsModelState } from "@/models/comment";
import { KnowledgeModelState } from "@/models/knowledge";
import { ArchivesModelState } from "@/models/archives";
import { LanguageModelState } from "@/models/language";

export interface IRootState {
    article: ArticleModelState;
    knowledge: KnowledgeModelState;
    comments: CommentsModelState;
    archives: ArchivesModelState;
    language:LanguageModelState;
    loading: {global: boolean},
}
