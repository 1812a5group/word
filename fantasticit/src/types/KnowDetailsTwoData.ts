export interface KnowDetailsTwoData {
    id: string;
    parentId: string;
    order: number;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface KnowDetailsTwoDataRigth {
    id: string;
    parentId?: any;
    order: number;
    title: string;
    cover: string;
    summary: string;
    content?: any;
    html?: any;
    toc?: any;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface FuzzyData {
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}
