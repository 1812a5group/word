
export interface RootObject {
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: Category;
    tags: Category[];
}

interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

export interface ClassifiCation {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}
export interface MaskItem {
    id: string;
    parentId?: any;
    order?: number;
    title: string;
    cover?: string;
    summary?: string;
    content?: any;
    html?: any;
    toc?: any;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface Knowledgedetails {
    id: string;
    parentId: string;
    order: number;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt?: string;
    updateAt: string;
}


export interface IDetailRecommend {
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category: Category;
    tags: Categorys[];
}

export interface Categorys {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

export interface IPostItem {
    width: number;
    height: number;
    html: string;
    name: string;
    pageUrl: string;
}
