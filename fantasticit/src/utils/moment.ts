import moment from 'moment'

moment.updateLocale('en', {
    relativeTime : {
        future: " %s",
        past:   "%s 前",
        s  : '几秒',
        ss : '%d 秒',
        m:  "几分",
        mm: "%d 分",
        h:  "几时",
        hh: "%d 时",
        d:  "%d 天前",
        dd: "%d 天前",
        M:  "%d 个月前",
        MM: "%d 个月前",
        y:  "大约%d 年前",
        yy: "大约%d 年前"
    }
});
export default moment