import copy from 'copy-to-clipboard';
import {message} from 'antd';
//复制
 
export function copyText(text: string){
    copy(text);
    message.success('复制成功!');
}