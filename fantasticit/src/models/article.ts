import {
  getRecommend,
  getcAtegoryTitle,
  getArticleList, getTagData,
  getArticleClassify,
  getTagClassify
} from '@/services';
import { IArticleItem, HTileItem, IRootState, ArticleItem, TagItem } from '@/types';
import { Effect, Reducer, } from 'umi';

export interface ArticleModelState {
  recommend: IArticleItem[];
  ategoryTitle: HTileItem[];
  ArticleList: ArticleItem[];
  ArticleNum: number;
  TagData: TagItem[];
  TagNum: number;
  ArticleClassify: ArticleItem[];
  ArticleClassifyNum: number;
  TagClassify: ArticleItem[];
  TagClassifyNum: number
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getRecommend: Effect;
    getcAtegoryTitle: Effect;
    getArticleList: Effect;
    getTagData: Effect;
    getArticleClassify: Effect;
    getTagClassify: Effect
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const IndexModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend: [],
    ategoryTitle: [],
    ArticleList: [],
    ArticleNum: 0,
    TagData: [],
    TagNum: 0,
    ArticleClassify: [],
    ArticleClassifyNum: 0,
    TagClassify: [],
    TagClassifyNum: 0
  },

  effects: {
    //推荐文章
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })
      }
    },
    //分类标题
    *getcAtegoryTitle({ payload }, { call, put }) {
      let result = yield call(getcAtegoryTitle)
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { ategoryTitle: result.data }
        })
      }
    },
    //文章列表
    *getArticleList({ payload }, { call, put, select }) {
      let data = yield select((state: IRootState) => state.article.ArticleList)
      let result = yield call(getArticleList, payload)
      if (result.statusCode === 200) {
        data = payload === 1 ? result.data[0] : [...data, ...result.data[0]]
        yield put({
          type: 'save',
          payload: { ArticleList: data, ArticleNum: result.data[1] }
        })
      }
    },
    //文章分类列表
    *getArticleClassify({ Classify, payload }, { call, put, select }) {
      let data = yield select((state: IRootState) => state.article.ArticleClassify)
      let result = yield call(getArticleClassify, Classify, payload)
      if (result.statusCode === 200) {
        data = payload === 1 ? result.data[0] : [...data, ...result.data[0]]
        yield put({
          type: 'save',
          payload: { ArticleClassify: data, ArticleClassifyNum: result.data[1] }
        })
      }
    },
    //文章标签
    *getTagData({ payload }, { call, put, select }) {
      let result = yield call(getTagData)
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { TagData: result.data }
        })
      }
    },
    //文章标签问类
    *getTagClassify({ Classify, payload }, { call, put, select }) {
      let data = yield select((state: IRootState) => state.article.TagClassify)
      let result = yield call(getTagClassify, Classify, payload)
      if (result.statusCode === 200) {
        data = payload === 1 ? result.data[0] : [...data, ...result.data[0]]
        yield put({
          type: 'save',
          payload: { TagClassify: data, TagClassifyNum: result.data[1] }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;
