import { getCommentList, getView, postMessage, getAbout, getMsgboard } from '@/services';
import { ModelsItem, IViewItem, IGetAbout, IGetMsgboard } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { message } from 'antd'

export interface CommentsModelState {
  commentList: ModelsItem[];
  commentTotal: number;
  view: IViewItem,
  getMsgboard: IGetMsgboard,
  getAbout: IGetAbout,
}

export interface CommentsModelType {
  namespace: 'comments';
  state: CommentsModelState;
  effects: {
    getCommentList: Effect;
    getView: Effect;
    postMessage: Effect;
    getMsgboard: Effect;
    getAbout: Effect;
  };
  reducers: {
    save: Reducer<CommentsModelState>;
  };
}

const IndexModel: CommentsModelType = {
  namespace: 'comments',

  state: {
    commentList: [],
    commentTotal: 0,
    view: ({} as IViewItem),
    getMsgboard: {} as IGetMsgboard,
    getAbout: {} as IGetAbout,
  },

  effects: {
    //获取评论内容
    *getCommentList({ payload }, { call, put }) {
      let result = yield call(getCommentList, payload.page, payload.id);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { commentList: result.data[0], commentTotal: result.data[1] }
        })
      }
    },
    //获取关于文章内容
    *getView({ payload }, { call, put }) {
      let result = yield call(getView, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { view: result.data }
        })
      }
    },
    //发布评论 | 回复评论
    *postMessage({ payload }, { call, put }) {
      let result = yield call(postMessage, payload.content, payload.email, payload.hostId, payload.name, payload.url, payload.parentCommentId, payload.replyUserEmail, payload.replyUserName);
      if (result.statusCode === 201) {
        message.success('发布评论成功，待审核通过')
      }
    },
    //留言板信息
    *getMsgboard({ payload }, { call, put }) {
      let result = yield call(getMsgboard)
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { getMsgboard: result.data }
        })
      }
    },
    //获取关于信息
    *getAbout({ payload }, { call, put }) {
      let result = yield call(getAbout)
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { getAbout: result.data }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default IndexModel;
