import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface LanguageModelState {
  locale: string;
  locales:{label:string,value:string}[]
}

export interface LanguageModelType {
  namespace: 'language';
  state: LanguageModelState;
  reducers: {
    save: Reducer<LanguageModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<LanguageModelState>;
  };
}

const languageModel: LanguageModelType = {
  namespace: 'language',
  state: {
    locale: 'zh-CN',
    locales:[
        {label: 'menu.language.chinese', value: 'zh-CN'},
        {label: 'menu.language.english', value: 'en-US'},
    ]
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default languageModel;