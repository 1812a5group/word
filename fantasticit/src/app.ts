import { RequestConfig } from 'umi';
import { createLogger } from 'redux-logger';
import { message } from 'antd';
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'
Nprogress.configure({showSpinner:false})
export const dva = {
  config: {
    // onAction: createLogger(),
    onError(e: Error) {
      // message.error(e.message, 3);
    },
  },
};
//全局路由切换配置
export function onRouteChange({}){
  Nprogress.start();
  setTimeout(()=>{
    Nprogress.done()
  },2000)
}
let showError = false
const baseUrl = 'https://creationapi.shbwyz.com';
export const request: RequestConfig = {
  timeout: 100000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [(url, options) => {
    return {
      url: `${baseUrl}${url}`,
      options,
    };
  }],
  responseInterceptors: [response => {
    const codeMaps: { [key: number]: string } = {
      400: '错误的请求',
      403: '禁止访问',
      404: '找不到资源',
      500: '服务器内部错误',
      502: '网关错误。',
      503: '服务不可用，服务器暂时过载或维护。',
      504: '网关超时。',
    };
    if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
      if (!showError) {
        showError = true
        message.error({
          content: codeMaps[response.status],
          onClose: () => showError = false
        })
      }
      message.error(codeMaps[response.status]);
    }
    return response;
  }],
};
