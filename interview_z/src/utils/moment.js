import moment from 'moment'
export function moments(time) {
    return moment(time).format("YYYY-MM-DD HH:mm")
}