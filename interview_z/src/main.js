import Vue from 'vue'
import App from './App'
import store from './store'
import VueCompositionAPI from '@vue/composition-api'
// main.js 文件
import uView from "uview-ui";
Vue.config.productionTip = false;
Vue.use(VueCompositionAPI)

Vue.use(uView);
App.mpType = 'app'

const app = new Vue({
  store,
  ...App,
})
app.$mount()
