import home from "./module/home"
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        home
    }
})

// export default new Vuex.Store({
//     modules: {
//         user,
//         index,
//     },
//     plugins: [createLogger()]
// });
