import {login,getsignList,getdetail,setdetail} from '../../services'

const state = {
    userInfo: {},
    isLogin: false,
    signList: [],
    detail: {},
    signDetail:{}
}


const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }

};

const actions = {
  async userLogin({commit},payload) {
        let res = await login(payload);
        if (res.code === 0){
            commit('update', {
                userInfo: res.data,
                isLogin: true
            })
            // 把登陆态存储到本地
            wx.setStorageSync("openid", res.data.openid)
            console.log('res...', res.data);
           wx.setStorageSync("iphone",res.data.phone)
        }
    },

    async getsignList({ commit }, payload) {
        let res = await getsignList(payload);
        if (res.code === 0){
            commit('update', {
                signList: res.data,
            })
            console.log(res.data);
        }
    },
    async getdetail({ commit }, payload) {
        let res = await getdetail(payload);
        if (res.code === 0){
            commit('update', {
                detail: res.data,
            })
        }
        console.log(res);
    },
    async setdetail({ commit }, payload) {
        let res = await setdetail(payload.id,payload.status);
        console.log(res);
        if (res.code === 0){
            let data = await getdetail(payload.id);
            await getsignList({page: 1,pageSize:10,status:-1})
            if (data.code === 0){
                commit('update', {
                    detail: data.data,
                })
            }
        }
    },


};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};