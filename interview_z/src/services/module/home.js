import request from '@/utils/request'

export const login = (code) => {
    return request.post("/user/code2session", {
        code
    })
}

export const addSign = (data) => {
    return request.post("/sign", {
        ...data
    })
}

export const getsignList = (params) => {
    return request.get(`/sign`,params)
}


export const getdetail = (id) => {
    return request.get(`/sign/${id}`)
}

export const setdetail = (id,status) => {
    return request.put(`/sign/${id}`, {
        status
    })
}


export const getorder = (total_fee) => {
    return request.post(`/sign/pay`, {
        total_fee:total_fee*1
    })
}
