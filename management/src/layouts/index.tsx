import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { NavLink, useLocation } from 'umi'
import Nav from '@/componets/Nav'
import style from './index.less'
import { Layout, Breadcrumb, Affix } from 'antd';
import LayoutFooter from '@/componets/LayoutFooter';
import LayoutHeader from '@/componets/LayoutHeader';
import { Helmet } from 'react-helmet';
import {
    UserOutlined,
    DashboardOutlined,
    FormOutlined,
    CopyOutlined,
    TagOutlined,
    SnippetsOutlined,
    BookOutlined,
    StarOutlined,
    MessageOutlined,
    MailOutlined,
    FolderOpenOutlined,
    SearchOutlined,
    ProjectOutlined,
    SettingOutlined,
} from '@ant-design/icons';
import useStore from '@/context/useStore'
let navlist = [{
    id: 1,
    path: "/",
    name: "工作台",
    type: "dashboard",
    iconfont: <DashboardOutlined />,
}, {
    name: "文章管理",
    type: "form",
    path: "/article",
    iconfont: <FormOutlined />,
    children: [{
        id: 2,
        path: "/article",
        name: "所有文章",
        type: "snippets",
        iconfont: <FormOutlined />,
    }, {
        id: 3,
        path: "/article/category",
        name: "分类管理",
        type: "copy",
        iconfont: <CopyOutlined />,
    }, {
        id: 4,
        path: "/article/tags",
        name: "标签管理",
        type: "tag",
        iconfont: <TagOutlined />,
    }]
}, {
    id: 5,
    path: '/page',
    name: "页面管理",
    type: "snippets",
    iconfont: <SnippetsOutlined />,
}, {
    id: 6,
    path: "/knowledge",
    name: "知识小册",
    type: "book",
    iconfont: <BookOutlined />,
}, {
    id: 7,
    path: "/poster",
    name: "海报管理",
    type: "star",
    iconfont: <StarOutlined />,
}, {
    id: 8,
    path: "/comment",
    name: "评论管理",
    type: "message",
    iconfont: <MessageOutlined />,
}, {
    id: 9,
    path: "/mail",
    name: "邮件管理",
    type: "mail",
    iconfont: <MailOutlined />,
}, {
    id: 10,
    path: "/file",
    name: "文件管理",
    type: "form",
    iconfont: <FolderOpenOutlined />,
}, {
    id: 11,
    path: "/search",
    name: "搜索记录",
    type: "search",
    iconfont: <SearchOutlined />,
}, {
    id: 12,
    path: '/view',
    name: "访问统计",
    type: "project",
    iconfont: <ProjectOutlined />,
}, {
    id: 13,
    path: "/user",
    name: "用户管理",
    type: "user",
    iconfont: <UserOutlined />,
}, {
    id: 14,
    path: '/setting',
    name: "系统设置",
    type: "setting",
    iconfont: <SettingOutlined />,
}]
const { Header, Content, Footer, Sider } = Layout;
const pathName = ['/login', '/register', '/chat', '/article/editor', '/page/editor', /^\/article\/((.*))[10-15]/ig,'/article/amEditor']

let layouts: React.FC<RouteComponentProps> = function (props) {

    let [collapsed, setCollapsed] = useState(false)
    let [breadcrumb, setBreadcrumb] = useState("")
    const store = useStore()
    useEffect(() => {
        store.setting.getSettingVal()
    }, [])
    let toggle = () => {
        setCollapsed(!collapsed)
    };
    const location = useLocation()
    let setNavBread = (str: string) => {
        setBreadcrumb(str)
    }
    let jump = () => {
        props.history.push("/");
        setBreadcrumb("")
    }
    for (let i = 0; i < pathName.length; i++) {
        if (typeof pathName[i] === 'string') {
            if (pathName.indexOf(location.pathname) !== -1) return <>{props.children}</>
        }
        if (typeof pathName[i] !== 'string') {
            if ((pathName[i] as RegExp).test(location.pathname)) return <>{props.children}</>
        }
    }
    return (
        <Layout className={style.layout} style={{ minHeight: '100vh' }}>
            <Helmet>
                <title>
                    {breadcrumb == "" ? "工作台" : breadcrumb}
                </title>
            </Helmet>
            {/* 左侧导航 */}
            <Sider trigger={null} collapsible collapsed={collapsed} className={style.layoutLeft}>
                <Nav setNavBread={setNavBread} collapsed={collapsed} navlist={navlist} />
            </Sider>
            <Layout className={style.layoutRight}>
                <Header className={style.layoutHeader} >
                    <LayoutHeader toggle={toggle} collapsed={collapsed} />
                </Header>
                <Content className={style.layoutContent}>
                    {/* 面包屑 */}
                    <div className={style.contentHeader}>
                        {
                            location.pathname === '/' ? <>
                                <Breadcrumb className={style.Breadcrumb}>
                                    <Breadcrumb.Item href='/'> {navlist[0].name}</Breadcrumb.Item>
                                </Breadcrumb>
                                <h2 className={style.hello}>您好，{JSON.parse(localStorage.getItem('user')!).name}</h2>
                                <div className={style.role}>
                                    您的角色：{
                                       JSON.parse(localStorage.getItem('user')!).role === 'admin' ? '管理员' : '访客' 
                                    }
                                </div>
                            </> : <Breadcrumb>
                                <Breadcrumb.Item href='/'>{navlist[0].name}</Breadcrumb.Item>
                                {
                                    navlist.map(item => {
                                        if (!item.children && item.path === location.pathname) {
                                            return <Breadcrumb.Item key={item.id}> {item.name}</Breadcrumb.Item>
                                        }
                                        else if (item.children?.length && item.children.some(val => val.path === location.pathname)) {
                                            return item.children?.map(item1 => {
                                                if (item.path === item1.path) {

                                                    return <>
                                                        <Breadcrumb.Item key={item1.id}> {item1.name}</Breadcrumb.Item>
                                                    </>
                                                } else {

                                                    return <>
                                                        <Breadcrumb.Item key={item1.id}>{item1.path === location.pathname ? item1.name : ''}</Breadcrumb.Item>
                                                    </>
                                                }
                                            })
                                        }
                                    })

                                }
                            </Breadcrumb>
                        }
                    </div>
                    <div className={style.loyoutChildren}>
                        {console.log(props.children)
                        }
                        {props.children}
                        <LayoutFooter />
                    </div>
                </Content>
            </Layout>
        </Layout>
    )
}

export default layouts
