import React, { ReactElement, useEffect, useRef } from 'react'
import style from './index.less'
import * as echarts from 'echarts';
interface Props {

}
let PanelNav: React.FC<Props> = function (props) {
    const eChartsRef: any = useRef(null);
    useEffect(() => {
        const chart = echarts.init(eChartsRef.current);   //echart初始化容器
        let option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999',
                    }
                }
            },
            legend: {
                data: ['评论数', '访问量']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Web', 'Thu', 'Fri', 'Sta', 'Sun'],
                    axisPointer: {
                        type: 'shadow',
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '每周客户访问指标',
                    min: 0,
                    max: 400,
                    interval: 100,
                    axisLabel: {
                        formatter: '{value}',
                    }
                },
                {
                    type: 'value',
                    min: 0,
                    max: 400,
                    interval: 100,

                },

            ],
            series: [
                {
                    name: '评论数',
                    type: 'bar',
                    data: [110, 200, 150, 90, 80, 105, 120],
                    color: '#C23531'
                },
                {
                    name: '访问量',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [380, 295, 340, 110, 100, 120, 160]
                }


            ]
        };
        chart.setOption(option);
        setTimeout(function () {
            window.onresize = function () {
                chart.resize();
            }
        }, 200)
    }, []);

    return (
        <div className={style.panelNav}>
            <div className={style.canvas}>
                <div ref={eChartsRef} style={{ width: "115%", height: "100%" }}></div>
            </div>
        </div>
    )
}

export default PanelNav
