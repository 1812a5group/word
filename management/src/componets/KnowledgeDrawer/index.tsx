import React from 'react'
import style from './index.less'
import { Drawer, Button, Switch, Upload, message, Input } from 'antd'
const { Dragger } = Upload;
import { } from 'antd';


import { InboxOutlined } from '@ant-design/icons';
import { IKnowledgeItem } from '@/types'
import useStore from '@/context/useStore'
interface Props {
    visible: boolean
    onClose(): void
    item: Partial<IKnowledgeItem>
}

const KnowledgeDrawer: React.FC<Props> = (props) => {
    const { visible, onClose, item } = props
    const store = useStore()
 
    //删除
    const delFile = () => {
        store.file.delFile(item.id!)
    }
    function onChange() {
        console.log(`switch to 开关`);
    }

    const propsAddress = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    //选择文件上传
    const propsfile = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    return <Drawer
        title="更新知识库"
        placement="right"
        width="640"
        onClose={onClose}
        visible={visible}>

        <div className={style.form_item}>
            <div className={style.lable}>名称:</div>
            <div className={style.input_text}><Input value={item.title} /></div>
        </div>
        <div className={style.form_item}>
            <div className={style.lable}>描述:</div>
            <div className={style.input_text}><Input value={item.summary} /></div>
        </div>
        <div className={style.form_item}>
            <div className={style.lable}>评论</div>
            <div><Switch defaultChecked onChange={onChange} /></div>
        </div>
        <div className={style.form_item}>
            <div className={style.lable}>封面</div>
            <div className={style.input_text}>
                <Dragger className={style.dragger} {...propsAddress} style={{ background: "#fff" }}>
                    <p className="ant-upload-drag-icon"><InboxOutlined /></p>
                    <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                    <p className="ant-upload-hint">将文件上传到 OSS, 如未配置请先配置</p>
                </Dragger>
            </div>
        </div>
        <div className={style.form_item}>
            <div className={style.lable}></div>
            <div className={style.input_text}>
                <Input /> 
            </div>
        </div>
        <div className={style.form_item}>
            <div className={style.lable}></div>
            <div className={style.input_text}> 
                <Upload {...propsfile}>
                    <Button>
                        选择文件
                    </Button>
                </Upload>
            </div>
        </div>

        <div className={style.footer}>
            <div className={style.footLeft}></div>
            <div className={style.footright}>
                <Button style={{ marginRight: "8px" }} onClick={onClose}>关闭</Button>
                <Button danger onClick={delFile}>删除</Button>
            </div>
        </div> 
    </Drawer>
}
export default KnowledgeDrawer;