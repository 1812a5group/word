import React, { ReactElement, useState } from 'react'
import { Avatar, Menu, Dropdown, Breadcrumb } from 'antd'
import style from './index.less'

import { Table, Popover, Button } from 'antd';

interface Props {
    direction: any,
    url: string
}



let BubblePaper: React.FC<Props> = function (props) {

    const content = (
        <div className={style.bigFFF}>
            <iframe style={{ width: "100%", height: "100%" }} src={props.url} ></iframe>
            {/* <p><span>404</span>  This page could not be found.</p> */}
        </div>
    );

    return <Popover placement={props.direction} content={content} title="页面预览">
        <span>文章</span>
    </Popover>

}

export default BubblePaper
