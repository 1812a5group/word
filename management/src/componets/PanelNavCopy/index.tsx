import React, { ReactElement } from 'react'
import style from './index.less'
import {
    ComposedChart,
    Line,
    Area,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts';
interface Props {

}
let PanelNav: React.FC<Props> = function (props) {

    const data = [
        {
            name: 'Page A',
            uv: 59,
            pv: 80,
            amt: 200,
        },
        {
            name: 'Page B',
            uv: 88,
            pv: 97,
            amt: 100,
        },
        {
            name: 'Page C',
            uv: 97,
            pv: 100,
            amt: 100,
        },
        {
            name: 'Page D',
            uv: 140,
            pv: 170,
            amt: 150,
        },
        {
            name: 'Page E',
            uv: 120,
            pv: 148,
            amt: 200,
        },
        {
            name: 'Page F',
            uv: 100,
            pv: 120,
            amt: 400,
        },
    ];
    // const demoUrl = 'https://codesandbox.io/s/composed-chart-of-same-data-i67zd';
    return (
        <div className={style.panelNav}>
            <div className={style.header}>
                <p>
                    面板导航
                </p>

            </div>
            <div className={style.canvas}>
                <h3>
                    每周用户达标量
                </h3>
                <ResponsiveContainer width="90%" height="90%" className={style.canvasContent}>

                    <ComposedChart
                        width={500}
                        height={400}
                        data={data}
                        margin={{
                            top: 20,
                            right: 20,
                            bottom: 20,
                            left: 20,
                        }}
                    >
                        <Legend />
                        <CartesianGrid stroke="#f5f5f5" />
                        <XAxis dataKey="name" scale="band" />
                        <YAxis />
                        <Tooltip />
                        <Bar dataKey="uv" barSize={150} fill="#D53A35" />
                        <Line dataKey="pv" width={5} stroke="#000" />
                    </ComposedChart>
                </ResponsiveContainer>
            </div>
        </div>
    )
}

export default PanelNav
