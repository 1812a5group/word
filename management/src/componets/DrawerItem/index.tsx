import React from 'react'
import { Drawer, Button } from 'antd'
import { IFileItem } from '@/types'
import style from './index.less'
import classnames from 'classnames'
import { copyText } from '@/utils/copy'
import ImageView from '@/componets/ImageView'
import useStore from '@/context/useStore'
interface Props {
    visible: boolean
    onClose(): void
    item: Partial<IFileItem>
}

const DrawerItem: React.FC<Props> = (props) => {
    const { visible, onClose, item } = props
    const store = useStore()
    //复制
    const changeCopy = () => {
        copyText(item.url as string)
    }
    //删除
    const delFile = () => {
        store.file.delFile(item.id!)
    }
    return <div>
        <Drawer
            title="文件信息"
            placement="right"
            width="640"
            onClose={onClose}
            visible={visible}>
            <div className={style.img}>
                <ImageView>
                    <img src={item.url} alt="" className={style.src} />
                </ImageView>
            </div>
            <div className={style.name}>
                <div className={style.box}>
                    <p className={style.file}>文件名称:</p>
                    <div>{item.originalname}</div>
                </div>
            </div>
            <div className={style.name}>
                <div className={style.box}>
                    <p className={style.file}>存储路径:</p>
                    <div>{item.filename}</div>
                </div>
            </div>
            <div className={classnames(style.name, style.row)}>
                <div className={style.left}>
                    <p className={style.file}>文件类型:</p>
                    <div>{item.type}</div>
                </div>
                <div className={style.left}>
                    <p className={style.file}>文件大小:</p>
                    <div>{(item.size as number / 1024 / 1024).toFixed(2)}MB</div>

                </div>
            </div>
            <div className={style.inp}>
                <p className={style.file}>文件大小:</p>
                <div>
                    <div className={style.input} onClick={changeCopy}>
                        {item.url}
                    </div>
                    <Button style={{ border: "none", color: "#09f" }} className={style.btn} onClick={changeCopy}>复制</Button>
                </div>
            </div>
            <div className={style.footer}>
                <div className={style.footLeft}></div>
                <div className={style.footright}>
                    <Button style={{ marginRight: "8px" }} onClick={onClose}>关闭</Button>
                    <Button danger onClick={delFile}>删除</Button>
                </div>
            </div>

        </Drawer>
    </div>
}
export default DrawerItem;