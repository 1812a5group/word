import { ImailItem, ISearchInfo } from "@/types";
import React, { useState,Key } from "react"
import style from "./index.less"
import { Alert, Empty, Table, Button, Popconfirm, message, Badge, Tooltip } from 'antd';
import { ReloadOutlined } from '@ant-design/icons'; 
import { observer } from 'mobx-react-lite';
interface Props {
    info: any,
    columns: any,
    infoLength: number,
    scrollX: number,
    deleteList: any,
    params: any
}
const Smtable: React.FC<Props> = (props) => {
    const [showDel, setshowDel] = useState(false)
    const [selectedRoekeys, setSelectedRoekeys] = useState<Key[]>([])
    function onSelectChange(selectedRoekeys: Key[]) {
        setSelectedRoekeys(selectedRoekeys)
    }
    //多选框
    const rowSelection = {
        selectedRoekeys,
        onChange: onSelectChange
    };

    return (
        <>
            <div className={style.mailbottom}>
                {
                    props.info ? <div className={style.searchBottom}>
                        <Tooltip placement="top" title="刷新">
                            <p className={style.icon} onClick={() => {
                                props.info(1, 12, props.params)
                            }}><ReloadOutlined /></p>
                        </Tooltip>
                        {selectedRoekeys.length  ?
                            <Popconfirm
                                title="确认删除？"
                                onConfirm={() => props.deleteList(selectedRoekeys as string[])}
                                onCancel={() => message.error('取消删除')}
                                okText="确认"
                                cancelText="取消"
                            >
                                <Button className={style.deletebtn} danger>删除</Button>
                            </Popconfirm> : null}
                        <Table
                            rowKey="id"
                            scroll={{ x: props.scrollX }}
                            rowSelection={rowSelection}
                            columns={props.columns}
                            dataSource={props.info}
                            pagination={{
                                showSizeChanger: true,
                                defaultCurrent: 1,
                                total: props.infoLength,
                                showTotal: (total) => `共${total}条`,
                            }}
                        />
                    </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                }
            </div>
        </>
    )
}
export default observer(Smtable);