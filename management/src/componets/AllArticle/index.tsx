import React from 'react'
import style from './index.less'
import { ICategoryItem } from '@/types'
import { Input, Button } from 'antd';
interface Props {
    title: string
    list: Array<ICategoryItem>
    changeValue(item:ICategoryItem): void
}
const AllArticle: React.FC<Props> = (props) => {
    let { title, list, changeValue } = props

    return <>
        <div className={style.header}>
            <div className={style.title}>
                所有{title}
            </div>

        </div>
        <div className={style.main}>
            <ul className={style.ul}>
                {
                    list.map(item => {
                        return <li className={style.item}
                        onClick={()=>changeValue(item)}
                        
                        
                            key={item.id}
                        >{item.label}</li>
                    })
                }
            </ul>
        </div>
    </>
}

export default AllArticle