import React, { ReactElement, useState } from 'react'
import { Form, Input, Button, Select } from 'antd';
const { Option } = Select;

import style from "./index.less"
import { Key } from 'rc-select/lib/interface/generator';
import { NamePath } from 'rc-field-form/lib/interface';
interface Props {
    info?: any,
    getParams?(params: any): void
}

let SearchHead: React.FC<Props> = function (props) {
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };
    const [form] = Form.useForm();

    // 搜索
    const onFinish = (values: any) => {
        console.log("values...", values);
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        props.getParams!(params)
    }

    return (
        <div className={style.searchTop}>
            <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
                <div className={style.searchtop}>
                    {
                        props.info?.map((item: { placeholder: string | undefined; name: NamePath | undefined; label: {} | null | undefined; select: { value: Key; text: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }[]; }, index: number) => {
                            if (item.placeholder !== "") {
                                return <Form.Item key={index} className="formInp" name={item.name} label={item.label}>
                                    <Input placeholder={item.placeholder}/>
                                </Form.Item>
                            } else {
                                return <Form.Item className="formInp" key={index} name={item.name} label={item.label}>
                                    <Select placeholder="">
                                        {item.select.map((ite: { value: Key; text: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }, ind: number) => {
                                            return <Option key={ind} value={ite.value}>{ite.text}</Option>
                                        })}
                                    </Select>
                                </Form.Item>
                            }
                        })
                    }
                </div>
                <div className={style.searchbtn}>
                    <div className={style.searchRight}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button htmlType="button" onClick={() => { form.resetFields() }}>
                            重置
                        </Button>
                    </div>
                </div>
            </Form>

        </div>
    )
}

export default SearchHead
