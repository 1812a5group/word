import React from 'react'
import style from './index.less'
import { Input, Button, Popconfirm } from 'antd';
interface Props {
    title: string
    value: string
    label: string
    id: string
    flag: boolean
    changeFlag(): void
    changeVal(name: string, val: string, id: string): void
    updateCategory(val: string): void
    delCategory(id: string): void
}
const AddArticle: React.FC<Props> = (props) => {
    let { title, value, label, id, flag, changeFlag, changeVal, updateCategory, delCategory } = props

    return <>
        <div className={style.header}>
            <div className={style.title}>
                添加{title}
            </div>

        </div>
        <div className={style.main}>
            <div className={style.inp}>
                <div className={style.inpitem}>
                    <Input
                        value={label}
                        name='label'
                        onChange={(e) => changeVal(e.target.name, e.target.value, id)}
                        placeholder={`输入${title}名称`}
                    />
                </div>
                <div className={style.inpitem}>
                    <Input
                        value={value}
                        name='value'
                        onChange={(e) => changeVal(e.target.name, e.target.value, id)}
                        placeholder={`输入${title}值（请输入英文，作为路由使用）`}
                    />
                </div>
                <div className={style.btn}>
                    <div className={style.left}>
                        <Button type="primary" onClick={(e) => updateCategory(e.target.innerHTML)}>{flag ? '更新' : '保存'}</Button>
                        {flag && <Button type="dashed"
                            onClick={changeFlag}
                        >返回添加</Button>}
                    </div>
                    <div className={style.right}>
                        {flag && <Popconfirm
                            title={`确认删除这个${title}？`}
                            onConfirm={()=>delCategory(id)}
                            okText="确认" cancelText="取消"
                        >
                            <Button danger
                            >删除</Button>
                        </Popconfirm>
                        }

                    </div>
                </div>

            </div>
        </div>
    </>
}

export default AddArticle