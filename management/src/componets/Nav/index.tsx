import React, { ReactElement, useEffect, useState, useRef, Ref } from 'react'
import style from './index.less'
import { Menu, Button, Dropdown } from 'antd';
import { NavLink } from 'react-router-dom';
import { LocationDescriptor, Location } from 'history';
import { useHistory } from 'umi'
const { SubMenu } = Menu;
interface Props {
    setNavBread(str: string): void
    collapsed: boolean,
    navlist: Array<any>
}

let layouts: React.FC<Props> = function (props) {
    const history = useHistory()
    const [defaulKey, setdefaulKey] = useState<string>('1')
    useEffect(() => {
        let key = props.navlist.findIndex(item => item.path === location.pathname)
        setdefaulKey(String(key + 1))
        // console.log(String(props.navlist.findIndex(item => item.path === location.pathname) + 1));
    }, [])
    const Btnref = useRef(null)
    let [personalList, setPersonalList] = useState([{
        title:"新建文章-协同编辑器",
        path:"/article/amEditor",
    },{
        title: "新建文章", 
        path:"/article/editor"
    },{
        title:"新建页面",
        path:"/page/editor"
    }])
    const menu = (
        <Menu>
            {
                personalList.map((item, index) => {
                    return <Menu.Item key={index} onClick={()=>{
                        history.push(`${item.path}`)
                    }}>
                        {item.title}
                    </Menu.Item>
                })
            }
        </Menu>
    );

    let { collapsed, navlist } = props
    return (
        <div className={style.nav}>
            <div className={style.herder}>
                <img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" />
                <span style={{ display: collapsed ? 'none' : 'block' }}>管理后台</span>
            </div>
            <div className={style.layoutbtn}>
                <Dropdown overlay={menu} placement="bottomCenter">
                    <Button
                        type="primary"
                        size="large"
                        className={style.ant_btn}
                        ref={Btnref}
                    >
                        {collapsed ? '+' : '+ 新建'}

                    </Button>
                </Dropdown>

                {/* <Menu>
                    {
                        personalList.map((item, index) => {
                            return <Menu.Item key={index}>
                                {item}
                            </Menu.Item>
                        })
                    }
                </Menu> */}
            </div>
            <Menu theme="dark" mode="inline" >
                {
                    navlist.map((item) => {
                        return item.children ? <SubMenu
                            key={item.name}
                            icon={item.iconfont}
                            title={item.name}
                        >
                            {item.children.map((item2: { iconfont: any; id: React.Key | null | undefined; path: LocationDescriptor<unknown> | ((location: Location<unknown>) => LocationDescriptor<unknown>); name: string | number | boolean | {} | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactNodeArray | React.ReactPortal | null | undefined; }) => {
                                return <Menu.Item key={item2.id} className={style.menu}>
                                    <NavLink exact activeClassName={style.active} to={item2.path} onClick={() => props.setNavBread(item.name)}>
                                        {item2.iconfont}
                                        <span>{item2.name} </span>
                                    </NavLink>
                                </Menu.Item>
                            })}
                        </SubMenu> : <Menu.Item key={item.id} className={style.menu}>
                            <NavLink exact activeClassName={style.active} to={item.path} onClick={() => props.setNavBread(item.name)}>
                                {item.iconfont}
                                <span>{item.name} </span>
                            </NavLink>
                        </Menu.Item>
                    })
                }
            </Menu>
        </div>
    )
}

export default layouts
