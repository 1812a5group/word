import React, { ReactElement, useState } from 'react'
import { Avatar, Menu, Dropdown, Breadcrumb } from 'antd'
import { removeToken } from '@/utils'
import { history } from 'umi'
import style from './index.less'
import { MenuUnfoldOutlined, MenuFoldOutlined, UserOutlined, GithubOutlined } from '@ant-design/icons';
interface Props {
    toggle(): void
    collapsed: boolean
}


let header: React.FC<Props> = function (props) {
    let { toggle, collapsed } = props
    let [personalList, setPersonalList] = useState(['个人中心', '用户管理', '系统设置', '退出登录'])
    const menu = (
        <Menu>
            {
                personalList.map((item, index) => {
                    return <Menu.Item key={index} onClick={() => {
                        if (index === 3) {
                            removeToken()
                            history.replace('/login?from=' + encodeURIComponent(location.pathname))
                        }
                        if (index === 0) {
                            history.push('/ownspace')
                        }
                    }}>
                        {item}
                    </Menu.Item>
                })
            }
        </Menu>
    );
    return (

        <div className={style.header}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: toggle,
            })}
            <div className={style.right}>
                <a href=""> <GithubOutlined /> </a>
                <Dropdown overlay={menu} placement="bottomCenter">
                    <div className={style.tigger} style={{ height: "25px" }}>
                        <span><Avatar size="small" src={JSON.parse(localStorage.getItem('user')!).avatar} icon={<UserOutlined />} /></span>
                        <span>  Hi, {JSON.parse(localStorage.getItem('user')!).name}</span>
                    </div>

                </Dropdown>
            </div>
        </div>
    )
}

export default header
