import React, { useEffect, useState } from 'react'
import style from './index.less'
import './index.less'
import { Form, Input, Button, Card, Col, Row, Pagination, Drawer } from 'antd'
import { useLocation } from 'umi'
import { IFileItem } from '@/types'
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore'
import DrawerItem from '@/componets/DrawerItem'
import moment from 'moment'
const { Meta } = Card;
const FileReuse: React.FC = (props) => {
    const store = useStore()
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [visible, setVisible] = useState<boolean>(false);
    const [item, setItem] = useState<Partial<IFileItem>>({})
    useEffect(() => {
        store.file.getFileList({ page, pageSize })
    }, [page, pageSize])
    const location = useLocation()
    //点击搜索
    const onFinish = (values: any) => {
        store.file.getFileList({ page, pageSize, ...values })
    }
    const showDrawer = (item: IFileItem) => {
        setVisible(true);
        setItem(item)
    };
    const onClose = () => {
        setVisible(false);
    };
    return <div>
        <div className={style.top} style={{ marginBottom: "16px" }}>
            <Form
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 14 }}
                layout="horizontal"
                onFinish={onFinish}
                initialValues={{ remember: true }}
            >
                <div className={style.form}>
                    <Form.Item
                        className={style.item}
                        label="文件名称"
                        name='originalname'>
                        <Input placeholder="请输入文件名称" />
                    </Form.Item>
                    <Form.Item
                        className={style.item}
                        label="文件类型"
                        name='type'>
                        <Input placeholder="请输入文件类型" />
                    </Form.Item>
                </div>
                <div className={style.btn}>
                    <div className={style.searchbtn}>
                        <Button type="primary" htmlType="submit">
                            搜索
                            </Button>
                        <Button htmlType="reset" style={{ marginLeft: "8px" }}>
                            重置
                            </Button>
                    </div>
                </div>
            </Form>

        </div>
        {
            location.pathname === '/knowledge' ?
                <div className={style.file}>
                    <div>
                        <Button className={style.upload}>上传文件</Button>
                    </div>
                </div> : <></>
        }
        <div className={style.card}>
            <Row gutter={24}

            >
                {
                    store.file.fileList.map(item => {
                        return <Col span={6}
                            key={item.id}
                            style={{ marginBottom: "16px" }}
                            onClick={() => showDrawer(item)}
                        >
                            <Card
                                hoverable
                                style={{ maxWidth: "100%", height: "275px" }}
                                cover={<img alt="example" src={item.url} className={style.img} />}
                            >
                                <Meta title={item.originalname} description={`上传于${moment(item.createAt).format("YYYY-MM-DD HH:mm:ss")}`} />
                            </Card>
                        </Col>
                    })
                }
            </Row>
            <div className="pagina">
                <div className={style.left}>
                </div>
                <div className={style.right}>
                    <Pagination
                        defaultCurrent={1}
                        total={store.file.fileListCount}
                        showSizeChanger={true}
                        pageSize={pageSize}
                        current={page}
                        pageSizeOptions={['8', '12', '24', '36']}
                        showTotal={(total,) => `共 ${total} 条`}
                        onChange={(current, pageSize) => {
                            setPage(current)
                            setPageSize(pageSize as number)
                        }}
                    />
                </div>
            </div>
        </div>
            <DrawerItem
            visible={visible}
            onClose={onClose}
            item={item}
        />
    </div>
}
export default observer(FileReuse)