import React, { ReactElement, useState } from 'react'
import { Avatar, Menu, Dropdown, Breadcrumb } from 'antd'
import style from './index.less'

import { Popover } from 'antd';

interface Props {
    content: string,
    direction: any
}


let BubbleComments: React.FC<Props> = function (props) {
    const content = (
        <>
            {props.content}
        </>
    );

    return <Popover placement={props.direction} content={content} title="评论详情-原始内容">
        <span>查看内容</span>
    </Popover>

}

export default BubbleComments
