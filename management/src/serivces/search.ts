
import { request } from "@/.umi/plugin-request/request";
import { ISearchInfo } from "@/types";


//搜索列表数据  
export function getsearchArticle(page=1, pageSize=12, params: Partial<ISearchInfo> = {}) {
    return request(`/api/search?page=${page}&pageSize=${pageSize}`, { params })
}

// 删除
export function deleteRecords(id: string) {
    return request(`/api/search/${id}`, {
        method: "DELETE"
    })
}