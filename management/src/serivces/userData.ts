import { UserDataItem, UserItem } from '@/types';
import {request} from 'umi';

//用户管理
export function userData(page=1,pageSize=12,params:Partial<UserItem>={}) {
    return request(`/api/user?page=${page}&pageSize=${pageSize}`,{params})
}

//角色/状态
export function userAction(data:UserDataItem) {
    return request(`/api/user/update`,{
        method:'POST',
        data
    })
}