import { request } from 'umi'
import { IArticleParams, IArticlePatch ,ICategoryPatch,IArtilePostParams} from '@/types'
//获取所有文章数据
export const getArticle = (params: IArticleParams) => {
    return request('/api/article', {
        params
    })
}
//发布数据 草稿数据 首焦推荐 撤销首焦
export const articlePatch = (id: string, params: IArtilePostParams) => {
    
    return request(`/api/article/${id}`,{
        method:'PATCH',
        data:params
    })
}
//删除文章数据
export const deleteArticle = (id:string) => {
    return request(`/api/article/${id}`,{
        method:'DELETE'
    })
}
//获取分类管理 、 标签管理数据
export const getcategory = () => {
    return request(`/api/category`)
}
//获取标签管理数据
export const gettag = () => {
    return request(`/api/tag`)
}
//更新分类管理 、 标签管理数据
export const changeCategory = (name:string,id:string,params:ICategoryPatch) => {
    return request(`/api/${name}/${id}`,{
        method:'PATCH',
        data:params
    })
}
//添加分类管理 、 标签管理数据
export const addCategory = (name:string,params:ICategoryPatch) => {
    return request(`/api/${name}`,{
        method:'POST',
        data:params
    })
}
//删除分类管理 、 标签管理数据
export const delCategory = (name:string,id:string) => {
    return request(`/api/${name}/${id}`,{
        method:'DELETE',
    })
}
//添加文章
export const addArticleEditor = (params:IArtilePostParams) => {
    return request(`/api/article`,{
        method:'POST',
        data:params
    })
}
//获取详情数据
export const getArticleDetail = (id:string) => {
    return request(`/api/article/${id}`)
}