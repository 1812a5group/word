import { request } from "@/.umi/plugin-request/request";
import { IViewItem } from "@/types";

//访问统计列表数据  
export function getViewList(page = 1, pageSize = 12, params: Partial<IViewItem> = {}) {
    return request(`/api/view?page=${page}&pageSize=${pageSize}`, { params })
}

// 删除
export function deleteView(id: string) { 
    return request(`/api/view/${id}`, {
        method: "DELETE"
    })
}