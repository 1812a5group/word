import { request } from 'umi'
import { IFileParams } from '@/types'
//获取文件数据 模糊搜索
export const getFileList = (params: IFileParams) => {
    return request('/api/file', {
        params
    })
}
//删除文件数据
export const delFile = (id:string) => {
    return request(`/api/file/${id}`,{
        method:"DELETE"
    })
}