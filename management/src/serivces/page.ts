import { request } from "umi";

export function getpagedata(page: number, pageSize: number,params:any) {
    return request(`/api/page?page=${page}&pageSize=${pageSize}`,{params})
}
// 发布下线
export function setpagestatus(id: string, data: string) {
    return request(`/api/page/${id}`, {
        method: "PATCH",
        data
    })
}
//删除
export function delpages(id: string) {
    return request(`/api/page/${id}`, {
        method: "DELETE",
    })
}
