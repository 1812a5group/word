import { request } from 'umi'

export const getComment = (page = 1, pageSize = 6, params = {}) => {
    return request(`/api/comment?page=${page}&pageSize=${pageSize}`, { params })
}
// 设置通过拒绝权限
export const setCommentPass = (id: string, pass: boolean) => {
    return request(`/api/comment/${id}`, {
        method: "PATCH",
        data: {
            pass
        }
    })
}

// 删除评论
export const delCommentitem = (id: string) => {
    return request(`/api/comment/${id}`, {
        method: "DELETE"
    })
}
