import {request} from "umi"
 
export const getKnowledgeList = (page: number, pageSize: number,params={}) => {
    return request(`/api/knowledge?page=${page}&pageSize=${pageSize}`,{params})
}

// 删除
export function deleteKnowledgeCard(id: string) {
    // console.log(id,"知识小册删除");
    
    return request(`/api/knowledge/${id}`, {
        method: "DELETE"
    })
}

// 发布、草稿
export function editKnowledgePublish(id: string,flag:boolean) {
    return request(`/api/knowledge/${id}`, {
        method: "PATCH"
    })
}