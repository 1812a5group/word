import { LoginData, LoginVal, RegisterVal, UpdataPassword } from '@/types';
import {request} from 'umi';

//登录
export function login(data:LoginVal) {
    return request('/api/auth/login',{
        method:'POST',
        data
    })
}

//注册
export function register(data:RegisterVal) {
    return request('/api/user/register',{
        method:'POST',
        data
    }) 
}

//更改密码
export function updataPassword(data:UpdataPassword) {
    return request('/api/user/password',{
        method:'POST',
        data
    })  
}

//更改信息
export function update(data:LoginData) {
    return request('/api/user/update',{
        method:'POST',
        data
    })  
}