import { request } from "@/.umi/plugin-request/request"; 
import { IPosteritem } from "@/types";

//海报管理列表数据  
export function getPosterList(page = 1, pageSize = 12, params: Partial<IPosteritem> = {}) {
    return request(`/api/poster?page=${page}&pageSize=${pageSize}`, { params })
}

// 删除
export function deletePoster(id: string) {
    return request(`/api/poster/${id}`, {
        method: "DELETE"
    })
}