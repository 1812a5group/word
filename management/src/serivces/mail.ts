import { request } from "@/.umi/plugin-request/request";
import { ImailItem,IReplyItem } from "@/types";

//邮件列表数据  
export function getMailList(page = 1, pageSize = 12, params: Partial<ImailItem> = {}) {
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`, { params })
}
// 删除
export function deleteMail(id: string) {
    return request(`/api/smtp/${id}`, {
        method: "DELETE"
    })
}
// 回复邮件
export function replyMail(data: IReplyItem){
    return request('http://127.0.0.1:7001/mail', {
        method: 'POST',
        data
    })
}