import { SettingVal } from '@/types';
import {request} from 'umi';


//获取系统信息
export function settingVal() {
    return request('/api/setting/get',{method:'POST'})
}

//修改系统信息
export function SetSettingVal(data:SettingVal) {
    return request('/api/setting',{method:'POST',data})
}