import { getpagedata, setpagestatus, delpages } from '@/serivces'
import { makeAutoObservable } from 'mobx'
// import { IpageItem } from '@/types'
import { message } from "antd"
interface IpageItem{
    any:any
}
class Page {
    pageList: Array<IpageItem> = [];
    pageListNum: number = 1
    constructor() {
        makeAutoObservable(this)
    }
    async getpagedata(page: number, pageSize: number, params?: any) {
        let res = await getpagedata(page, pageSize, params);
        this.pageList = res.data[0]
        this.pageListNum = res.data[1]
    }
    async setpagestatus(id: string, data: any) {
        let res = await setpagestatus(id, data);
        message.success("操作成功")
    }
    async delpages(id: string) {
        let res = await delpages(id);
        message.success("删除成功")
    }
    async setalldel(id: string[], page: number, pagesize: number) {
        Promise.all(id.map(item => delpages(item))).then(res => {
            message.success("操作成功")
            this.getpagedata(page, pagesize)
        }).catch(err => {
            message.success("操作失败")
            console.log(err, "多个操作失败");
        })
    }
    async setallstatus(id: string[], data: any, page: number, pagesize: number) {
        Promise.all(id.map(item => setpagestatus(item, data))).then(res => {
            message.success("操作成功")
        }).catch(err => {
            message.success("操作失败")
            console.log(err, "多个操作失败");
        })
    }
    async setallrelease(id: string[], data: any, page: number, pagesize: number) {
        Promise.all(id.map(item => setpagestatus(item, data))).then(res => {
            message.success("操作成功")
            this.getpagedata(page, pagesize)
        }).catch(err => {
            message.success("操作失败")
            console.log(err, "多个操作失败");
        })
    }

}
export default Page;
