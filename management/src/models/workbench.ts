import { makeAutoObservable } from "mobx"
import { getComment, setCommentPass, delCommentitem } from '@/serivces'
import { CommentList } from '@/types'
import { message } from 'antd'
class User {
    // 最新文章
    commentList: CommentList[] = [];
    commentNum = 0;

    constructor() {
        makeAutoObservable(this);
    }
    async getComment(page: number, pagesize: number, params: Partial<CommentList> = { }) {
        let res = await getComment(page, pagesize, params);
        console.log(res);

        if (res.statusCode === 200) {
            this.commentList = res.data[0];
            this.commentNum = res.data[1];
        }
    }
    async setCommentPassDis(id: string, pass: boolean) {
        let res = await setCommentPass(id, pass);
        if (res.statusCode === 200) {
            message.success(res.data.pass ? "评论已通过" : "评论已拒绝")
        }
    }
    async delCommentitem(id: string, page?: number, pagesize?: number) {
        let res = await delCommentitem(id);
        if (res.statusCode === 200) {
            message.success("评论删除成功")
            this.getComment(page as number, pagesize as number)
        }
    }
    async setCommentmultiple(id: string[], pass: boolean, page: number, pagesize: number) {
        Promise.all(id.map(item => setCommentPass(item, pass))).then(res => {
            message.success("操作成功")
            this.getComment(page, pagesize)
        }).catch(err => {
            message.success("操作失败")
            console.log(err, "多个通过拒绝失败");
        })
    }
    async setCommentmultipleDel(id: string[], page: number, pagesize: number) {
        Promise.all(id.map(item => delCommentitem(item))).then(res => {
            message.success("操作成功")
            this.getComment(page, pagesize)
        }).catch(err => {
            message.success("操作失败")
            console.log(err, "多个通过拒绝失败");
        })
    }

}

export default User;
