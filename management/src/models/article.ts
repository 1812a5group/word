import { getArticle, getcategory ,articlePatch,deleteArticle, changeCategory,addCategory,delCategory,addArticleEditor,getArticleDetail,gettag} from '@/serivces'
import { makeAutoObservable, runInAction } from 'mobx'
import { IArticleList, ICategoryItem , IArticleParams, IArticlePatch ,ICategoryPatch,IArtilePostParams,IDetailObj} from '@/types'
import { message } from 'antd'
class Article {
  articleList: Array<IArticleList> = [];
  articleListNum: number = 0;
  categoryList: Array<ICategoryItem> = []
  tagList: Array<ICategoryItem> = []
  detailObj:Partial<IDetailObj> = {}
  constructor() {
    makeAutoObservable(this)
  }
  //获取所有文章数据
  async getArticle(params:IArticleParams) {
    
    let result = await getArticle({...params})
    if (result.statusCode === 200) {
      runInAction(() => {
        this.articleList = result.data[0]
        this.articleListNum = result.data[1]
      })
    }
    return result.data
  }
  //发布数据 草稿数据 首焦推荐 撤销首焦
  async articlePatch(ids:string[],params:IArticlePatch){
    
    Promise.all(ids.map(id=>articlePatch(id,params)))
    .then(res=>{
      
      if(res[0].statusCode===200){
        message.success('操作成功')
        this.getArticle({page:1})
        return res[0].data
      }
    })
    .catch(error=>{
      message.error('操作失败')
    })
  }
  //删除文章数据
  async deleteArticle(ids:string[]){
    Promise.all(ids.map(id=>deleteArticle(id)))
    .then(res=>{
      if(res[0].statusCode===200){
        message.success('操作成功')
        this.getArticle({page:1})
        return res[0].data
      }
    })
    .catch(error=>{
      message.error('操作失败')
    })
  }
  //获取分类管理
  async getcategory() {
    let result = await getcategory()
    
    if (result.statusCode === 200) {
      runInAction(() => {
        this.categoryList = result.data
      })
    }
    return result.data
  }
  //获取标签管理数据
  async gettag() {
    let result = await gettag()
    if (result.statusCode === 200) {
      runInAction(() => {
        this.tagList = result.data
      })
    }
    return result.data
  }
  //更新 分类管理 | 标签管理 数据
  async changeCategory(name:string,id:string,params:ICategoryPatch){
    let result = await changeCategory(name,id,params)
    if(result.statusCode===200){
      message.success(`更新${name==='category'?'分类':'标签'}成功`)
      this.getcategory()
      this.gettag()
    }
  }
  //添加 分类管理 | 标签管理 数据
  async addCategory(name:string,params:ICategoryPatch){
    let result = await addCategory(name,params)
    if(result.statusCode===201){
      message.success(`添加${name==='category'?'分类':'标签'}成功`)
      this.getcategory()
      this.gettag()
    }
  }
  //删除 分类管理 | 标签管理 数据
  async delCategory(name:string,id:string){
    let result = await delCategory(name,id)
    if(result.statusCode===200){
      message.success(`删除${name==='category'?'分类':'标签'}成功`)
      this.getcategory()
      this.gettag()
    }
  }
  //添加文章
  async addArticleEditor(params:IArtilePostParams){
    let result = await addArticleEditor(params)
    if(result.statusCode===201){
      return result
    }
  }
  //获取详情数据
  async getArticleDetail(id:string){
    let result = await getArticleDetail(id)
    if(result.statusCode===200){
      this.detailObj = result.data
    }
    return result.data
  }
}
export default Article;