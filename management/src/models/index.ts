import Search from './search'
import User from './user'
import Article from './article'
import Mail from './mail'
import Workbench from './workbench'
import File from './file'
import View from './view'
import UserData from './userData'
import Setting from './setting'
import Poster from './poster'
import Knowledge from './knowledge'
import Page from './page'

export default {
    search: new Search,
    user: new User,
    article: new Article,
    mail: new Mail,
    workbench: new Workbench,
    file: new File,
    view: new View,
    userData: new UserData,
    setting: new Setting,
    poster: new Poster,
    knowledge: new Knowledge,
    Page :new Page
}
