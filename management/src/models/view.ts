import { deleteView, getViewList } from '@/serivces';
import { makeAutoObservable } from 'mobx';
import { message } from "antd"
import { IViewItem } from '@/types';

class View {
    viewList: Array<IViewItem> = [];
    viewListNum: number = 1

    constructor() {
        makeAutoObservable(this);
    }

    async getViewList(page: number, pageSize: number, params = {}) {
        let result = await getViewList(page, pageSize, params);
        console.log('view  result...', result.data);
        if (result.statusCode === 200) {
            this.viewList = result.data[0];
            this.viewListNum = result.data[1];
        }
        return result;
    }

    //删除
    async deleteView(id: string[]) {
        console.log(id, "这是na'ge传过来的是一个数组id");

        Promise.all(id.map(id => deleteView(id))).then(res => {
            message.success('删除成功');
            this.getViewList(1, 12);
        }).catch(err => {
            this.getViewList(1, 12);
        })
    }
}

export default View;