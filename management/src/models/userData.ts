import { userAction, userData } from '@/serivces'
import { UserDataItem, UserItem } from '@/types'
import {makeAutoObservable} from 'mobx'
import { message } from 'antd';

class UserData {
    userData:UserDataItem[]=[]
    userDataNum:number=0
    constructor() {
        makeAutoObservable(this)
    }
    //获取用户属于
    async getUserData(page:number,pageSize:number,params:Partial<UserItem>) {
        let res = await userData(page,pageSize,params)
        if(res) {
            this.userData = res.data[0]
            this.userDataNum = res.data[1]
        }
    }

    //更改
    async changeAction(data:UserDataItem,page:number,pageSize:number,params:Partial<UserItem>) {
        let res = await userAction(data)
        if(res) {
            message.success('操作成功')
            this.getUserData(page,pageSize,params)
        }
    }

    //批量修改
    async changeAll(dis:UserDataItem[],page:number,pageSize:number,params:Partial<UserItem>) {
        Promise.all(dis.map(item=> userAction(item)))
        .then(res=> {
            message.success('操作成功')
            this.getUserData(page,pageSize,params)
        })
        .catch(res=> {
            message.success('操作失败')
            this.getUserData(page,pageSize,params)
        })
    }
}

export default UserData