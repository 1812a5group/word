import { getPosterList, deletePoster } from '@/serivces';
import { IPosteritem } from '@/types';
import { makeAutoObservable } from 'mobx';
import { message } from 'antd';

class Search {
    posterList: Array<IPosteritem> = [];
    posterNum: number = 0

    constructor() {
        makeAutoObservable(this);
    }

    async getPosterList(page: number, pageSize: number, params = {}) {
        let result = await getPosterList(page, pageSize, params);
        console.log('search  result...', result.data);
        if (result.statusCode === 200) {
            this.posterList = result.data[0];
            this.posterNum = result.data[1];
        }
        return result;
    }

    //删除
    async deletePoster(id: string[]) {
        Promise.all(id.map(id => deletePoster(id))).then(res => {
            message.success('删除成功');
            this.getPosterList(1, 12);
        })
    }
}

export default Search;