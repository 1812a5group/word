import { login, register, updataPassword , update } from "@/serivces";
import { LoginData, LoginVal, RegisterVal, UpdataPassword } from "@/types";
import { removeToken, setToken } from "@/utils";
import { makeAutoObservable } from "mobx"
import {message} from 'antd'

class User{
    isLogin = false;
    userInfo:Partial<LoginData> = {}
    constructor(){
        makeAutoObservable(this);
    }

    //登录
    async login(data:LoginVal){
        let result = await login(data);
        if (result.data){
            this.isLogin = true;
            setToken(result.data.token);
            localStorage.setItem('user',JSON.stringify(result.data))
            
        }
        return result.data;
    }

    //注册
    async register(data:RegisterVal){
        let result = await register(data);
        console.log('register result...', result);
        return result.data;
    }

    //更改密码
    async updataPaw(data:UpdataPassword){
        let result = await updataPassword(data);
        if(result.data) {
            message.success('密码更新成功,请重新登录')
            this.logout()
        }
        return result.data;
    }

    //更改信息
    async update(data:LoginData){
        let result = await update(data);
        if(result.data) {
            message.success('用户信息已保存')
            this.logout()
        }
        return result.data;
    }

    logout(){
        this.isLogin = false;
        this.userInfo = {};
        removeToken();
    }

}

export default User;
