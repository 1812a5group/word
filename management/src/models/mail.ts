import { deleteMail, getMailList } from '@/serivces'
import { makeAutoObservable } from 'mobx'
import { ImailItem } from '@/types'
import { message } from "antd"
class Mail {
    mailList: Array<ImailItem> = [];
    mailListNum: number = 1
    constructor() {
        makeAutoObservable(this)
    }
    async getMailList(page: number, pageSize: number,params={}) {
        let result = await getMailList(page, pageSize,params)
        console.log(" mail result...", result);

        if (result.statusCode === 200) {
            this.mailList = result.data[0]
            this.mailListNum = result.data[1]
        }
    }

    //删除
    async deleteMail(id: string[]) {
        Promise.all(id.map(id => deleteMail(id))).then(res => {
            message.success('删除成功');
            this.getMailList(1,12);
        })
    }
}
export default Mail;