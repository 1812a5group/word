import { SetSettingVal, settingVal } from "@/serivces";
import { SettingVal } from "@/types";
import { makeAutoObservable } from "mobx"
import {message} from 'antd'

class Setting{
    settingVal:Partial<SettingVal> = {}

    constructor(){
        makeAutoObservable(this);
    }

    async getSettingVal() {
        let res = await settingVal()
        if(res) {
            this.settingVal = res.data
            // console.log(res.data);
        }
    }
    async SetSettingVal(data:SettingVal) {
        let res = await SetSettingVal(data)
        console.log(res);
        
        if(res.statusCode==201) {
            this.settingVal = res.data
            message.success('保存成功')
        }
    }
}

export default Setting;
