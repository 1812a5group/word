import { getFileList ,delFile} from '@/serivces'
import { makeAutoObservable, runInAction} from 'mobx'
import { IFileItem, IFileParams } from '@/types'
class File {
    fileList: Array<IFileItem> = []
    fileListCount: number = 0
    constructor() {
        makeAutoObservable(this)
    }
    //获取文件列表数据
    async getFileList(params?: IFileParams) {
        let result = await getFileList(params as IFileParams)
        if (result.statusCode === 200) {
            runInAction(() => {
                this.fileList = result.data[0]
                this.fileListCount = result.data[1]
            })
        }
        return result.data
    }
    //删除文件列表数据
    async delFile(id:string) {
        let result = await delFile(id)
        console.log(result);
        
    }
}
export default File;