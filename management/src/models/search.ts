import { getsearchArticle, deleteRecords } from '@/serivces';
import { ISearchInfo } from '@/types';
import { makeAutoObservable } from 'mobx';
import { message } from 'antd';

class Search {
    searchInfo: Array<ISearchInfo> = [];
    searchInfoNum: number = 0

    constructor() {
        makeAutoObservable(this);
    }

    async getsearchArticle(page: number, pageSize: number, params = {}) {
        let result = await getsearchArticle(page, pageSize, params);
        console.log('search  result...', result.data);
        if (result.statusCode === 200) {
            this.searchInfo = result.data[0];
            this.searchInfoNum = result.data[1];
        }
        return result;
    }

    //删除
    async deleteRecords(id: string[]) {
        Promise.all(id.map(id => deleteRecords(id))).then(res => {
            message.success('删除成功');
            this.getsearchArticle(1, 12);
        })
    }
}

export default Search;