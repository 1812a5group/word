export interface ISearchInfo {
  id: string;
  type: string;
  keyword: string;
  count: number;
  createAt: string;
  updateAt: string;
}