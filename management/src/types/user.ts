//登录请求参数
export interface LoginVal {
  name: string;
  password: string;
}
//登录响应数据
export interface LoginData {
  id: string;
  name: string;
  avatar?: any;
  email?: any;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
  token: string;
}

//注册参数
export interface RegisterVal {
  confirm: string;
  name: string;
  password: string;
}

//更改密码
export interface UpdataPassword extends LoginData {
  newPassword: string;
  oldPassword: string;
}