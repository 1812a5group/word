export interface IFileItem {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
}
export interface IFileParams {
  page: number
  pageSize: number
  originalname?: string
  type?: string
}