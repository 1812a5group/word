export interface ImailItem {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: any;
  html: string;
  createAt: string;
}
export interface IReplyItem {
  to: string;
  subject: string;
  html: string;
}