export interface UserItem {
    name: string;
    email: string;
    role: string;
    status: string;
}

export interface UserDataItem {
  id: string;
  name: string;
  avatar?: string;
  email?: string;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
}