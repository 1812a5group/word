export interface IPosteritem {
    id: string,
    name: string,
    size: 0,
    pageUrl: string,
    imgUrl: string,
    createAt: string
}