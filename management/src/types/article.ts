export interface IArticleList {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: ITag[];
  category?: ITag;
}

export interface ITag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface IcolumnsItem {
  title: string;
  width: number;
  dataIndex?: string;
  key: string;
  fixed?: string;
}
export interface ICategoryItem {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface IArticleParams {
  title?: string;
  page: number;
  pageSize?: number;
  status?: string;
  category?: any;
}
export interface IArticlePatch {
  status?: string ;
  isRecommended?: boolean;
}

export interface ICategoryPatch {
  label:string
  value:string
}
export interface IArtilePostParams {
  content?: string;
  html?: string;
  status?: string;
  title?: string;
  toc?: string;
}
export interface IDetailObj {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: any;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: any;
  tags: any[];
}