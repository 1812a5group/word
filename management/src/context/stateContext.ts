import React from 'react';
import store from '@/models'

export default React.createContext(store)
