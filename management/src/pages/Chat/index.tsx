import React, { useEffect, useRef, useState } from "react";
import io, { Socket } from 'socket.io-client';
import Cls from 'classnames';
import md5 from 'md5'
import './index.less'

interface UserItem {
    userName?: string;
    msg: string
}

const COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];
const TYPING_TIMER_LENGTH = 400; // ms
let timeout = 0
const chat: React.FC = function () {
    const socket = useRef<Socket>();
    const chatArea = useRef<HTMLDivElement>(null);
    const [userName, setuserName] = useState('')    //名字
    const [login, setlogin] = useState(true);   //登录页面开关
    const [msg, setmsg] = useState(''); //消息
    const [userArr, setuserArr] = useState<UserItem[]>([])  //数据
    const [isTyping, setIsTyping] = useState(false);    //是否显示正在输入
    useEffect(() => {
        if (chatArea.current){
            console.log(chatArea.current?.scrollHeight!);
            chatArea.current!.scrollTop = chatArea.current?.scrollHeight!;
        }
    }, [msg])

    
    useEffect(() => {
        socket.current = io('ws://10.0.9.7:3000')
        //登录
        socket.current.on('login', (data) => {
            setuserArr(userArr => [...userArr, {
                msg: 'Welcome to Socket.IO Chat – '
            }, {
                msg: `there are ${data.numUsers} participants`
            }])
        });
        //添加消息
        socket.current.on('new message', (data) => {
            setuserArr(userArr => [...userArr, {
                userName: data.username,
                msg: data.message,
            }])
        });
        //用户添加进来
        socket.current.on('user joined', (data) => {
            setuserArr(userArr => [...userArr, {
                msg: `${data.username} 加入`
            }, {
                msg: `there are ${data.numUsers} participants,`
            }])
        });

        //退出聊天
        socket.current.on('user left', (data) => {
            setuserArr(userArr => [...userArr, {
                msg: `${data.username} 退出聊天`
            }, {
                msg: `there are ${data.numUsers} participants,`
            }])
        });

        //正在输入消息
        socket.current.on('typing', (data) => {
            setuserArr(userArr => [...userArr, {
                userName: data.username,
                msg: '正在输入消息',
            }])
        });

        //删除正在输入消息
        socket.current.on('stop typing', (data) => {
            setuserArr(userArr => {
                let index = userArr.findIndex(item => {
                    return (item.userName === data.username) && (item.msg === '正在输入消息');
                })
                console.log(index,'index');
                
                if (index > -1) {
                    userArr.splice(index, 1);
                }
                return [...userArr];
            });
        });

        // ondisconnect断网之前执行
        socket.current.on('disconnect', () => {
            setuserArr(userArr=>[...userArr, {
                msg: 'you have been disconnected'
            }])
        });

        //重新连接
        socket.current.on('reconnect', () => {
            setuserArr(userArr=>[...userArr, {
                msg: 'you have been reconnected'
            }])
            if (userName) {
                socket.current!.emit('add user', userName);
            }
        });

        // 连接报错
        socket.current.on('reconnect_error', () => {
            setuserArr(userArr=>[...userArr, {
                msg: 'attempt to reconnect has failed'
            }])
        });

    }, [])

    //添加用户
    function AddUser(e: React.KeyboardEvent<HTMLInputElement>) {
        if (userName && e.keyCode === 13) {
            if (userName) {
                setlogin(false)
                socket.current?.emit('add user', userName)
            }
        }
    }

    //添加消息
    function AddMsg(e: React.KeyboardEvent<HTMLInputElement>) {
        if (userName && e.keyCode === 13) {
            if (userName) {
                socket.current?.emit('new message', msg);
                setmsg('')
                console.log(userName, msg);
                setuserArr(userArr => [...userArr, {
                    userName,
                    msg,
                }])
            }
        }
    }

    //正在输入
    function typing() {
        if (!isTyping){
            setIsTyping(true);
            socket.current!.emit('typing');
            timeout = setTimeout(() =>{
                socket.current!.emit('stop typing');
                setIsTyping(false);
            }, TYPING_TIMER_LENGTH) as unknown as number;
        }else{
            clearTimeout(timeout);
            timeout = setTimeout(() =>{
                socket.current!.emit('stop typing');
                setIsTyping(false);
            }, TYPING_TIMER_LENGTH) as unknown as number;
        }


    }
    //颜色处理
    function userColor(userName: string) {
        let color = md5(userName);
        return parseInt(color.slice(-1), 16) % COLORS.length
    }

    return <div className="pages">
        <div className={Cls('chats', login ? 'page' : '')}>
            <div className="chatArea" ref={chatArea}>
                <ul className="messages">
                    {
                        userArr.map((item, index) => {
                            return <li key={index} className={item.userName ? 'chat' : 'log'}>
                                {
                                    item.userName ?
                                        <span><b style={{ color: COLORS[userColor(item.userName)] }}>{item.userName}</b>  {item.msg}</span> :
                                        <span>{item.msg}</span>
                                }
                            </li>
                        })
                    }
                </ul>
            </div>
            <input
                className="inputMessage"
                placeholder="Type here..."
                value={msg}
                onChange={(e) => setmsg(e.target.value)}
                onKeyDown={(e) => {
                    AddMsg(e)
                }}
                onInput={()=> typing()}
            />
        </div>
        {
            login ? <div className="login page">
                <div className="form">
                    <h3 className="title">What's your nickname?</h3>
                    <input className="usernameInput" type="text" value={userName} onChange={(e) => setuserName(e.target.value)} onKeyDown={(e) => {
                        AddUser(e)
                    }} />
                </div>
            </div> : ''
        }
    </div>
}

export default chat