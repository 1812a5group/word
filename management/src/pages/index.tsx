import react, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import BubbleComments from '@/componets/BubbleComments';
import BubblePaper from '@/componets/BubblePaper';
import PanelNav from '@/componets/PanelNav';
import { Popconfirm, Modal, Input, Card } from 'antd';
import style from './index.less'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
const { TextArea } = Input;
let IndexPage: react.FC = function () {
  let store = useStore()
  let [id, setId] = useState("")
  let [visible, setVisible] = useState(false)
  let [text, setText] = useState("")
  let [page, setPage] = useState(1)
  let [pagesize, setPagesize] = useState(12)

  useEffect(() => {
    store.article.getArticle({page:1,pageSize:6})
    store.workbench.getComment(1, 6)
  }, [])
  // 设置通过拒绝
  async function setcommentpass(id: string, pass: boolean) {
    await store.workbench.setCommentPassDis(id, pass)
    store.workbench.getComment(1, 6)

  }
  // 删除评论
  async function confirm() {
    if (id != "") {
      await store.workbench.delCommentitem(id)
      store.workbench.getComment(1, 6)

    }
  }
  // 回复
  function showModal() {
    setVisible(true);
  };
  function handleOk(e: react.MouseEvent<HTMLElement, MouseEvent>) {
    // 万事具备 但是传递参数少 等身份信息 ----恒
    console.log(e);
    setVisible(false)
  };
  function handleCancel(e: react.MouseEvent<HTMLElement, MouseEvent>) {
    setVisible(false)
  };
  // 获取文本框内容
  function setTextVal(e: react.ChangeEvent<HTMLTextAreaElement>) {
    setText(e.target.defaultValue)
  }
  return (
    <div className={style.workbench}>
      <Card title="面板导航" style={{ width: "100%" }}>
        <PanelNav />
      </Card>

      <Card title="快速导航" style={{ width: "100%", marginTop: "24px" }}>
        <div className={style.quicknav}>
          <Link to="/article">文章管理</Link>
          <Link to="/comment">评论管理</Link>
          <Link to="/user">用户管理</Link>
          <Link to="/view">访问管理</Link>
          <Link to="/setting">系统设置</Link>
        </div>
      </Card>

      <Card title="最新文章" extra={<Link to="/article">全部文章</Link>} style={{ width: "100%", marginTop: "24px" }}>
        <div className={style.article}>
          {store.article.articleList.map((item, index) => {
            return <div key={item.id} className={style.articleItem}>
              <img src={item.cover} alt="文章封面" />
              <p>{item.title}</p>
            </div>
          })}
        </div>
      </Card>

      <Card title="最新评论" extra={<Link to="/comment">全部评论</Link>} style={{ width: "100%", marginTop: "24px" }}>
        <div className={style.latestComments}>
          {
            store.workbench.commentList.map((item, index) => {
              return <li key={index} className={style.commentsItem} >
                <p>

                  {item.name} 在
                  <BubblePaper direction="right" url={item.url} />
                  评论
                  <BubbleComments direction="top" content={item.content} />
                  {/*  js-cuke 下午看一下  */}
                  {item.pass ? <b> <i className={style.yellow}></i> 通过 </b> : <b> <i className={style.green}></i> 未通过 </b>}
                </p>
                <p>
                  <span onClick={() => setcommentpass(item.id, true)}>通过</span>
                  <span onClick={() => setcommentpass(item.id, false)}>拒绝</span>
                  <span onClick={() => showModal()}>回复</span>

                  <Popconfirm
                    title="确认删除这个评论？"
                    onConfirm={confirm}
                    okText="确定"
                    cancelText="取消"
                  >
                    <span onClick={() => setId(item.id)}>删除</span>
                  </Popconfirm>
                </p>
              </li>
            })
          }
        </div>
      </Card >

      <Modal
        title="回复评论"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <TextArea rows={4} onChange={(e) => setTextVal(e)} />
      </Modal>
    </div>
  );
}

export default observer(IndexPage)
