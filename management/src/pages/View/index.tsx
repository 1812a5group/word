import React, { Key, ReactElement, useEffect, useState } from 'react'
import { Table, Button, Empty, Popconfirm, message, Badge, Tooltip } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite';
import style from "./index.less"
import useStore from '@/context/useStore';
import { IViewItem } from '@/types';
import SearchHead from '@/componets/SearchHead'
import moment from 'moment';
import Smtable from '@/componets/Smtable';

interface Props {

}

let Statistical: React.FC = function (props) {
    const store = useStore() 
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);

    const [params, setParams] = useState({});
    const [showDelete, setShowDelete] = useState(false);
    useEffect(() => {
        store.view.getViewList(page, pageSize, params)
    }, [page, params])
    const [selectedRoekeys, setSelectedRoekeys] = useState<Key[]>([])
    function onSelectChange(selectedRoekeys: Key[]) {
        setSelectedRoekeys(selectedRoekeys)
        selectedRoekeys.length > 0 ? setShowDelete(true) : setShowDelete(false)
    }
    //多选框
    const rowSelection = {
        selectedRoekeys,
        onChange: onSelectChange
    };

    function getParams(params: any) {
        store.view.getViewList(page, pageSize, params)
    }
    const info = [{
        name: "ip", label: "IP", placeholder: "请输入IP地址"
    }, {
        name: "userAgent", label: "UA", placeholder: "请输入User Agent"
    }, {
        name: "url", label: "URL", placeholder: "请输入 URL"
    }, {
        name: "address", label: "地址", placeholder: "请输入地址"
    }, {
        name: "browser", label: "浏览器", placeholder: "请输入浏览器"
    }, {
        name: "engine", label: "内核", placeholder: "请输入内核"
    }, {
        name: "os", label: "OS", placeholder: "请输入操作系统"
    }, {
        name: "device", label: "设备", placeholder: "请输入设备"
    }]

    const columns: any = [
        {
            title: 'URL', dataIndex: 'url', width: 200, fixed: 'left', key: "name",
            render: (text:string) => <a style={{ color: "#1890ff" }}>{text}</a>,
        }, {
            title: 'IP', dataIndex: 'ip', key: "1"
        }, {
            title: '浏览器', dataIndex: "browser", key: "2"
        }, {
            title: '内核', dataIndex: 'engine', key: "3"
        }, {
            title: '操作系统', dataIndex: 'os', key: "4"
        }, {
            title: '设备', dataIndex: 'device', key: "5"
        }, {
            title: '地址', dataIndex: 'address', key: "6"
        }, {
            title: '访问量',
            dataIndex: 'count',
            key: "7",
            render: (text: string) => (
                <Badge
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#52c41a' }}
                />
            ),
        }, {
            title: '访问时间',
            dataIndex: 'createAt',
            key: "8",
            render: (text: string) => (
                <div>{moment(text).format("YYYY-MM-DD HH:mm:ss")}</div>
            ),
        }, {
            title: '操作',
            width: 120,
            fixed: 'right',
            render: (record: IViewItem) => (
                <span>
                    <Popconfirm
                        title="确认删除这个搜索记录？"
                        onConfirm={() => {store.view.deleteView([record.id])}}
                        onCancel={() => { message.error('取消删除') }}
                        okText="确认"
                        cancelText="取消"
                    >
                        <a style={{ color: "#1890ff" }}>删除</a>
                    </Popconfirm>
                </span>
            ),
        }
    ];

    return (
        <>
            {/* 搜索框 */}
            <SearchHead info={info} getParams={getParams} />

            {
                store.view.viewList ? <div className={style.searchBottom}>

                    <div className={style.button}>
                        <div className={style.btn_left} style={{ display: showDelete ? "block" : "none" }}>
                            <Popconfirm
                                title="确认删除？"
                                onConfirm={() => {
                                    store.view.deleteView(selectedRoekeys as string[])
                                    setShowDelete(false)
                                }}
                                onCancel={() => message.error('取消删除')}
                                okText="确认"
                                cancelText="取消"
                            >
                                <Button className={style.deletebtn} danger>删除</Button>
                            </Popconfirm>
                        </div>

                        <Tooltip placement="top" title="刷新">
                            <p className={style.btn_right} onClick={() => {
                               store.view.getViewList(page,pageSize)
                            }}><ReloadOutlined /></p>
                        </Tooltip>
                    </div>
                    <Table
                        rowKey="id"
                        scroll={{ x: 1300 }}
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={store.view.viewList}
                        pagination={{
                            showSizeChanger: true,
                            pageSizeOptions: ['8', '10', '20', '30'],
                            total: store.view.viewListNum,
                            showTotal: (total) => `共${total}条 `,
                            onChange: (page, pageSize) => {
                                store.view.getViewList(page, pageSize!)
                                setPage(page)
                                setPageSize(pageSize as number)
                            }
                        }}
                    />
                </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            }
            {/* <Smtable
                info={store.view.viewList}
                columns={columns}
                infoLength={store.view.viewListNum}
                scrollX={0}
                deleteList={store.view.deleteView}
                params={params}
            /> */}
        </>
    )
}

export default observer(Statistical);
