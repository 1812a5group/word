import React, { useEffect, useState } from 'react'
import { Tabs, Radio, Space, Form, Input, Button } from 'antd';
import style from './index.less'
import { FileImageOutlined, InfoCircleOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import { SettingVal } from '@/types';
import { observer } from 'mobx-react-lite'
import Monaco from 'react-monaco-editor';
const { TabPane } = Tabs;

interface I1anVal {
    title: string;
    content: string;
    key: string;
}

let setting: React.FC = function (props) {
    const store = useStore()
    const [form] = Form.useForm();
    //系统设置
    //保存
    const onFinish = (values: SettingVal) => {
        store.setting.SetSettingVal({
            systemUrl: values.systemUrl,
            adminSystemUrl: values.adminSystemUrl,
            systemTitle: values.systemTitle,
            systemLogo: values.systemLogo,
            systemFavicon: values.systemFavicon,
            systemFooterInfo: values.systemFooterInfo,
        })
        // setvalues(values)
    };


    //国际化设置
    const [newTabIndex, setnewTabIndex] = useState<number>(0)
    const [i18nData, seti18nData] = useState<I1anVal[]>([])
    const [activeKey, setactiveKey] = useState<string>('')
    const TwoOnChange = (activeKey: string) => {
        setactiveKey(activeKey);
    };

    useEffect(() => {
        if (store.setting.settingVal.i18n) {
            let obj = JSON.parse(store.setting.settingVal.i18n!)
            Object.keys(obj).forEach(((item, index) => {
                console.log(JSON.stringify(obj[item]));
                i18nData.push({
                    title: item,
                    content: JSON.stringify(obj[item], null, 2),
                    key: String(index + 1)
                })
                seti18nData(i18nData)
            }))
            setactiveKey(i18nData[0].key)
        }
        form.setFieldsValue({
            ...store.setting.settingVal
        })
    }, [store.setting.settingVal.i18n])

    const onEdit = (targetKey: string | React.MouseEvent<Element, MouseEvent> | React.KeyboardEvent<Element>, action: "add" | "remove") => {
        console.log(action, targetKey);
        if (action === 'add') {
            TwoAdd()
        } else {
            remove((targetKey as string))
        }
    };
    const TwoAdd = () => {
        setnewTabIndex(newTabIndex => newTabIndex++)
        const activeKey = `newTab${newTabIndex}`;
        const newPanes = [...i18nData];
        newPanes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
        setactiveKey(activeKey)
        seti18nData(newPanes)
    };
    const remove = (targetKey: string) => {
        let newActiveKey = activeKey;
        let lastIndex: number = 0;
        i18nData.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const newPanes = i18nData.filter(pane => pane.key !== targetKey);
        if (newPanes.length && newActiveKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newPanes[lastIndex].key;
            } else {
                newActiveKey = newPanes[0].key;
            }
        }
        setactiveKey(i18nData[0].key)
        seti18nData(newPanes)
    };

    //seo设置
    //保存
    const onSeoFinish = (values: any) => {
        store.setting.SetSettingVal({
            seoKeyword: values.seoKeyword,
            seoDesc: values.seoDesc,
        })
    }

    //数据统计
    //保存
    const onDataFinish = (values: any) => {
        store.setting.SetSettingVal({
            baiduAnalyticsId: values.baiduAnalyticsId,
            googleAnalyticsId: values.googleAnalyticsId,
        })
    }
    //OSS设置
    //保存
    const onOssFinish = (values: any) => {
        console.log(values);
    }
    //SMTP服务
    //保存
    const onSmtpFinish = (values: any) => {
        store.setting.SetSettingVal({
            smtpHost: values.smtpHost,
            smtpPort: values.smtpPort,
            smtpUser: values.smtpUser,
            smtpPass: values.smtpPass,
            smtpFromUser: values.smtpFromUser,
        })
    }

    //国际化设置
    const onChangeHandle = (val: string) => {
        console.log(val);
    }
    const editorDidMountHandle = (editor: any, monaco: any) => {
        // console.log('editorDidMount', editor, monaco);
        editor.focus();
    }
    const options = {
        selectOnLineNumbers: true,
        renderSideBySide: false,
        enableSplitViewResizing: true,
        vertical: 'auto',
        style: { height: '500px' }
    };

    return (
        <div className={style.setting}>
            <Tabs tabPosition='left'>
                <TabPane tab="系统设置" key="1">
                    <Form
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onFinish}
                    >
                        <Form.Item label="系统地址" name='systemUrl'>
                            <Input placeholder='请输入系统地址' />
                        </Form.Item>
                        <Form.Item label="后台地址" name='adminSystemUrl'>
                            <Input placeholder='请输入后台地址' />
                        </Form.Item>
                        <Form.Item label="系统标题" name='systemTitle'>
                            <Input placeholder='请输入系统标题' />
                        </Form.Item>
                        <Form.Item label="Logo" name='systemLogo'>
                            <Input placeholder='请输入Logo' suffix={
                                <FileImageOutlined />
                            } />
                        </Form.Item>
                        <Form.Item label="Favicon" name='systemFavicon'>
                            <Input placeholder='请输入Favicon' suffix={
                                <FileImageOutlined />
                            } />
                        </Form.Item>
                        <Form.Item label="页脚信息" name='systemFooterInfo'>
                            <Input.TextArea placeholder='请输入页脚信息' rows={8} />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </TabPane>
                <TabPane tab="国际化设置" key="2">
                    <Form
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onFinish}
                    >
                        <Tabs
                            type="editable-card"
                            onChange={TwoOnChange}
                            activeKey={activeKey}
                            onEdit={onEdit}
                        >
                            {i18nData.map(pane => (
                                <TabPane tab={pane.title} key={pane.key}>
                                    <Monaco
                                        height='460'
                                        // language="javascript"
                                        value={pane.content}
                                        // options={options}
                                        onChange={onChangeHandle}
                                    // editorDidMount={editorDidMountHandle}
                                    />
                                </TabPane>
                            ))}
                        </Tabs>
                        <Button type="primary" htmlType="submit">
                            保存
                        </Button>
                    </Form>
                </TabPane>
                <TabPane tab="SEO设置" key="3">
                    <Form
                        // {...formItemLayout}  
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onSeoFinish}
                    >
                        <Form.Item label="关键词" name='seoKeyword'>
                            <Input placeholder='请输入关键词' />
                        </Form.Item>
                        <Form.Item label="描述信息" name='seoDesc'>
                            <Input.TextArea placeholder='请输入描述信息' rows={8} />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </TabPane>
                <TabPane tab="数据统计" key="4">
                    <Form
                        // {...formItemLayout}  
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onDataFinish}
                    >
                        <Form.Item label="百度统计" name='baiduAnalyticsId'>
                            <Input placeholder='请输入百度统计' />
                        </Form.Item>
                        <Form.Item label="谷歌分析" name='googleAnalyticsId'>
                            <Input placeholder='请输入谷歌分析' />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </TabPane>
                <TabPane tab="OSS设置" key="5">
                    <Form
                        // {...formItemLayout}  
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onOssFinish}
                    >
                        <Monaco
                            height='400px'
                            language="javascript"
                            value={store.setting.settingVal.oss}
                            options={options}
                            onChange={onChangeHandle}
                            editorDidMount={editorDidMountHandle}
                        />
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </TabPane>
                <TabPane tab="SMTP服务" key="6">
                    <Form
                        // {...formItemLayout}  
                        layout='vertical'
                        form={form}
                        initialValues={{ layout: 'vertical' }}
                        onFinish={onSmtpFinish}
                    >
                        <Form.Item label="SMTP 地址" name='smtpHost'>
                            <Input placeholder='请输入SMTP 地址' />
                        </Form.Item>
                        <Form.Item label="SMTP 端口（强制使用 SSL 连接）" name='smtpPort'>
                            <Input placeholder='请输入SMTP 端口（强制使用 SSL 连接）' />
                        </Form.Item>
                        <Form.Item label="SMTP 用户" name='smtpUser'>
                            <Input placeholder='请输入SMTP 用户' />
                        </Form.Item>
                        <Form.Item label="SMTP 密码" name='smtpPass'>
                            <Input placeholder='请输入SMTP 密码' />
                        </Form.Item>
                        <Form.Item label="发件人" name='smtpFromUser'>
                            <Input placeholder='请输入发件人' />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </TabPane>
            </Tabs>
        </div>
    )
}

export default observer(setting)
