import React, { useEffect, useState, Key } from 'react';
import { history } from 'umi'
import { List, Card, Row, Col, Tabs, Form, Input, InputNumber, Button, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore';
import style from './index.less'
import { } from '@/types';
import Cls from 'classnames'
const { TabPane } = Tabs;
const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
};
interface Props {

}

let Ownspace: React.FC = function (props) {
    const store = useStore()
    const [dataArr, setdataArr] = useState<string[]>([])
    useEffect(() => {
        //文章
        store.article.getArticle({ page: 1 })
        //分类
        store.article.getcategory()
        //标签
        store.article.gettag()
        //文件
        store.file.getFileList()
        //评论
        store.workbench.getComment(1, 12)
    }, [])

    const onFinish = async (values: any) => {
        let res = await store.user.update({
            ...JSON.parse(localStorage.getItem('user')!),
            name: values.name,
            email: values.email
        })
    };
    const onFinishPaw = async (values: any) => {
        let res = await store.user.updataPaw({
            newPassword: values.newPassword,
            oldPassword: values.oldPassword,
            ...JSON.parse(localStorage.getItem('user')!)
        })
        if (res) {
            history.push('/login')
        }
    };
    const [form] = Form.useForm();
    useEffect(() => {
        setdataArr([
            `累计发表了 ${store.article.articleListNum} 篇文章`,
            `累计创建了 ${store.article.categoryList.length} 个分类`,
            `累计创建了 ${store.article.tagList.length} 个标签`,
            `累计上传了 ${store.file.fileListCount} 个文件`,
            `累计获得了 ${store.workbench.commentNum} 个评论`,
        ])
    }, [store.article.articleListNum,
    store.article.categoryList,
    store.article.tagList,
    store.file.fileListCount,
    store.workbench.commentNum
    ])
    useEffect(() => {
        form.setFieldsValue({
            ...store.user.userInfo
        })
    }, [store.user.userInfo])

    return (
        <div className={Cls(style.ownspace, 'ownspace')}>
            <Row className={style.ownspaceRow}>
                <Col span={12}>
                    <List
                        className={style.leftList}
                        header={<div>系统概览</div>}
                        // footer={<div>Footer</div>}
                        bordered
                        dataSource={dataArr}
                        renderItem={item => (
                            <List.Item>
                                <span>{item}</span>
                            </List.Item>
                        )}
                    />
                </Col>
                <Col span={12}>
                    <Card title="个人资料" className={style.leftList} bordered={false} style={{ width: '100%' }}>
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="基本设置" key="1">
                                <Form
                                    {...layout}
                                    form={form}
                                    onFinish={onFinish}
                                    labelAlign='left'
                                >
                                    <Form.Item className='Avatar' name='avatar' label={false} style={{ width: '100%', textAlign: 'center' }}>
                                        <Avatar size={64} src={JSON.parse(localStorage.getItem('user')!).avatar} />
                                    </Form.Item>
                                    <Form.Item label="用户名" name='name' initialValue={JSON.parse(localStorage.getItem('user')!).name}>
                                        <Input />
                                    </Form.Item>
                                    <Form.Item name='email' label="邮箱" initialValue={JSON.parse(localStorage.getItem('user')!).email} rules={[{ type: 'email' }]}>
                                        <Input />
                                    </Form.Item>
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit">
                                            保存
                                        </Button>
                                    </Form.Item>
                                </Form>
                            </TabPane>
                            <TabPane tab="更新密码" key="2">
                                <Form
                                    {...layout}
                                    form={form}
                                    onFinish={onFinishPaw}
                                    labelAlign='left'
                                    requiredMark={false}
                                >
                                    <Form.Item
                                        name="oldPassword"
                                        label="原密码"
                                        rules={[
                                            {
                                                required: true,
                                                message: '请输入密码!',
                                            },
                                        ]}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                    <Form.Item
                                        name="newPassword"
                                        label="新密码"
                                        rules={[
                                            {
                                                required: true,
                                                message: '请输入新密码!',
                                            },
                                        ]}
                                    >
                                        <Input.Password />
                                    </Form.Item>

                                    <Form.Item
                                        name="confirm"
                                        label="确认密码"
                                        dependencies={['password']}
                                        rules={[
                                            {
                                                required: true,
                                                message: '密码不一致!',
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(_, value) {
                                                    if (!value || getFieldValue('newPassword') === value) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(new Error('密码不一致!'));
                                                },
                                            }),
                                        ]}
                                    >
                                        <Input.Password />
                                    </Form.Item>
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit">
                                            更新
                                        </Button>
                                    </Form.Item>
                                </Form>
                            </TabPane>
                        </Tabs>
                    </Card>
                </Col>
            </Row>
        </div >
    )
}

export default observer(Ownspace)
