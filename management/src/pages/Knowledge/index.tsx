import SearchHead from '@/componets/SearchHead'
import React, { ReactElement, useEffect, useState } from 'react'
import style from "./index.less"
import { Empty, Button, Card, Avatar, Popconfirm, message, Tooltip, Drawer } from "antd"
import { PlusOutlined, EditOutlined, CloudUploadOutlined, CloudDownloadOutlined, DeleteOutlined, SettingOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
import { login } from '@/serivces';
import KnowledgeDrawer from '@/componets/KnowledgeDrawer';
import { IKnowledgeItem } from '@/types';
const { Meta } = Card;

interface Props {

}

let Knowledge: React.FC = function (props) {
    const store = useStore()
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [flag, setFlag] = useState(false)
    const [params, setParams] = useState({});
    const [visible, setVisible] = useState(false)
    const [item, setItem] = useState<Partial<IKnowledgeItem>>({})

    useEffect(() => {
        store.knowledge.getKnowledgeList(page, pageSize, params)
    }, [params])

    function getParams(params: any) {
        store.knowledge.getKnowledgeList(page, pageSize, params)
    }
    const info = [{
        name: "title", label: "名称", placeholder: "请输入搜索类型"
    }, {
        name: "status", label: "状态", placeholder: "", select: [{ value: "publish", text: "已发布" }, { value: "draft", text: "草稿" }]
    }]

    function showDrawer(item: IKnowledgeItem) {
        setVisible(true);
        setItem(item)
    }

    function onClose() {
        setVisible(false)
    }

    return (
        <>
            {/* 搜索框 */}
            <SearchHead info={info} getParams={getParams} />

            <div className={style.knowledge_bottom}>
                <div className={style.new_top}>
                    <div className={style.new_btn}>
                        <Button type="primary"><PlusOutlined />新建</Button>
                    </div>
                </div>
                <div className={style.card}>
                    {
                        store.knowledge.knowledgeList ?
                            <div className={style.card_box}>
                                {
                                    store.knowledge.knowledgeList.map((item => {
                                        return <div key={item.id} className={style.card_content}>
                                            <Card
                                                cover={<img alt="example" src={item.cover} />}
                                                actions={[
                                                    <EditOutlined  key="edit" onClick={() => {
                                                        console.log("编辑");

                                                    }} />,
                                                    item.status === "publish" ?
                                                        <Tooltip placement="top" title="设为草稿"><CloudDownloadOutlined
                                                            onClick={() => {
                                                                console.log("存为草稿")
                                                                store.knowledge.editKnowledgePublish(item.id, false)
                                                            }} /></Tooltip> :
                                                        <Tooltip placement="top" title="发布线上"><CloudUploadOutlined
                                                            onClick={() => {
                                                                console.log("发布线上") 
                                                                store.knowledge.editKnowledgePublish(item.id, true)
                                                            }} /></Tooltip>,

                                                    <SettingOutlined key="setting" onClick={() => showDrawer(item)} />,
                                                    <Popconfirm
                                                        title="确认删除？"
                                                        onConfirm={() => { store.knowledge.deleteKnowledgeCard([item.id]) }}
                                                        onCancel={() => { message.error('取消删除') }}
                                                        okText="确认"
                                                        cancelText="取消"
                                                    >
                                                        <DeleteOutlined />
                                                    </Popconfirm>
                                                ]}
                                            >
                                                <Meta title={item.title} description={item.summary} />
                                            </Card>

                                        </div>
                                    }))
                                }
                                {/* 弹框 */}
                                <KnowledgeDrawer
                                    visible={visible}
                                    onClose={onClose}
                                    item={item}
                                />
                            </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />

                    }
                </div>
            </div>
        </>
    )
}

export default observer(Knowledge)
