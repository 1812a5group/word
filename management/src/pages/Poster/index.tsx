import SearchHead from '@/componets/SearchHead'
import React, { ReactElement, useEffect, useState } from 'react'
import style from "./index.less"
import { Alert, Upload, message, Empty } from 'antd';
import { InboxOutlined } from '@ant-design/icons'; 
import { Link, useHistory } from 'react-router-dom';
import { storeAnnotation } from 'mobx/dist/internal';
import useStore from '@/context/useStore';
const { Dragger } = Upload;

interface Props {

}

let posters: React.FC = function (props) {
    const store = useStore();
    const history = useHistory()
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState({});
    
    useEffect(() => {
        store.poster.getPosterList(page, pageSize, params)
    }, [params])
    console.log(store.poster.posterList,"海报管理数据");
    
    const info = [{
        name: "type", label: "文件名称", placeholder: "请输入文件名称"
    }]
    const propsAddress = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    const messageinfo = (
        <>系统检测到<strong> 阿里云OSS配置 </strong> 未完善，<span onClick={() => history.push("/setting")}>点我立即完善</span></>
    )
    return (
        <>
            <div className={style.tipe}>
                <Link to="setting">
                    <Alert message={messageinfo} type="warning" />
                </Link>
            </div>
            <div className={style.file_upload}>
                <Dragger {...propsAddress} style={{ background: "#fff" }}>
                    <p className="ant-upload-drag-icon"><InboxOutlined /></p>
                    <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                    <p className="ant-upload-hint">将文件上传到 OSS, 如未配置请先配置</p>
                </Dragger>
            </div>
            <SearchHead info={info} />
            <div className={style.poster_bottom}>
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
        </>
    )
}

export default posters
