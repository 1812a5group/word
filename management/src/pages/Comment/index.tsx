import React, { ReactElement, useEffect, useState } from 'react'
import style from './index.less'
import { Table, Popconfirm, Modal, Input, Button, Tooltip } from 'antd'
import BubbleComments from '@/componets/BubbleComments';
import BubblePaper from '@/componets/BubblePaper';
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import { CommentList } from '@/types'
import moment from 'moment'
import { ReloadOutlined } from '@ant-design/icons';
import SearchHead from '@/componets/SearchHead'
const { TextArea } = Input;
interface Props {

}

let comments: React.FC = function (props) {
    let store = useStore();
    let [id, setId] = useState("");
    let [visible, setVisible] = useState(false)
    let [flag, setflag] = useState(false)
    let [text, setText] = useState("")
    let [page, setPage] = useState(1)
    let [pagesize, setPagesize] = useState(12)
    let [params, seParams] = useState({ })

    let [selectedRows, setSelectedRows] = useState<string[]>([])
    useEffect(() => {
        store.workbench.getComment(1, 12, params)
    }, [params])
    // 设置页码
    async function totals(page: number, pagesize?: number) {
        await store.workbench.getComment(page, pagesize as number)
        setPage(page)
        setPagesize(pagesize as number)
    }
    // 设置通过拒绝
    async function setcommentpass(id: string, pass: boolean) {
        await store.workbench.setCommentPassDis(id, pass)
        await store.workbench.getComment(page, pagesize)
    }
    // 多个通过 拒绝
    async function multiple(pass: boolean) {
        await store.workbench.setCommentmultiple(selectedRows, pass, page, pagesize);
    }
    // 删除评论
    async function confirm() {
        if (id != "") {
            await store.workbench.delCommentitem(id, page, pagesize)
        }
    }
    // 多个删除
    async function multipleDel() {
        await store.workbench.setCommentmultipleDel(selectedRows, page, pagesize);
    }
    // 回复
    function showModal() {
        setVisible(true);
    };
    function handleOk(e: React.MouseEvent<HTMLElement, MouseEvent>) {
        // 万事具备 但是传递参数少 等身份信息 ----恒
        console.log(e);
        setVisible(false)
    };
    function handleCancel(e: React.MouseEvent<HTMLElement, MouseEvent>) {
        setVisible(false)
    };
    // 获取文本框内容
    function setTextVal(e: React.ChangeEvent<HTMLTextAreaElement>) {
        setText(e.target.defaultValue)
    }
    // table 表格样式
    const columns = [
        {
            title: '状态',
            dataIndex: 'pass',
            render: (text: boolean, record: CommentList) => <div className={style.item}>
                {
                    record.pass
                        ?
                        <b> <i className={style.yellow}></i> 通过 </b>
                        :
                        <b> <i className={style.green}></i> 未通过 </b>
                }
            </div>
        },
        {
            title: '称呼',
            dataIndex: 'name',
        },
        {
            title: '联系方式',
            dataIndex: 'email',
        }, {
            title: '原始内容',
            dataIndex: 'address',
            render: (text: boolean, record: CommentList) => <div className={style.item}>
                <BubbleComments direction="top" content={record.content} />
            </div>
        }, {
            title: 'HTML 内容',
            dataIndex: 'address',
            render: (text: boolean, record: CommentList) => <div className={style.item}>
                <BubbleComments direction="top" content={record.content} />
            </div>
        }, {
            title: '管理文章',
            dataIndex: 'address',
            render: (text: boolean, record: CommentList) => <div className={style.item}>
                <BubblePaper direction="right" url={record.url} />

            </div>
        }, {
            title: '创建时间',
            dataIndex: 'createAt',
            render: (text: boolean, record: CommentList) => <div className={style.item}>
                {moment(record.createAt).format("YYYY-MM-DD HH:mm:ss")}
            </div>
        }, {
            title: '父级评论',
            dataIndex: 'address',
            render: (text: boolean) => <div className={style.item}>
                无
            </div>
        },
        {
            title: '操作',
            key: 'action',
            render: (text: boolean, record: CommentList) => (
                <div className={style.item}>
                    <span onClick={() => setcommentpass(record.id, true)}>通过</span>
                    <span onClick={() => setcommentpass(record.id, false)}>拒绝</span>
                    <span onClick={() => showModal()}>回复</span>

                    <Popconfirm
                        title="确认删除这个评论？"
                        onConfirm={confirm}
                        okText="确定"
                        cancelText="取消"
                    >
                        <span onClick={() => setId(record.id)}>删除</span>
                    </Popconfirm>
                </div>
            ),
        },
    ];
    // 多选框 事件
    const rowSelection = {
        onChange: (selectedRowKeys: any, selectedRows: any) => {
            if (selectedRows.length > 0) {
                setflag(true)
                setSelectedRows(selectedRowKeys)
            } else {
                setflag(false)
            }
        },
        getCheckboxProps: (record: any) => (
            {
                disabled: record.name === 'Disabled User',
                name: record.name,
            }),
    };
 
    const info = [{
        name: "name", label: "称呼", placeholder: "请输入称呼"
    }, {
        name: "email", label: "Email", placeholder: "请输入联系方式"
    }, {
        name: "pass", label: "状态", placeholder: "", select: [{ value: "0", text: "未通过" }, { value: "1", text: "已通过" }]
    }]
    function getParams(params: any) { 
        store.workbench.getComment(1, 12, params)
    }
    return (
        <div className={style.comments}>

            <SearchHead info={info} getParams={getParams} />
            <div className={style.table} >
                <div className={style.placeholder}>
                    <div className={flag ? style.btnBlock : style.btnNone}>
                        <Button onClick={() => multiple(true)}>通过</Button>
                        <Button onClick={() => multiple(false)}>拒绝</Button>
                        <Button
                            style={{ borderColor: "#ff4d4f", color: "#ff4d4f" }}
                            onClick={() => multipleDel()}
                        > 删除 </Button>
                    </div>
                    <div></div>
                    <div>
                        <Tooltip placement="top" title={"刷新"}>
                            <ReloadOutlined onClick={() => store.workbench.getComment(1, 12)} />
                        </Tooltip>

                    </div>
                </div>

                <Table
                    // scroll={{ x: 1300 }}
                    rowKey="id"
                    columns={columns}
                    dataSource={store.workbench.commentList}
                    rowSelection={rowSelection}
                    pagination={{
                        showSizeChanger: true,
                        pageSizeOptions: ['8', '12', '24', '36'],
                        total: store.workbench.commentNum,
                        onChange: totals
                    }}
                />
            </div>
            <Modal
                title="回复评论"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <TextArea rows={4} onChange={(e) => setTextVal(e)} />
            </Modal>
        </div>
    )
}

export default observer(comments)
