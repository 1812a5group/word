import React, { useEffect, useState, Key } from 'react';
import { Form, Input, Button, Radio, Select, Table } from 'antd';
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore';
import style from './index.less'
import { UserDataItem } from '@/types';
import Cls from 'classnames'
import moment from 'moment';

interface Props {

}

let user: React.FC = function (props) {
    const columns = [
        {
            title: '账户',
            dataIndex: 'name',
        },
        {
            title: '邮箱',
            dataIndex: 'email',
        },
        {
            title: '角色',
            dataIndex: 'role',
            render: (text: boolean, record: UserDataItem) => {
                return <span>{record.role === 'admin' ? '管理员' : '访客'}</span>
            }
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (text: boolean, record: UserDataItem) => {
                return <span
                    className={Cls(style.UserStatus,record.status === 'active' ? '' : style.active)}>
                    <i></i>{record.status === 'locked' ? '已锁定' : '可用'}
                </span>
            }
        },
        {
            title: '注册日期',
            dataIndex: 'createAt',
            render: (text: boolean, record: UserDataItem) => {
                return <span>{moment(record.createAt).format("YYYY-MM-DD HH:mm:ss")}</span>
            }
        },
        {
            title: '操作',
            // dataIndex: 'action',
            render: (text: boolean, record: UserDataItem) => {
                return <div className={style.userAction}>
                    <span 
                        onClick={()=> {
                            store.userData.changeAction(
                                {...record,status:record.status === 'locked'? 'active' :'locked' },
                                page,
                                pageSize,
                                values
                                )
                        }}
                    >{record.status === 'locked' ? '启用' : '禁用'}</span>
                    <i></i>
                    <span
                        onClick={()=> {
                            store.userData.changeAction(
                                {...record,role:record.role === 'admin'? 'visitor' :'admin' },
                                page,
                                pageSize,
                                values
                                )
                        }}
                    >{record.role === 'admin' ? '解除授权' : '授权'}</span>
                </div>
            }
        },
    ];
    const store = useStore();
    const [page, setpage] = useState(1)
    const [pageSize, setpageSize] = useState(12)
    const [values, setvalues] = useState({})

    const layout = {
        labelCol: { span: 6 },
        // wrapperCol: { span: 12 },
    };
    const [form] = Form.useForm();
    //搜索
    const onFinish = (values: any) => {
        console.log(values);
        setvalues(values)
    };
    //重置
    const onReset = () => {
        form.resetFields();
    };
    //数据筛选
    useEffect(() => {
        store.userData.getUserData(page, pageSize, { ...values })
    }, [page, values, pageSize])

    const [selectedRowKeys, setSelectedRowKeys] = useState<Key []>([])
    const [selectedRows, setselectedRows] = useState<UserDataItem[]>([])

    function onSelectChange(selectedRowKeys: Key[], selectedRows: UserDataItem[]){
        setselectedRows(selectedRows);
    }
    //选中
    const rowSelection = {
        selectedRows,
        onChange: onSelectChange,
    };
    //页码
    const onPageChange = (pageNumber:number,pagesize:number)=> {
        setpage(pageNumber)
        setpageSize(pagesize)
    }
    //是否有选中
    const hasSelected = selectedRows.length > 0;
    //批量改变数据
    const changAll = (type:string,state:string)=> {

    }
    return (
        <div className={style.user}>
            <div className={style.searchTop}>
                <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
                    <div className={style.searchtop}>
                        <Form.Item className="formInp" name='name' label='账户'>
                            <Input placeholder='账户' />
                        </Form.Item>
                        <Form.Item className="formInp" name='email' label='邮箱'>
                            <Input placeholder='邮箱' />
                        </Form.Item>
                        <Form.Item className="formInp" label="角色" name='role'>
                            <Select>
                                <Select.Option value="admin">管理员</Select.Option>
                                <Select.Option value="visitor">访客</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item className="formInp" label="状态" name='status'>
                            <Select>
                                <Select.Option value="locked">锁定</Select.Option>
                                <Select.Option value="active">可用</Select.Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className={style.searchbtn}>
                        <div className={style.searchRight}>
                            <Button type="primary" htmlType="submit">搜索</Button>
                            <Button htmlType="button" onClick={onReset}>
                                重置
                            </Button>
                        </div>
                    </div>
                </Form>
            </div>
            <div className={style.userList}>
                <div style={{ marginBottom: 16 }}>
                    {
                        hasSelected ? <div className={style.Buttons}>
                            <Button onClick={()=> {
                                selectedRows.forEach(item=> item.status = 'active')
                                store.userData.changeAll(selectedRows,page,pageSize,values)
                            }}>启用</Button>
                            <Button onClick={()=> {
                                selectedRows.forEach(item=> item.status = 'locked')
                                store.userData.changeAll(selectedRows,page,pageSize,values)
                            }}>禁用</Button>
                            <Button onClick={()=> {
                                selectedRows.forEach(item=> item.role = 'visitor')
                                store.userData.changeAll(selectedRows,page,pageSize,values)
                            }}>接触授权</Button>
                            <Button onClick={()=> {
                                selectedRows.forEach(item=> item.role = 'admin')
                                store.userData.changeAll(selectedRows,page,pageSize,values)
                            }}>授权</Button>
                        </div> : ''
                    }
                </div>
                <Table 
                rowSelection={rowSelection} 
                columns={columns} 
                dataSource={store.userData.userData}
                rowKey='id'
                pagination={{
                    total:store.userData.userDataNum,
                    defaultPageSize:pageSize,
                    pageSizeOptions:['8','12','24','36'],
                    showTotal:total => `共 ${total} 条`,
                    onChange:(pageNumber,pagesize)=> {
                        onPageChange(pageNumber,pagesize!)
                    }
                }}
                />
            </div>
        </div >
    )
}

export default observer(user)
