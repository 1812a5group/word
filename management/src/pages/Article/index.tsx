import React, { useEffect, useState } from 'react'
import useStore from '@/context/useStore'
import { Form, Input, Select, Button, Table, Space, Badge, Popconfirm, Tag } from 'antd'
import style from './index.less'
import { IArticleList, IArticlePatch, ITag } from '@/types'
import moment from 'moment'
import { observer } from 'mobx-react-lite'
import './index.less'
import { ReloadOutlined } from '@ant-design/icons';
import { useHistory } from 'umi'
interface Props {

}


let article: React.FC<Props> = function (props) {
    const store = useStore()
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [selectedRowKeys, setSelectedRowKeys] = useState<string[]>([])
    useEffect(() => {
        store.article.getArticle({ page })
        store.article.getcategory()
    }, [page,pageSize])
    const columns: any = [
        {
            title: '标题',
            width: 200,
            dataIndex: 'title',
            key: 'name',
            fixed: 'left',
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: '1',
            width: 120,
            render: (text: string, record: IArticleList) => (
                <div>
                    <p><Badge
                        color={text === 'publish' ? '#52c41a' : '#faad14'}
                    />{text === 'publish' ? '已发布' : "草稿"}</p>
                </div>
            ),
        },
        {
            title: '分类',
            dataIndex: 'category',
            key: '2',
            width: 120,
            render: (text: ITag, record: IArticleList) => (
                text && <Tag
                    color={`#${Math.floor(Math.random() * 16777215).toString(16)}`}
                >
                    {text.label}
                </Tag>
            ),
        },
        {
            title: '标签',
            dataIndex: 'tags',
            key: '3',
            width: 120,
            render: (text: ITag[], record: IArticleList) => (
                text.length>0 && text.map(item=>{
                    return <Tag
                    key={item.id}
                    color={`#${Math.floor(Math.random() * 16777215).toString(16)}`}
                >
                    {item.label}
                </Tag>
                })
            ),
        },
        {
            title: '阅读量',
            dataIndex: 'views',
            key: '4',
            width: 120,
            render: (text: number, record: IArticleList) => (
                <Badge
                    showZero={true}
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#52c41a' }}
                />
            ),
        },
        {
            title: '喜欢数',
            dataIndex: 'likes',
            key: '5',
            width: 120,
            render: (text: number, record: IArticleList) => (
                <Badge
                    showZero={true}
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#eb2f96' }}
                />
            ),
        },
        {
            title: '发布时间',
            dataIndex: 'publishAt',
            key: '6',
            width: 200,
            render: (text: string, record: IArticleList) => (
                <p>{moment(text).format("YYYY-MM-DD HH:mm:ss")}</p>
            ),
        },
        {
            title: '操作',
            key: 'operation',
            fixed: 'right',
            width: 308,
            render: (text: IArticleList, record: IArticleList) => (
                <Space size="middle">
                    <a style={{ color: "#09f" }} onClick={()=>{history.push(`/article/editor/${text.id}`)}}>编辑</a>
                    <a style={{ color: "#09f" }} onClick={(e) => { changeSongJiao(text.id, e.target.innerHTML) }}>{text.isRecommended ? '撤销首焦' : '首焦推荐'}</a>
                    <a style={{ color: "#09f" }}>查看访问</a>
                    <Popconfirm
                        title="确定删除这个文章？"
                        okText="确定" cancelText="取消"
                        onConfirm={() => confirmDel(text.id)}
                    >
                        <a href="" style={{ color: "#09f" }}>删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];
    //点击搜索
    const onFinish = (values: any) => {
        let status = ''
        if (values.status === '已发布') {
            status = 'publish'
        } else if (values.status === '草稿') {
            status = 'draft'
        }
        store.article.getArticle({ page, pageSize, title: values.title, status, category: values.category })

    }
    //设置首焦 撤销首焦 操作单条数据
    const changeSongJiao = (id: string, value: string) => {
        
        if (value === '首焦推荐') {
            store.article.articlePatch([id], ({ isRecommended: true } as IArticlePatch))
        } else if (value === '撤销首焦') {
            store.article.articlePatch([id], ({ isRecommended: false } as IArticlePatch))
        }
    }
    const start = () => {
        setLoading(true)
        // ajax request after empty completing
        setTimeout(() => {
            setLoading(false)
            setSelectedRowKeys([])
        }, 1000);
    };
    //点击多选框保存id
    const onSelectChange = (selectedRowKeys: any) => {
        setSelectedRowKeys(selectedRowKeys)
    };
    //操作多条数据
    const changeBtn = (value: string) => {
        if (value === '发 布') {
            store.article.articlePatch(selectedRowKeys, { status: 'publish' })
        } else if (value === '草 稿') {
            store.article.articlePatch(selectedRowKeys, { status: 'draft' })
        } else if (value === '首焦推荐') {
            store.article.articlePatch(selectedRowKeys, { isRecommended: true })
        } else if (value === '撤销首焦') {
            store.article.articlePatch(selectedRowKeys, { isRecommended: false })
        }
    }
    //删除单个
    const confirmDel = (id: string) => {
        store.article.deleteArticle([id])
    }
    //删除多个数据
    const confirm = () => {
        store.article.deleteArticle(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    const hasSelected = selectedRowKeys.length > 0;
    return (
        <div >
            <div className={style.top} style={{ marginBottom: "16px" }}>
                <Form
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 14 }}
                    layout="horizontal"
                    onFinish={onFinish}
                    initialValues={{ remember: true }}
                >
                    <div className='form'>
                        <Form.Item label="标题" name='title'>
                            <Input placeholder="请输入文章标题" />
                        </Form.Item>
                        <Form.Item label="状态" name='status'>
                            <Select>
                                <Select.Option value="已发布">已发布</Select.Option>
                                <Select.Option value="草稿">草稿</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="分类" name='category'>
                            <Select>
                                <Select.Option value="demo">Demo</Select.Option>
                                {
                                    store.article.categoryList.map(item=>{
                                        return <Select.Option value={item.id} key={item.id}>{item.label}</Select.Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                    </div>
                    <div className={style.btn}>
                        <div className={style.searchbtn}>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button htmlType="reset" style={{ marginLeft: "8px" }}>
                                重置
                            </Button>
                        </div>
                    </div>
                </Form>

            </div>
            <div className={style.table}>
                <div className={style.header}>
                    <div className="left">
                        {
                            hasSelected && <div>
                                <Button style={{ marginRight: '8px' }} onClick={(e) => changeBtn(e.target.innerHTML)}>发布</Button>
                                <Button style={{ marginRight: '8px' }} onClick={(e) => changeBtn(e.target.innerHTML)}>草稿</Button>
                                <Button style={{ marginRight: '8px' }} onClick={(e) => changeBtn(e.target.innerHTML)}>首焦推荐</Button>
                                <Button style={{ marginRight: '8px' }} onClick={(e) => changeBtn(e.target.innerHTML)}>撤销首焦</Button>
                                <Button onClick={(e) => changeBtn(e.target.innerHTML)} danger>
                                    <Popconfirm
                                        title="确定删除？"
                                        okText="确定" cancelText="取消"
                                        onConfirm={confirm}
                                    >
                                        <a href="">删除</a>
                                    </Popconfirm>
                                </Button>
                            </div>
                        }
                    </div>
                    <div className="right">
                        <Button type="primary" size='middle' onClick={() => history.push('/article/editor')}> + 新建</Button>
                        <span style={{ marginLeft: '12px' }} className={style.refresh}><ReloadOutlined /></span>
                    </div>
                </div>
                { }
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={store.article.articleList}
                    rowKey={data => data.id}
                    scroll={{ x: 1300 }}
                    pagination={{
                        current: page,
                        pageSize: pageSize,
                        defaultCurrent: 1,
                        pageSizeOptions: ['8', '12', '24', '36'],
                        total: store.article.articleListNum,
                        showSizeChanger: true,
                        showTotal: (total) => `共${total}条`,
                        onChange: (page, pageSize) => {
                            setPage(page)
                            setPageSize(pageSize as number)
                        }
                    }}
                />
            </div>
        </div>
    )
}

export default observer(article)
