import React, { useState, useEffect } from 'react'
import Editor from 'for-editor'
import style from './index.less'
import useStore from '@/context/useStore'
import './index.less'
import { IArtilePostParams } from '@/types'
import { Menu, Dropdown, Button, Space, message, Drawer, Form, Input, Select } from 'antd';
import { makeHtml, makeToc } from '@/utils/markdown'
import { useHistory } from 'umi'
const menu = (
    <Menu>
        <Menu.Item key="0">查看</Menu.Item>
        <Menu.Item key="1">设置</Menu.Item>
        <Menu.Item key="2">保存草稿</Menu.Item>
        <Menu.Item key="3">删除</Menu.Item>
    </Menu>
);



const editor = () => {
    const [content, setContent] = useState('')
    const [title, setTitle] = useState('')
    const store = useStore()
    const history = useHistory()
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    // useEffect(() => {
    //     window.addEventListener('message', function (message) {
    //         setContent(message.data);
    //     })
    // }, []);
    async function submit() {
        let params:IArtilePostParams = {} 
        if (title == '') {
            message.warning('请输入文章标题')
            return;
        }
        params.content = content;
        params.html = makeHtml(content)
        params.status = 'publish'
        params.title = title
        params.toc = JSON.stringify(makeToc(params.html as string))
        
        let result = await store.article.addArticleEditor(params)
        console.log(result);
        if(result.statusCode===201){
            message.success('文章已发布')
            history.replace(`/article/editor/${result.data.id}`)
        }
        
    }

    return <div className={style.edit}>
        <div className={style.header}>
            <div className={style.box}>
                <div className={style.left}>
                    <span aria-label="close" style={{ width: "24px", height: "24px", textAlign: "center", display: "inline-block", lineHeight: "24px" }} className={style.img}>
                        <svg width="1em" height="1em" fill="currentColor" aria-hidden="true" data-icon="close" focusable="false" viewBox="64 64 896 896" style={{ textAlign: "center" }}>
                            <path d='M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z'></path>
                        </svg>
                    </span>
                    <span className={style.title}>
                        <input type="text" placeholder="请输入文章标题" className={style.inp} value={title} onChange={(e) => setTitle(e.target.value)} />
                    </span>
                </div>
                <div className={style.right}>
                    <span>
                        <Button type="primary" onClick={submit}>发布</Button>
                        <Dropdown overlay={menu} placement="bottomRight">
                            <Button style={{ border: "none" }}>
                                <svg viewBox="64 64 896 896" focusable="false" data-icon="ellipsis" width="1em" height="1em" fill="#0188fb" aria-hidden="true">
                                    <path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path>
                                </svg>
                            </Button>
                        </Dropdown>

                    </span>
                </div>
            </div>
        </div>
        <div className={style.main}>
            <Editor
                height="100%"
                value={content}
                toolbar={toolbar}
                onChange={value => setContent(value)}

            />
            {/* <section style={{ width: "100%", height: "100%" }}>
                <iframe src="https://jasonandjay.com/editor/static/" style={{ width: "100%", height: "100%" }}></iframe>
            </section> */}


        </div>
    </div>
}
export default editor;