// 标签管理页面
import React, { useEffect, useState } from 'react'
import AddArticle from '@/componets/AddArticle'
import AllArticle from '@/componets/AllArticle'
import { useLocation } from 'umi'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import style from './index.less'
import { ICategoryItem } from '@/types'
interface Props {

}
let ArticleTag: React.FC = function (props) {
    const location = useLocation()
    const store = useStore()
    const [label, setLabel] = useState('')
    const [value, setValue] = useState('')
    const [id, setId] = useState('')
    const [flag, setFlag] = useState(false)
    useEffect(() => {
        store.article.gettag()
    }, [])
    //改变内容
    const changeValue = (item: ICategoryItem) => {
        setLabel(item.label)
        setValue(item.value)
        setId(item.id)
        setFlag(true)
    }
    //点击返回按钮 改变falg
    const changeFlag = () => {
        setFlag(false)
        setLabel('')
        setValue('')
    }
    //改变input框数据内容
    const changeVal = (name: string, val: string, id: string) => {
        setId(id)
        if (name === 'label') {
            setLabel(val)
        } else if (name === 'value') {
            setValue(val)
        }
    }
    //点击更新按钮
    const updateCategory = (val: string) => {
        if (val === '更 新') {
            if (label !== '' && value !== '') {
                store.article.changeCategory(location.pathname.split('/')[2].slice(0, -1), id, { label, value })
                setFlag(false)
                setLabel('')
                setValue('')
            }

        } else { //点击保存按钮
            if (label !== '' && value !== '') {
                store.article.addCategory(location.pathname.split('/')[2].slice(0, -1), { label, value })
                setLabel('')
                setValue('')
            }

        }
    }
    //点击删除按钮
    const delCategory = (id: string) => {
        store.article.delCategory(location.pathname.split('/')[2].slice(0, -1), id)
        setFlag(false)
        setLabel('')
        setValue('')
    }
    return (
        <div>
            <div className={style.box}>
                <div className={style.left}>
                    <AddArticle
                        title="标签"
                        label={label}
                        value={value}
                        id={id}
                        flag={flag}
                        changeFlag={changeFlag}
                        changeVal={changeVal}
                        updateCategory={updateCategory}
                        delCategory={delCategory}
                    />
                </div>
                <div className={style.right}>
                    <div className={style.box}>
                        <AllArticle
                            title="标签"
                            list={store.article.tagList}
                            changeValue={changeValue}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default observer(ArticleTag)
