import SearchHead from '@/componets/SearchHead';
import React, { Key, ReactElement, useEffect, useState } from 'react';
import style from "./index.less";
import { Alert, Empty, Table, Button, Popconfirm, message, Badge, Tooltip, Modal, Input } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { makeHtml } from '@/utils/markdown';
import { Link, useHistory } from 'react-router-dom';
import useStore from '@/context/useStore';
import moment from 'moment';
import { ImailItem } from '@/types';
import { replyMail } from '@/serivces';
interface Props {

}

let mail: React.FC = function (props) {
    const store = useStore()
    const history = useHistory()
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState({});
    const [reply, setReply] = useState('');
    const [showDelete, setShowDelete] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [current, setCurrent] = useState<ImailItem>();
    useEffect(() => {
        store.mail.getMailList(page, pageSize, params)
    }, [page, pageSize, params])

    const [selectedRoekeys, setSelectedRoekeys] = useState<Key[]>([])
    function onSelectChange(selectedRoekeys: Key[]) {
        setSelectedRoekeys(selectedRoekeys)
        selectedRoekeys.length > 0 ? setShowDelete(true) : setShowDelete(false)

    }
    //多选框
    const rowSelection = {
        selectedRoekeys,
        onChange: onSelectChange
    };
    function getParams(params: any) {
        store.mail.getMailList(page, pageSize, params)
    }

    //回复
    async function replayMail() {
        if (!reply) {
            message.warn('请输入回复内容');
            return;
        }
        let result = await replyMail({
            to: current?.to!,
            subject: `回复${current?.from}`,
            html: makeHtml(reply)
        })
        setShowModal(false);
        if (result.message === '发送邮件成功') {
            message.success(result.message);
        } else {
            message.error(result.message);
        }
    }
    const info = [{
        name: "from", label: "发件人", placeholder: "请输入发件人", key: "1"
    }, {
        name: "to", label: "收件人", placeholder: "请输入收件人", key: "2"
    }, {
        name: "subject", label: "主题", placeholder: "请输入主题", key: "3"
    }]

    const columns = [
        {
            title: '发件人',
            dataIndex: 'from',
        },
        {
            title: '收件人',
            dataIndex: 'to',
        }, {
            title: '主题',
            dataIndex: 'subject',
        },
        {
            title: '发送时间',
            dataIndex: "createAt",
            render: (text: string, record: any) => (
                <div>{moment(text).format("YYYY-MM-DD HH:mm:ss")}</div>
            ),
        },
        {
            title: '操作',
            key: 'action',
            render: (record: ImailItem) => (
                <span>
                    <Popconfirm
                        title="确认删除这个搜索记录？"
                        onConfirm={() => { store.mail.deleteMail([record.id]) }}
                        onCancel={() => { message.error('Click on No') }}
                        okText="确认"
                        cancelText="取消"
                    >
                        <a style={{ color: "#1890ff" }}>删除</a>
                    </Popconfirm>
                    &nbsp; <a style={{ color: "#1890ff" }} onClick={() => {
                        setShowModal(true);
                        setCurrent(record);
                    }}>回复</a>
                </span>
            ),
        },
    ];
    const messageinfo = (
        <>系统检测到<strong> SMTP 配置 </strong> 未完善，当收到评论时，无法进行邮件通知。<span onClick={() => history.push("/setting")}>点我立即完善</span></>
    )

    return (
        <>
            <div className={style.tipe}>
                <Link to="setting">
                    <Alert message={messageinfo} type="warning" />
                </Link>
            </div>

            {/* 搜索框 */}
            <SearchHead info={info} getParams={getParams} />

            <div className={style.mailbottom}>
                {
                    store.mail.mailList ? <div className={style.searchBottom}>
                        <div className={style.button}>
                            <div className={style.btn_left} style={{ display: showDelete ? "block" : "none" }}>
                                <Popconfirm
                                    title="确认删除？"
                                    onConfirm={() => {
                                        store.mail.deleteMail(selectedRoekeys as string[])
                                        setShowDelete(false)
                                    }}
                                    onCancel={() => message.error('取消删除')}
                                    okText="确认"
                                    cancelText="取消"
                                >
                                    <Button className={style.deletebtn} danger>删除</Button>
                                </Popconfirm>
                            </div>

                            <Tooltip placement="top" title="刷新">
                                <p className={style.btn_right} onClick={() => {
                                    store.mail.getMailList(page, pageSize, params)
                                }}><ReloadOutlined /></p>
                            </Tooltip>
                        </div>
                        <Table
                            rowKey="id"
                            scroll={{ x: 1300 }}
                            rowSelection={rowSelection}
                            columns={columns}
                            dataSource={store.mail.mailList}
                            pagination={{
                                showSizeChanger: true,
                                pageSizeOptions: ['8', '10', '20', '30'],
                                total: store.mail.mailListNum,
                                showTotal: (total) => `共${total}条 `,
                                onChange: (page, pageSize) => {
                                    store.mail.getMailList(page, pageSize!)
                                    setPage(page)
                                    setPageSize(pageSize as number)
                                }
                            }}
                        />
                    </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                }
                <Modal
                    title={`回复${current?.to}`}
                    onCancel={() => setShowModal(false)}
                    visible={showModal}
                    footer={<Button onClick={() => replayMail()}>回复</Button>}
                >
                    <Input.TextArea placeholder="回复内容支持md,html和普通文本" rows={8} value={reply} onChange={e => setReply(e.target.value)}></Input.TextArea>
                </Modal>
            </div>
        </>
    )
}

export default mail
