import React, { ReactElement, useEffect, useState, Key } from 'react'
import style from "./index.less"
import { Table, Button, Popconfirm, message, Badge, Tooltip, Empty } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore';
import { ISearchInfo } from '@/types';
import SearchHead from '@/componets/SearchHead';
import moment from 'moment';

interface Props {

}

let Search: React.FC = function (props) {
    const store = useStore();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState({});
    const [showDelete, setShowDelete] = useState(false);

    useEffect(() => {
        store.search.getsearchArticle(page, pageSize, params)
    }, [params])

    const [selectedRoekeys, setSelectedRoekeys] = useState<Key[]>([])
    //按钮
    function onSelectChange(selectedRoekeys: Key[]) {
        setSelectedRoekeys(selectedRoekeys)
        selectedRoekeys.length > 0 ? setShowDelete(true) : setShowDelete(false)
    }
    //多选框
    const rowSelection = {
        selectedRoekeys,
        onChange: onSelectChange
    };


    function getParams(params: any) {
        store.search.getsearchArticle(page, pageSize, params)
    }

    const info = [{
        name: "type", label: "类型", placeholder: "请输入搜索类型"
    }, {
        name: "keyword", label: "搜索词", placeholder: "请输入搜索词"
    }, {
        name: "count", label: "搜索量", placeholder: "请输入搜索量"
    }]


    const columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            dataIndex: 'count',
            render: (text: string, record: any) => (
                <Badge
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#52c41a' }}
                />
            ),
        },
        {
            title: '搜索时间',
            dataIndex: "createAt",
            render: (text: string, record: any) => (
                <div>{moment(text).format("YYYY-MM-DD HH:mm:ss")}</div>
            ),
        },
        {
            title: '操作',
            key: 'action',
            render: (record: ISearchInfo) => (
                <span>
                    <Popconfirm
                        title="确认删除这个搜索记录？"
                        onConfirm={() => {
                            store.search.deleteRecords([record.id])
                        }}
                        onCancel={() => { message.error('取消删除') }}
                        okText="确认"
                        cancelText="取消"
                    >
                        <a style={{ color: "#1890ff" }}>删除</a>
                    </Popconfirm>
                </span>
            ),
        },
    ];
    return (
        <>
            {/* 搜索框 */}
            <SearchHead info={info} getParams={getParams} />

            <div className={style.mailbottom}>
                {
                    store.search.searchInfo ? <div className={style.searchBottom}>
                        <div className={style.button}>
                            <div className={style.btn_left} style={{ display: showDelete ? "block" : "none" }}>
                                <Popconfirm
                                    title="确认删除？"
                                    onConfirm={() => {
                                        store.search.deleteRecords(selectedRoekeys as string[])
                                        setShowDelete(false)
                                    }}
                                    onCancel={() => message.error('取消删除')}
                                    okText="确认"
                                    cancelText="取消"
                                >
                                    <Button className={style.deletebtn} danger>删除</Button>
                                </Popconfirm>
                            </div>

                            <Tooltip placement="top" title="刷新">
                                <p className={style.btn_right} onClick={() => {
                                    store.search.getsearchArticle(1, 12)
                                }}><ReloadOutlined /></p>
                            </Tooltip>
                        </div>
                        <Table
                            rowKey="id"
                            scroll={{ x: 0 }}
                            rowSelection={rowSelection}
                            columns={columns}
                            dataSource={store.search.searchInfo}
                            pagination={{
                                showSizeChanger: true,
                                pageSizeOptions: ['8', '10', '20', '30'],
                                total: store.search.searchInfoNum,
                                showTotal: (total) => `共${total}条 `,
                                onChange: (page, pageSize) => {
                                    store.search.getsearchArticle(page, pageSize!)
                                    setPage(page)
                                    setPageSize(pageSize as number)
                                }
                            }}
                        />
                    </div> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                }
            </div>
        </>
    )

}

export default observer(Search);
