import React, { ReactElement } from 'react'  
import FileReuse from '@/componets/FileReuse'
import style from './index.less'
import {Upload,message} from 'antd'
const { Dragger } = Upload;
import { InboxOutlined } from '@ant-design/icons';
interface Props {

}

let File: React.FC = function (props) {
    const propsAddress = {
        name: 'file',
        multiple : true,
        action: '/upload',
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return (
        <> 
        <div className={style.file_upload}>
                <Dragger {...propsAddress} style={{ background: "#fff" }}>
                    <p className="ant-upload-drag-icon"><InboxOutlined /></p>
                    <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                    <p className="ant-upload-hint">将文件上传到 OSS, 如未配置请先配置</p>
                </Dragger>
            </div>
            <FileReuse />
        </>
    )
}

export default File
