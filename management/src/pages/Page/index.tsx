import React, { ReactElement, useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import { Table, Popconfirm, Button, Tooltip, Modal } from 'antd'
import { ReloadOutlined } from '@ant-design/icons';
import SearchHead from '@/componets/SearchHead'
import useStore from '@/context/useStore'
import style from './index.less'
import moment from 'moment'

interface Props extends RouteComponentProps {

}

let PageManagement: React.FC<RouteComponentProps> = function (props) {
    const store = useStore()
    const [page, setPage] = useState(1)
    const [pagesize, setPagesize] = useState(12)
    let [flag, setflag] = useState(false)
    let [id, setId] = useState("");
    let [visible, setVisible] = useState(false)
    let [selectedRows, setSelectedRows] = useState<string[]>([])
    // 搜索样式
    const info = [{
        name: "title", label: "名称", placeholder: "请输入页面名称"
    }, {
        name: "city", label: "路径", placeholder: "请输入页面路径"
    }, {
        name: "status", label: "状态", placeholder: "",
        select: [
            { value: "publish", text: "已发布" },
            { value: "draft", text: "草稿" }
        ]
    }]
    useEffect(() => {
        store.Page.getpagedata(page, pagesize)
    }, [])
    // 删除确定
    async function confirm() {
        await store.Page.delpages(id);
        await store.Page.getpagedata(page, pagesize);
    }
    // 删除
    async function del() {
        console.log(id);
    }
    // 多个删除
    async function alldel() {
        await store.Page.setalldel(selectedRows, page, pagesize);
    }
    // 多个发布草稿
    async function allrelease(status: string) {
        await store.Page.setallrelease(selectedRows, { status }, page, pagesize);
        await store.Page.getpagedata(page, pagesize);
    }
    // 发布草稿
    async function release(id: string, status: string) {
        await store.Page.setpagestatus(id, { status });
    }
    // table 表格样式
    const columns = [
        {
            title: '名称',
            dataIndex: 'name',
        },
        {
            title: '路径',
            dataIndex: 'path',
        },
        {
            title: '顺序',
            dataIndex: 'order',
        }, {
            title: '阅读量',
            dataIndex: 'views',
            render: (text: boolean, record: any) => <div className={style.views}>
                <span>{record.views}</span>
            </div>
        }, {
            title: '状态',
            dataIndex: 'status',
            render: (text: boolean, record: any) => <div className={style.status}>
                {
                    record.status === "draft" ? <span>
                        <i className={style.green}></i> 草稿
                    </span>
                        : <span>
                            <i className={style.yellow}></i>已发布
                        </span>
                }
            </div>
        }, {
            title: '发布时间',
            dataIndex: 'publishAt',
            render: (text: boolean, record: any) => <div className={style.publishAt}>
                {moment(record.publishAt).format("YYYY-MM-DD HH:mm:ss")}
            </div>
        }, {
            title: '操作',
            dataIndex: 'createAt',
            render: (text: boolean, record: any) => <div className={style.createAt}>
                <span>编辑</span>
                <span onClick={() => release(record.id, record.status === "draft" ? "publish" : "draft")}>
                    {
                        record.status === "draft" ? "发布" : "下线"
                    }
                </span>
                <span onClick={() => setVisible(!visible)}>访问</span>
                <Popconfirm
                    title="确认删除这个评论？"
                    onConfirm={confirm}
                    okText="确定"
                    cancelText="取消"
                >
                    <span onClick={() => setId(record.id)}>删除</span>
                </Popconfirm>
            </div>
        }
    ];
    // 多选框 事件
    const rowSelection = {
        onChange: (selectedRowKeys: any, selectedRows: any) => {
            if (selectedRows.length > 0) {
                setflag(true)
                setSelectedRows(selectedRowKeys)
            } else {
                setflag(false)
            }
        },
        getCheckboxProps: (record: any) => (
            {
                disabled: record.name === 'Disabled User',
                name: record.name,
            }),
    };
    // 页码改变请求s
    async function totals(page: number, pagesize?: number) {
        setPage(page)
        setPagesize(pagesize as number)
        await store.Page.getpagedata(page, pagesize as number)
    }
    async function getParams(params: any) {
        await store.Page.getpagedata(page, pagesize, params)
    }
    return (
        <div className={style.page}>
            <SearchHead info={info} getParams={getParams} />
            <div className={style.table}>
                <div className={style.placeholder}>
                    <div className={flag ? style.btnBlock : style.btnNone}>
                        <Button onClick={() => allrelease("publish")}>发布</Button>
                        <Button onClick={() => allrelease("draft")}>下线</Button>
                        <Popconfirm
                            title="确认删除这个评论？"
                            onConfirm={alldel}
                            okText="确定"
                            cancelText="取消"
                        >
                            <Button
                                style={{ borderColor: "#ff4d4f", color: "#ff4d4f" }}
                            > 删除 </Button>
                        </Popconfirm>

                    </div>
                    <div></div>
                    <div>
                        <Button type="primary" onClick={() => props.history.push('/page/editor')} ><span> + </span> 新建 </Button>
                        <Tooltip placement="top" title={"刷新"}>
                            <ReloadOutlined onClick={() => store.Page.getpagedata(page, pagesize)} />
                        </Tooltip>
                    </div>
                </div>
                <Table
                    // scroll={{ x: 1300 }}
                    rowKey="id"
                    columns={columns}
                    dataSource={store.Page.pageList}
                    rowSelection={rowSelection}
                    pagination={{
                        showSizeChanger: true,
                        pageSizeOptions: ['8', '12', '24', '36'],
                        total: store.Page.pageListNum,
                        onChange: totals
                    }}
                />
            </div>
            <Modal
                title="访问统计"
                visible={visible}
                onCancel={() => setVisible(!visible)}
                footer={null}
            >
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </div>
    )
}

export default PageManagement
