import React, { useEffect, useState } from 'react'
import { useRouteMatch } from 'umi'
import useStore from '@/context/useStore'
import Editor from 'for-editor'
import style from './index.less'
import './index.less'
import { Menu, Dropdown, Button, Space, message, Drawer, Form, Input, Select } from 'antd';
import { makeHtml, makeToc } from '@/utils/markdown'
import { useHistory } from 'umi'
import { IDetailObj } from '@/types'
interface Props {

}
interface Id {
    id: string
}
const detailEditor: React.FC = () => {


    let match = useRouteMatch<Id>()
    const [settingDrawer, setSettingDrawer] = useState(false);
    const [content, setContent] = useState('')
    const [title, setTitle] = useState('')
    const [cover, setCover] = useState('')
    const [form] = Form.useForm();
    const store = useStore()
    const history = useHistory()
    const [count,setCount] = useState(0)
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    const menu = (
        <Menu>
            <Menu.Item key="0">查看</Menu.Item>
            <Menu.Item key="1" onClick={() => setSettingDrawer(true)}>设置</Menu.Item>
            <Menu.Item key="2">保存草稿</Menu.Item>
            <Menu.Item key="3">删除</Menu.Item>
        </Menu>
    );
    //获取详情数据
    useEffect(() => {
        store.article.getArticleDetail(match.params.id).then(res => {

            setTitle(res.title)
            setContent(res.content)
            setCover(res.cover)
        }).catch(error => {
            console.log(error);

        })
        store.article.getcategory()
        store.article.gettag()
    }, [])
    useEffect(() => {
        if (settingDrawer&&count===1) {
            let obj: Partial<IDetailObj> = store.article.detailObj
            store.article.detailObj.tags!.length > 0 ? obj.tags = obj.tags?.map(item => item.id) : obj.tags = []
            obj.category ? obj.category = store.article.detailObj.category.id : obj.category = null
            form.setFieldsValue(obj)
        }
        setCount(count+1)
    }, [settingDrawer])
    function submit(starus:string) {
        let params: { [key: string]: string | boolean | null | Array<string>} = form.getFieldsValue()

        params.content = content;
        params.html = makeHtml(content)
        params.status = starus
        params.title = title
        params.toc = JSON.stringify(makeToc(params.html as string))
        params = {...store.article.detailObj,...params} as { [key: string]: string | boolean | null };
        if((store.article.detailObj.tags as Array<any>).length>0){
            params.tags=(store.article.detailObj.tags  as Array<string>).map(item=>item).join(',')
        }else{
            params.tags=(params.tags as Array<string>).map(item=>item).join(',')
        }

        console.log(params);
        store.article.articlePatch([params.id as string],params)
        
    }


    return <div className={style.edit}>
        <div className={style.header}>
            <div className={style.box}>
                <div className={style.left}>
                    <span aria-label="close" style={{ width: "24px", height: "24px", textAlign: "center", display: "inline-block", lineHeight: "24px" }} className={style.img}>
                        <svg width="1em" height="1em" fill="currentColor" aria-hidden="true" data-icon="close" focusable="false" viewBox="64 64 896 896" style={{ textAlign: "center" }}>
                            <path d='M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z'></path>
                        </svg>
                    </span>
                    <span className={style.title}>
                        <input type="text" placeholder="请输入文章标题"

                            className={style.inp} value={title} onChange={(e) => setTitle(e.target.value)} />
                    </span>
                </div>
                <div className={style.right}>
                    <span>
                        <Button type="primary" onClick={()=>submit('publish')}>发布</Button>
                        <Dropdown overlay={menu} placement="bottomRight">
                            <Button style={{ border: "none" }}>
                                <svg viewBox="64 64 896 896" focusable="false" data-icon="ellipsis" width="1em" height="1em" fill="#0188fb" aria-hidden="true">
                                    <path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path>
                                </svg>
                            </Button>
                        </Dropdown>

                    </span>
                </div>
            </div>
        </div>


        <div className={style.main}>
            <Editor
                height="100%"
                value={content}
                toolbar={toolbar}
                onChange={value => setContent(value)}
            />
        </div>
        <Drawer
            width="35%"
            title="文章设置"
            placement="right"
            onClose={() => setSettingDrawer(false)}
            visible={settingDrawer}
            footer={
                <Button type='primary' onClick={() => {
                    setSettingDrawer(false)
                }} htmlType="submit">确认</Button>
            }
        >
            <Form
                form={form}
            >
                <Form.Item name="summary" label="文章摘要">
                    <Input.TextArea></Input.TextArea>
                </Form.Item>
                <Form.Item name="password" label="访问密码">
                    <Input.Password placeholder="" />
                </Form.Item>
                <Form.Item name="totalAmount" label="付费查看">
                    <Input.Password placeholder="" />
                </Form.Item>
                <Form.Item name="isCommentable" label="开启评论">
                    <Input type="checkbox" placeholder="" />
                </Form.Item>
                <Form.Item name="isRecommended" label="首页推荐">
                    <Input type="checkbox" placeholder="" />
                </Form.Item>
                <Form.Item name="category" label="选择分类">
                    <Select>{
                        store.article.categoryList.map(item => {
                            return <Select.Option value={item.id} key={item.id}>{item.label}</Select.Option>
                        })
                    }</Select>
                </Form.Item>
                <Form.Item name="tags"
                    label="选择标签">
                    <Select
                        mode="tags"

                    >{
                            store.article.tagList.map(item => {
                                return <Select.Option value={item.id} key={item.id}>{item.label}</Select.Option>
                            })
                        }</Select>
                </Form.Item>
                <img src={cover} alt="" />
                <Form.Item name="cover" label="文章封面">
                    <Input type="text" placeholder="" onChange={e => setCover(e.target.value)} />
                </Form.Item>
                <Button onClick={() => {
                    let values = form.getFieldsValue();
                    form.setFieldsValue({ ...values, cover: '' })
                    setCover('');
                }}>移除</Button>
            </Form>
        </Drawer>

    </div>
}
export default detailEditor